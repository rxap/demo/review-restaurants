export interface RestaurantControllerGetByIdParameter {
  restaurantId: string;
}
