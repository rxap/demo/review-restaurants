export interface ReviewControllerUpdateParameter {
  restaurantId: string;
  reviewId: string;
}
