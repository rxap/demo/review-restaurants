export interface UserControllerDeleteParameter {
  userId: string;
}
