export interface ReviewControllerDeleteParameter {
  restaurantId: string;
  reviewId: string;
}
