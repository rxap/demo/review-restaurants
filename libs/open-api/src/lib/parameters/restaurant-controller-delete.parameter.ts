export interface RestaurantControllerDeleteParameter {
  restaurantId: string;
}
