export interface UserControllerGetByIdParameter {
  userId: string;
}
