import {
  ChangeDetectorRef,
  Directive,
  Inject,
  Injectable,
  INJECTOR,
  Injector,
  Input,
  NgModule,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';
import {
  OpenApiRemoteMethod,
  OpenApiRemoteMethodParameter,
  RxapOpenApiRemoteMethod,
} from '@rxap/open-api/remote-method';
import { RemoteMethodLoader } from '@rxap/remote-method';
import {
  RemoteMethodDirective,
  RemoteMethodTemplateDirective,
  RemoteMethodTemplateDirectiveContext,
  RemoteMethodTemplateDirectiveErrorContext,
} from '@rxap/remote-method/directive';
import { UserControllerUpdateParameter } from '../parameters/user-controller-update.parameter';
import { UserControllerUpdateRequestBody } from '../request-bodies/user-controller-update.request-body';

@Injectable({
  providedIn: 'root',
})
@RxapOpenApiRemoteMethod('UserController_update')
export class UserControllerUpdateRemoteMethod
  extends OpenApiRemoteMethod<void, UserControllerUpdateParameter, UserControllerUpdateRequestBody> {
  public call(parameters: OpenApiRemoteMethodParameter<UserControllerUpdateParameter, UserControllerUpdateRequestBody>): Promise<void> {
    return super.call(parameters);
  }
}

@Directive({
  selector: '[reviewRestaurantsUserControllerUpdateRemoteMethod]',
  exportAs: 'reviewRestaurantsUserControllerUpdateRemoteMethod',
})
export class UserControllerUpdateRemoteMethodTemplateDirective
  extends RemoteMethodTemplateDirective<void, OpenApiRemoteMethodParameter<UserControllerUpdateParameter, UserControllerUpdateRequestBody>> {
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsUserControllerUpdateRemoteMethodParameters')
  public parameters?: OpenApiRemoteMethodParameter<UserControllerUpdateParameter, UserControllerUpdateRequestBody>;
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsUserControllerUpdateRemoteMethodError')
  public errorTemplate?: TemplateRef<RemoteMethodTemplateDirectiveErrorContext>;

  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(UserControllerUpdateRemoteMethod) remoteMethod: UserControllerUpdateRemoteMethod,
    @Inject(TemplateRef) template: TemplateRef<RemoteMethodTemplateDirectiveContext<void>>,
    @Inject(ViewContainerRef) viewContainerRef: ViewContainerRef,
    @Inject(ChangeDetectorRef) cdr: ChangeDetectorRef,
  ) {
    super(template, remoteMethodLoader, injector, viewContainerRef, cdr);
    this.remoteMethodOrIdOrToken = remoteMethod;
  }
}

@NgModule({
  declarations: [UserControllerUpdateRemoteMethodTemplateDirective],
  exports:      [UserControllerUpdateRemoteMethodTemplateDirective],
})
export class UserControllerUpdateRemoteMethodTemplateDirectiveModule {
}

@Directive({
  selector: '[reviewRestaurantsUserControllerUpdateRemoteMethod]',
  exportAs: 'reviewRestaurantsUserControllerUpdateRemoteMethod',
})
export class UserControllerUpdateRemoteMethodDirective
  extends RemoteMethodDirective<void, OpenApiRemoteMethodParameter<UserControllerUpdateParameter, UserControllerUpdateRequestBody>> {
  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(UserControllerUpdateRemoteMethod) remoteMethod: UserControllerUpdateRemoteMethod,
  ) {
    super(remoteMethodLoader, injector);
    this.remoteMethodOrIdOrToken = remoteMethod;
  }
}

@NgModule({
  declarations: [UserControllerUpdateRemoteMethodDirective],
  exports:      [UserControllerUpdateRemoteMethodDirective],
})
export class UserControllerUpdateRemoteMethodDirectiveModule {
}
