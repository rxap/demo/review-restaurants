import {
  ChangeDetectorRef,
  Directive,
  Inject,
  Injectable,
  INJECTOR,
  Injector,
  Input,
  NgModule,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';
import {
  OpenApiRemoteMethod,
  OpenApiRemoteMethodParameter,
  RxapOpenApiRemoteMethod,
} from '@rxap/open-api/remote-method';
import { RemoteMethodLoader } from '@rxap/remote-method';
import {
  RemoteMethodDirective,
  RemoteMethodTemplateDirective,
  RemoteMethodTemplateDirectiveContext,
  RemoteMethodTemplateDirectiveErrorContext,
} from '@rxap/remote-method/directive';
import { AppControllerGetDataResponse } from '../responses/app-controller-get-data.response';

@Injectable({
  providedIn: 'root',
})
@RxapOpenApiRemoteMethod('AppController_getData')
export class AppControllerGetDataRemoteMethod extends OpenApiRemoteMethod<AppControllerGetDataResponse, void, void> {
  public call(): Promise<AppControllerGetDataResponse> {
    return super.call();
  }
}

@Directive({
  selector: '[reviewRestaurantsAppControllerGetDataRemoteMethod]',
  exportAs: 'reviewRestaurantsAppControllerGetDataRemoteMethod',
})
export class AppControllerGetDataRemoteMethodTemplateDirective
  extends RemoteMethodTemplateDirective<AppControllerGetDataResponse, OpenApiRemoteMethodParameter<void, void>> {
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsAppControllerGetDataRemoteMethodParameters')
  public parameters?: OpenApiRemoteMethodParameter<void, void>;
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsAppControllerGetDataRemoteMethodError')
  public errorTemplate?: TemplateRef<RemoteMethodTemplateDirectiveErrorContext>;

  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(AppControllerGetDataRemoteMethod) remoteMethod: AppControllerGetDataRemoteMethod,
    @Inject(TemplateRef) template: TemplateRef<RemoteMethodTemplateDirectiveContext<AppControllerGetDataResponse>>,
    @Inject(ViewContainerRef) viewContainerRef: ViewContainerRef,
    @Inject(ChangeDetectorRef) cdr: ChangeDetectorRef,
  ) {
    super(template, remoteMethodLoader, injector, viewContainerRef, cdr);
    this.remoteMethodOrIdOrToken = remoteMethod;
    this.withoutParameters       = true;
  }
}

@NgModule({
  declarations: [AppControllerGetDataRemoteMethodTemplateDirective],
  exports:      [AppControllerGetDataRemoteMethodTemplateDirective],
})
export class AppControllerGetDataRemoteMethodTemplateDirectiveModule {
}

@Directive({
  selector: '[reviewRestaurantsAppControllerGetDataRemoteMethod]',
  exportAs: 'reviewRestaurantsAppControllerGetDataRemoteMethod',
})
export class AppControllerGetDataRemoteMethodDirective
  extends RemoteMethodDirective<AppControllerGetDataResponse, OpenApiRemoteMethodParameter<void, void>> {
  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(AppControllerGetDataRemoteMethod) remoteMethod: AppControllerGetDataRemoteMethod,
  ) {
    super(remoteMethodLoader, injector);
    this.remoteMethodOrIdOrToken = remoteMethod;
  }
}

@NgModule({
  declarations: [AppControllerGetDataRemoteMethodDirective],
  exports:      [AppControllerGetDataRemoteMethodDirective],
})
export class AppControllerGetDataRemoteMethodDirectiveModule {
}
