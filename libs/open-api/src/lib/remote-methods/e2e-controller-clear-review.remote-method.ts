import {
  ChangeDetectorRef,
  Directive,
  Inject,
  Injectable,
  INJECTOR,
  Injector,
  Input,
  NgModule,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';
import {
  OpenApiRemoteMethod,
  OpenApiRemoteMethodParameter,
  RxapOpenApiRemoteMethod,
} from '@rxap/open-api/remote-method';
import { RemoteMethodLoader } from '@rxap/remote-method';
import {
  RemoteMethodDirective,
  RemoteMethodTemplateDirective,
  RemoteMethodTemplateDirectiveContext,
  RemoteMethodTemplateDirectiveErrorContext,
} from '@rxap/remote-method/directive';

@Injectable({
  providedIn: 'root',
})
@RxapOpenApiRemoteMethod('E2eController_clearReview')
export class E2eControllerClearReviewRemoteMethod extends OpenApiRemoteMethod<void, void, void> {
  public call(): Promise<void> {
    return super.call();
  }
}

@Directive({
  selector: '[reviewRestaurantsE2eControllerClearReviewRemoteMethod]',
  exportAs: 'reviewRestaurantsE2eControllerClearReviewRemoteMethod',
})
export class E2eControllerClearReviewRemoteMethodTemplateDirective
  extends RemoteMethodTemplateDirective<void, OpenApiRemoteMethodParameter<void, void>> {
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsE2eControllerClearReviewRemoteMethodParameters')
  public parameters?: OpenApiRemoteMethodParameter<void, void>;
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsE2eControllerClearReviewRemoteMethodError')
  public errorTemplate?: TemplateRef<RemoteMethodTemplateDirectiveErrorContext>;

  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(E2eControllerClearReviewRemoteMethod) remoteMethod: E2eControllerClearReviewRemoteMethod,
    @Inject(TemplateRef) template: TemplateRef<RemoteMethodTemplateDirectiveContext<void>>,
    @Inject(ViewContainerRef) viewContainerRef: ViewContainerRef,
    @Inject(ChangeDetectorRef) cdr: ChangeDetectorRef,
  ) {
    super(template, remoteMethodLoader, injector, viewContainerRef, cdr);
    this.remoteMethodOrIdOrToken = remoteMethod;
    this.withoutParameters       = true;
  }
}

@NgModule({
  declarations: [E2eControllerClearReviewRemoteMethodTemplateDirective],
  exports:      [E2eControllerClearReviewRemoteMethodTemplateDirective],
})
export class E2eControllerClearReviewRemoteMethodTemplateDirectiveModule {
}

@Directive({
  selector: '[reviewRestaurantsE2eControllerClearReviewRemoteMethod]',
  exportAs: 'reviewRestaurantsE2eControllerClearReviewRemoteMethod',
})
export class E2eControllerClearReviewRemoteMethodDirective
  extends RemoteMethodDirective<void, OpenApiRemoteMethodParameter<void, void>> {
  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(E2eControllerClearReviewRemoteMethod) remoteMethod: E2eControllerClearReviewRemoteMethod,
  ) {
    super(remoteMethodLoader, injector);
    this.remoteMethodOrIdOrToken = remoteMethod;
  }
}

@NgModule({
  declarations: [E2eControllerClearReviewRemoteMethodDirective],
  exports:      [E2eControllerClearReviewRemoteMethodDirective],
})
export class E2eControllerClearReviewRemoteMethodDirectiveModule {
}
