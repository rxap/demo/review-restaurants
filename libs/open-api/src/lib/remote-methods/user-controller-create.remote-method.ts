import {
  ChangeDetectorRef,
  Directive,
  Inject,
  Injectable,
  INJECTOR,
  Injector,
  Input,
  NgModule,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';
import {
  OpenApiRemoteMethod,
  OpenApiRemoteMethodParameter,
  RxapOpenApiRemoteMethod,
} from '@rxap/open-api/remote-method';
import { RemoteMethodLoader } from '@rxap/remote-method';
import {
  RemoteMethodDirective,
  RemoteMethodTemplateDirective,
  RemoteMethodTemplateDirectiveContext,
  RemoteMethodTemplateDirectiveErrorContext,
} from '@rxap/remote-method/directive';
import { UserControllerCreateRequestBody } from '../request-bodies/user-controller-create.request-body';

@Injectable({
  providedIn: 'root',
})
@RxapOpenApiRemoteMethod('UserController_create')
export class UserControllerCreateRemoteMethod extends OpenApiRemoteMethod<void, void, UserControllerCreateRequestBody> {
  public call(parameters: OpenApiRemoteMethodParameter<void, UserControllerCreateRequestBody>): Promise<void> {
    return super.call(parameters);
  }
}

@Directive({
  selector: '[reviewRestaurantsUserControllerCreateRemoteMethod]',
  exportAs: 'reviewRestaurantsUserControllerCreateRemoteMethod',
})
export class UserControllerCreateRemoteMethodTemplateDirective
  extends RemoteMethodTemplateDirective<void, OpenApiRemoteMethodParameter<void, UserControllerCreateRequestBody>> {
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsUserControllerCreateRemoteMethodParameters')
  public parameters?: OpenApiRemoteMethodParameter<void, UserControllerCreateRequestBody>;
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsUserControllerCreateRemoteMethodError')
  public errorTemplate?: TemplateRef<RemoteMethodTemplateDirectiveErrorContext>;

  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(UserControllerCreateRemoteMethod) remoteMethod: UserControllerCreateRemoteMethod,
    @Inject(TemplateRef) template: TemplateRef<RemoteMethodTemplateDirectiveContext<void>>,
    @Inject(ViewContainerRef) viewContainerRef: ViewContainerRef,
    @Inject(ChangeDetectorRef) cdr: ChangeDetectorRef,
  ) {
    super(template, remoteMethodLoader, injector, viewContainerRef, cdr);
    this.remoteMethodOrIdOrToken = remoteMethod;
    this.withoutParameters       = true;
  }
}

@NgModule({
  declarations: [UserControllerCreateRemoteMethodTemplateDirective],
  exports:      [UserControllerCreateRemoteMethodTemplateDirective],
})
export class UserControllerCreateRemoteMethodTemplateDirectiveModule {
}

@Directive({
  selector: '[reviewRestaurantsUserControllerCreateRemoteMethod]',
  exportAs: 'reviewRestaurantsUserControllerCreateRemoteMethod',
})
export class UserControllerCreateRemoteMethodDirective
  extends RemoteMethodDirective<void, OpenApiRemoteMethodParameter<void, UserControllerCreateRequestBody>> {
  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(UserControllerCreateRemoteMethod) remoteMethod: UserControllerCreateRemoteMethod,
  ) {
    super(remoteMethodLoader, injector);
    this.remoteMethodOrIdOrToken = remoteMethod;
  }
}

@NgModule({
  declarations: [UserControllerCreateRemoteMethodDirective],
  exports:      [UserControllerCreateRemoteMethodDirective],
})
export class UserControllerCreateRemoteMethodDirectiveModule {
}
