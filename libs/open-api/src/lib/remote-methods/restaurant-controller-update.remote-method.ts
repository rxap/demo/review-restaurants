import {
  ChangeDetectorRef,
  Directive,
  Inject,
  Injectable,
  INJECTOR,
  Injector,
  Input,
  NgModule,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';
import {
  OpenApiRemoteMethod,
  OpenApiRemoteMethodParameter,
  RxapOpenApiRemoteMethod,
} from '@rxap/open-api/remote-method';
import { RemoteMethodLoader } from '@rxap/remote-method';
import {
  RemoteMethodDirective,
  RemoteMethodTemplateDirective,
  RemoteMethodTemplateDirectiveContext,
  RemoteMethodTemplateDirectiveErrorContext,
} from '@rxap/remote-method/directive';
import { RestaurantControllerUpdateParameter } from '../parameters/restaurant-controller-update.parameter';
import { RestaurantControllerUpdateRequestBody } from '../request-bodies/restaurant-controller-update.request-body';

@Injectable({
  providedIn: 'root',
})
@RxapOpenApiRemoteMethod('RestaurantController_update')
export class RestaurantControllerUpdateRemoteMethod
  extends OpenApiRemoteMethod<void, RestaurantControllerUpdateParameter, RestaurantControllerUpdateRequestBody> {
  public call(parameters: OpenApiRemoteMethodParameter<RestaurantControllerUpdateParameter, RestaurantControllerUpdateRequestBody>): Promise<void> {
    return super.call(parameters);
  }
}

@Directive({
  selector: '[reviewRestaurantsRestaurantControllerUpdateRemoteMethod]',
  exportAs: 'reviewRestaurantsRestaurantControllerUpdateRemoteMethod',
})
export class RestaurantControllerUpdateRemoteMethodTemplateDirective
  extends RemoteMethodTemplateDirective<void, OpenApiRemoteMethodParameter<RestaurantControllerUpdateParameter, RestaurantControllerUpdateRequestBody>> {
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsRestaurantControllerUpdateRemoteMethodParameters')
  public parameters?: OpenApiRemoteMethodParameter<RestaurantControllerUpdateParameter, RestaurantControllerUpdateRequestBody>;
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsRestaurantControllerUpdateRemoteMethodError')
  public errorTemplate?: TemplateRef<RemoteMethodTemplateDirectiveErrorContext>;

  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(RestaurantControllerUpdateRemoteMethod) remoteMethod: RestaurantControllerUpdateRemoteMethod,
    @Inject(TemplateRef) template: TemplateRef<RemoteMethodTemplateDirectiveContext<void>>,
    @Inject(ViewContainerRef) viewContainerRef: ViewContainerRef,
    @Inject(ChangeDetectorRef) cdr: ChangeDetectorRef,
  ) {
    super(template, remoteMethodLoader, injector, viewContainerRef, cdr);
    this.remoteMethodOrIdOrToken = remoteMethod;
  }
}

@NgModule({
  declarations: [RestaurantControllerUpdateRemoteMethodTemplateDirective],
  exports:      [RestaurantControllerUpdateRemoteMethodTemplateDirective],
})
export class RestaurantControllerUpdateRemoteMethodTemplateDirectiveModule {
}

@Directive({
  selector: '[reviewRestaurantsRestaurantControllerUpdateRemoteMethod]',
  exportAs: 'reviewRestaurantsRestaurantControllerUpdateRemoteMethod',
})
export class RestaurantControllerUpdateRemoteMethodDirective
  extends RemoteMethodDirective<void, OpenApiRemoteMethodParameter<RestaurantControllerUpdateParameter, RestaurantControllerUpdateRequestBody>> {
  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(RestaurantControllerUpdateRemoteMethod) remoteMethod: RestaurantControllerUpdateRemoteMethod,
  ) {
    super(remoteMethodLoader, injector);
    this.remoteMethodOrIdOrToken = remoteMethod;
  }
}

@NgModule({
  declarations: [RestaurantControllerUpdateRemoteMethodDirective],
  exports:      [RestaurantControllerUpdateRemoteMethodDirective],
})
export class RestaurantControllerUpdateRemoteMethodDirectiveModule {
}
