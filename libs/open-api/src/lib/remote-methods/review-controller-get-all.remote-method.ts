import {
  ChangeDetectorRef,
  Directive,
  Inject,
  Injectable,
  INJECTOR,
  Injector,
  Input,
  IterableDiffers,
  NgModule,
  NgZone,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';
import {
  OpenApiRemoteMethod,
  OpenApiRemoteMethodParameter,
  RxapOpenApiRemoteMethod,
} from '@rxap/open-api/remote-method';
import { RemoteMethodLoader } from '@rxap/remote-method';
import {
  RemoteMethodDirective,
  RemoteMethodTemplateCollectionDirective,
  RemoteMethodTemplateCollectionDirectiveContext,
  RemoteMethodTemplateCollectionDirectiveErrorContext,
  RemoteMethodTemplateDirective,
  RemoteMethodTemplateDirectiveContext,
  RemoteMethodTemplateDirectiveErrorContext,
} from '@rxap/remote-method/directive';
import { ArrayElement } from '@rxap/utilities';
import { ReviewControllerGetAllParameter } from '../parameters/review-controller-get-all.parameter';
import { ReviewControllerGetAllResponse } from '../responses/review-controller-get-all.response';

@Injectable({
  providedIn: 'root',
})
@RxapOpenApiRemoteMethod('ReviewController_getAll')
export class ReviewControllerGetAllRemoteMethod
  extends OpenApiRemoteMethod<ReviewControllerGetAllResponse, ReviewControllerGetAllParameter, void> {
  public call(parameters: OpenApiRemoteMethodParameter<ReviewControllerGetAllParameter, void>): Promise<ReviewControllerGetAllResponse> {
    return super.call(parameters);
  }
}

@Directive({
  selector: '[reviewRestaurantsReviewControllerGetAllCollectionRemoteMethod]',
  exportAs: 'reviewRestaurantsReviewControllerGetAllCollectionRemoteMethod',
})
export class ReviewControllerGetAllRemoteMethodTemplateCollectionDirective
  extends RemoteMethodTemplateCollectionDirective<ArrayElement<ReviewControllerGetAllResponse>, OpenApiRemoteMethodParameter<ReviewControllerGetAllParameter, void>> {
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsReviewControllerGetAllCollectionRemoteMethodParameters')
  public parameters?: OpenApiRemoteMethodParameter<ReviewControllerGetAllParameter, void>;
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsReviewControllerGetAllCollectionRemoteMethodError')
  public errorTemplate?: TemplateRef<RemoteMethodTemplateCollectionDirectiveErrorContext>;
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsReviewControllerGetAllCollectionRemoteMethodEmpty')
  public emptyTemplate?: TemplateRef<void>;

  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(ReviewControllerGetAllRemoteMethod) remoteMethod: ReviewControllerGetAllRemoteMethod,
    @Inject(TemplateRef) template: TemplateRef<RemoteMethodTemplateCollectionDirectiveContext<ArrayElement<ReviewControllerGetAllResponse>>>,
    @Inject(IterableDiffers) differs: IterableDiffers,
    @Inject(NgZone) zone: NgZone,
    @Inject(ViewContainerRef) viewContainerRef: ViewContainerRef,
    @Inject(ChangeDetectorRef) cdr: ChangeDetectorRef,
  ) {
    super(template, remoteMethodLoader, injector, viewContainerRef, cdr, differs, zone);
    this.remoteMethodOrIdOrToken = remoteMethod;
  }
}

@NgModule({
  declarations: [ReviewControllerGetAllRemoteMethodTemplateCollectionDirective],
  exports:      [ReviewControllerGetAllRemoteMethodTemplateCollectionDirective],
})
export class ReviewControllerGetAllRemoteMethodTemplateCollectionDirectiveModule {
}

@Directive({
  selector: '[reviewRestaurantsReviewControllerGetAllRemoteMethod]',
  exportAs: 'reviewRestaurantsReviewControllerGetAllRemoteMethod',
})
export class ReviewControllerGetAllRemoteMethodTemplateDirective
  extends RemoteMethodTemplateDirective<ReviewControllerGetAllResponse, OpenApiRemoteMethodParameter<ReviewControllerGetAllParameter, void>> {
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsReviewControllerGetAllRemoteMethodParameters')
  public parameters?: OpenApiRemoteMethodParameter<ReviewControllerGetAllParameter, void>;
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsReviewControllerGetAllRemoteMethodError')
  public errorTemplate?: TemplateRef<RemoteMethodTemplateDirectiveErrorContext>;

  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(ReviewControllerGetAllRemoteMethod) remoteMethod: ReviewControllerGetAllRemoteMethod,
    @Inject(TemplateRef) template: TemplateRef<RemoteMethodTemplateDirectiveContext<ReviewControllerGetAllResponse>>,
    @Inject(ViewContainerRef) viewContainerRef: ViewContainerRef,
    @Inject(ChangeDetectorRef) cdr: ChangeDetectorRef,
  ) {
    super(template, remoteMethodLoader, injector, viewContainerRef, cdr);
    this.remoteMethodOrIdOrToken = remoteMethod;
  }
}

@NgModule({
  declarations: [ReviewControllerGetAllRemoteMethodTemplateDirective],
  exports:      [ReviewControllerGetAllRemoteMethodTemplateDirective],
})
export class ReviewControllerGetAllRemoteMethodTemplateDirectiveModule {
}

@Directive({
  selector: '[reviewRestaurantsReviewControllerGetAllRemoteMethod]',
  exportAs: 'reviewRestaurantsReviewControllerGetAllRemoteMethod',
})
export class ReviewControllerGetAllRemoteMethodDirective
  extends RemoteMethodDirective<ReviewControllerGetAllResponse, OpenApiRemoteMethodParameter<ReviewControllerGetAllParameter, void>> {
  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(ReviewControllerGetAllRemoteMethod) remoteMethod: ReviewControllerGetAllRemoteMethod,
  ) {
    super(remoteMethodLoader, injector);
    this.remoteMethodOrIdOrToken = remoteMethod;
  }
}

@NgModule({
  declarations: [ReviewControllerGetAllRemoteMethodDirective],
  exports:      [ReviewControllerGetAllRemoteMethodDirective],
})
export class ReviewControllerGetAllRemoteMethodDirectiveModule {
}
