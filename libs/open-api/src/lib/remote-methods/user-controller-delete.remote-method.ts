import {
  ChangeDetectorRef,
  Directive,
  Inject,
  Injectable,
  INJECTOR,
  Injector,
  Input,
  NgModule,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';
import {
  OpenApiRemoteMethod,
  OpenApiRemoteMethodParameter,
  RxapOpenApiRemoteMethod,
} from '@rxap/open-api/remote-method';
import { RemoteMethodLoader } from '@rxap/remote-method';
import {
  RemoteMethodDirective,
  RemoteMethodTemplateDirective,
  RemoteMethodTemplateDirectiveContext,
  RemoteMethodTemplateDirectiveErrorContext,
} from '@rxap/remote-method/directive';
import { UserControllerDeleteParameter } from '../parameters/user-controller-delete.parameter';

@Injectable({
  providedIn: 'root',
})
@RxapOpenApiRemoteMethod('UserController_delete')
export class UserControllerDeleteRemoteMethod extends OpenApiRemoteMethod<void, UserControllerDeleteParameter, void> {
  public call(parameters: OpenApiRemoteMethodParameter<UserControllerDeleteParameter, void>): Promise<void> {
    return super.call(parameters);
  }
}

@Directive({
  selector: '[reviewRestaurantsUserControllerDeleteRemoteMethod]',
  exportAs: 'reviewRestaurantsUserControllerDeleteRemoteMethod',
})
export class UserControllerDeleteRemoteMethodTemplateDirective
  extends RemoteMethodTemplateDirective<void, OpenApiRemoteMethodParameter<UserControllerDeleteParameter, void>> {
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsUserControllerDeleteRemoteMethodParameters')
  public parameters?: OpenApiRemoteMethodParameter<UserControllerDeleteParameter, void>;
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsUserControllerDeleteRemoteMethodError')
  public errorTemplate?: TemplateRef<RemoteMethodTemplateDirectiveErrorContext>;

  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(UserControllerDeleteRemoteMethod) remoteMethod: UserControllerDeleteRemoteMethod,
    @Inject(TemplateRef) template: TemplateRef<RemoteMethodTemplateDirectiveContext<void>>,
    @Inject(ViewContainerRef) viewContainerRef: ViewContainerRef,
    @Inject(ChangeDetectorRef) cdr: ChangeDetectorRef,
  ) {
    super(template, remoteMethodLoader, injector, viewContainerRef, cdr);
    this.remoteMethodOrIdOrToken = remoteMethod;
  }
}

@NgModule({
  declarations: [UserControllerDeleteRemoteMethodTemplateDirective],
  exports:      [UserControllerDeleteRemoteMethodTemplateDirective],
})
export class UserControllerDeleteRemoteMethodTemplateDirectiveModule {
}

@Directive({
  selector: '[reviewRestaurantsUserControllerDeleteRemoteMethod]',
  exportAs: 'reviewRestaurantsUserControllerDeleteRemoteMethod',
})
export class UserControllerDeleteRemoteMethodDirective
  extends RemoteMethodDirective<void, OpenApiRemoteMethodParameter<UserControllerDeleteParameter, void>> {
  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(UserControllerDeleteRemoteMethod) remoteMethod: UserControllerDeleteRemoteMethod,
  ) {
    super(remoteMethodLoader, injector);
    this.remoteMethodOrIdOrToken = remoteMethod;
  }
}

@NgModule({
  declarations: [UserControllerDeleteRemoteMethodDirective],
  exports:      [UserControllerDeleteRemoteMethodDirective],
})
export class UserControllerDeleteRemoteMethodDirectiveModule {
}
