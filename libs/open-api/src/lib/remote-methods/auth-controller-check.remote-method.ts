import {
  ChangeDetectorRef,
  Directive,
  Inject,
  Injectable,
  INJECTOR,
  Injector,
  Input,
  NgModule,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';
import {
  OpenApiRemoteMethod,
  OpenApiRemoteMethodParameter,
  RxapOpenApiRemoteMethod,
} from '@rxap/open-api/remote-method';
import { RemoteMethodLoader } from '@rxap/remote-method';
import {
  RemoteMethodDirective,
  RemoteMethodTemplateDirective,
  RemoteMethodTemplateDirectiveContext,
  RemoteMethodTemplateDirectiveErrorContext,
} from '@rxap/remote-method/directive';
import { AuthControllerCheckResponse } from '../responses/auth-controller-check.response';

@Injectable({
  providedIn: 'root',
})
@RxapOpenApiRemoteMethod('AuthController_check')
export class AuthControllerCheckRemoteMethod extends OpenApiRemoteMethod<AuthControllerCheckResponse, void, void> {
  public call(): Promise<AuthControllerCheckResponse> {
    return super.call();
  }
}

@Directive({
  selector: '[reviewRestaurantsAuthControllerCheckRemoteMethod]',
  exportAs: 'reviewRestaurantsAuthControllerCheckRemoteMethod',
})
export class AuthControllerCheckRemoteMethodTemplateDirective
  extends RemoteMethodTemplateDirective<AuthControllerCheckResponse, OpenApiRemoteMethodParameter<void, void>> {
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsAuthControllerCheckRemoteMethodParameters')
  public parameters?: OpenApiRemoteMethodParameter<void, void>;
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsAuthControllerCheckRemoteMethodError')
  public errorTemplate?: TemplateRef<RemoteMethodTemplateDirectiveErrorContext>;

  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(AuthControllerCheckRemoteMethod) remoteMethod: AuthControllerCheckRemoteMethod,
    @Inject(TemplateRef) template: TemplateRef<RemoteMethodTemplateDirectiveContext<AuthControllerCheckResponse>>,
    @Inject(ViewContainerRef) viewContainerRef: ViewContainerRef,
    @Inject(ChangeDetectorRef) cdr: ChangeDetectorRef,
  ) {
    super(template, remoteMethodLoader, injector, viewContainerRef, cdr);
    this.remoteMethodOrIdOrToken = remoteMethod;
    this.withoutParameters       = true;
  }
}

@NgModule({
  declarations: [AuthControllerCheckRemoteMethodTemplateDirective],
  exports:      [AuthControllerCheckRemoteMethodTemplateDirective],
})
export class AuthControllerCheckRemoteMethodTemplateDirectiveModule {
}

@Directive({
  selector: '[reviewRestaurantsAuthControllerCheckRemoteMethod]',
  exportAs: 'reviewRestaurantsAuthControllerCheckRemoteMethod',
})
export class AuthControllerCheckRemoteMethodDirective
  extends RemoteMethodDirective<AuthControllerCheckResponse, OpenApiRemoteMethodParameter<void, void>> {
  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(AuthControllerCheckRemoteMethod) remoteMethod: AuthControllerCheckRemoteMethod,
  ) {
    super(remoteMethodLoader, injector);
    this.remoteMethodOrIdOrToken = remoteMethod;
  }
}

@NgModule({
  declarations: [AuthControllerCheckRemoteMethodDirective],
  exports:      [AuthControllerCheckRemoteMethodDirective],
})
export class AuthControllerCheckRemoteMethodDirectiveModule {
}
