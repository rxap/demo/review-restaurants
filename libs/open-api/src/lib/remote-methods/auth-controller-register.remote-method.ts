import {
  ChangeDetectorRef,
  Directive,
  Inject,
  Injectable,
  INJECTOR,
  Injector,
  Input,
  NgModule,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';
import {
  OpenApiRemoteMethod,
  OpenApiRemoteMethodParameter,
  RxapOpenApiRemoteMethod,
} from '@rxap/open-api/remote-method';
import { RemoteMethodLoader } from '@rxap/remote-method';
import {
  RemoteMethodDirective,
  RemoteMethodTemplateDirective,
  RemoteMethodTemplateDirectiveContext,
  RemoteMethodTemplateDirectiveErrorContext,
} from '@rxap/remote-method/directive';
import { AuthControllerRegisterRequestBody } from '../request-bodies/auth-controller-register.request-body';

@Injectable({
  providedIn: 'root',
})
@RxapOpenApiRemoteMethod('AuthController_register')
export class AuthControllerRegisterRemoteMethod
  extends OpenApiRemoteMethod<void, void, AuthControllerRegisterRequestBody> {
  public call(parameters: OpenApiRemoteMethodParameter<void, AuthControllerRegisterRequestBody>): Promise<void> {
    return super.call(parameters);
  }
}

@Directive({
  selector: '[reviewRestaurantsAuthControllerRegisterRemoteMethod]',
  exportAs: 'reviewRestaurantsAuthControllerRegisterRemoteMethod',
})
export class AuthControllerRegisterRemoteMethodTemplateDirective
  extends RemoteMethodTemplateDirective<void, OpenApiRemoteMethodParameter<void, AuthControllerRegisterRequestBody>> {
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsAuthControllerRegisterRemoteMethodParameters')
  public parameters?: OpenApiRemoteMethodParameter<void, AuthControllerRegisterRequestBody>;
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsAuthControllerRegisterRemoteMethodError')
  public errorTemplate?: TemplateRef<RemoteMethodTemplateDirectiveErrorContext>;

  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(AuthControllerRegisterRemoteMethod) remoteMethod: AuthControllerRegisterRemoteMethod,
    @Inject(TemplateRef) template: TemplateRef<RemoteMethodTemplateDirectiveContext<void>>,
    @Inject(ViewContainerRef) viewContainerRef: ViewContainerRef,
    @Inject(ChangeDetectorRef) cdr: ChangeDetectorRef,
  ) {
    super(template, remoteMethodLoader, injector, viewContainerRef, cdr);
    this.remoteMethodOrIdOrToken = remoteMethod;
    this.withoutParameters       = true;
  }
}

@NgModule({
  declarations: [AuthControllerRegisterRemoteMethodTemplateDirective],
  exports:      [AuthControllerRegisterRemoteMethodTemplateDirective],
})
export class AuthControllerRegisterRemoteMethodTemplateDirectiveModule {
}

@Directive({
  selector: '[reviewRestaurantsAuthControllerRegisterRemoteMethod]',
  exportAs: 'reviewRestaurantsAuthControllerRegisterRemoteMethod',
})
export class AuthControllerRegisterRemoteMethodDirective
  extends RemoteMethodDirective<void, OpenApiRemoteMethodParameter<void, AuthControllerRegisterRequestBody>> {
  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(AuthControllerRegisterRemoteMethod) remoteMethod: AuthControllerRegisterRemoteMethod,
  ) {
    super(remoteMethodLoader, injector);
    this.remoteMethodOrIdOrToken = remoteMethod;
  }
}

@NgModule({
  declarations: [AuthControllerRegisterRemoteMethodDirective],
  exports:      [AuthControllerRegisterRemoteMethodDirective],
})
export class AuthControllerRegisterRemoteMethodDirectiveModule {
}
