import {
  ChangeDetectorRef,
  Directive,
  Inject,
  Injectable,
  INJECTOR,
  Injector,
  Input,
  IterableDiffers,
  NgModule,
  NgZone,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';
import {
  OpenApiRemoteMethod,
  OpenApiRemoteMethodParameter,
  RxapOpenApiRemoteMethod,
} from '@rxap/open-api/remote-method';
import { RemoteMethodLoader } from '@rxap/remote-method';
import {
  RemoteMethodDirective,
  RemoteMethodTemplateCollectionDirective,
  RemoteMethodTemplateCollectionDirectiveContext,
  RemoteMethodTemplateCollectionDirectiveErrorContext,
  RemoteMethodTemplateDirective,
  RemoteMethodTemplateDirectiveContext,
  RemoteMethodTemplateDirectiveErrorContext,
} from '@rxap/remote-method/directive';
import { ArrayElement } from '@rxap/utilities';
import { RestaurantControllerGetAllResponse } from '../responses/restaurant-controller-get-all.response';

@Injectable({
  providedIn: 'root',
})
@RxapOpenApiRemoteMethod('RestaurantController_getAll')
export class RestaurantControllerGetAllRemoteMethod
  extends OpenApiRemoteMethod<RestaurantControllerGetAllResponse, void, void> {
  public call(): Promise<RestaurantControllerGetAllResponse> {
    return super.call();
  }
}

@Directive({
  selector: '[reviewRestaurantsRestaurantControllerGetAllCollectionRemoteMethod]',
  exportAs: 'reviewRestaurantsRestaurantControllerGetAllCollectionRemoteMethod',
})
export class RestaurantControllerGetAllRemoteMethodTemplateCollectionDirective
  extends RemoteMethodTemplateCollectionDirective<ArrayElement<RestaurantControllerGetAllResponse>, OpenApiRemoteMethodParameter<void, void>> {
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsRestaurantControllerGetAllCollectionRemoteMethodParameters')
  public parameters?: OpenApiRemoteMethodParameter<void, void>;
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsRestaurantControllerGetAllCollectionRemoteMethodError')
  public errorTemplate?: TemplateRef<RemoteMethodTemplateCollectionDirectiveErrorContext>;
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsRestaurantControllerGetAllCollectionRemoteMethodEmpty')
  public emptyTemplate?: TemplateRef<void>;

  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(RestaurantControllerGetAllRemoteMethod) remoteMethod: RestaurantControllerGetAllRemoteMethod,
    @Inject(TemplateRef) template: TemplateRef<RemoteMethodTemplateCollectionDirectiveContext<ArrayElement<RestaurantControllerGetAllResponse>>>,
    @Inject(IterableDiffers) differs: IterableDiffers,
    @Inject(NgZone) zone: NgZone,
    @Inject(ViewContainerRef) viewContainerRef: ViewContainerRef,
    @Inject(ChangeDetectorRef) cdr: ChangeDetectorRef,
  ) {
    super(template, remoteMethodLoader, injector, viewContainerRef, cdr, differs, zone);
    this.remoteMethodOrIdOrToken = remoteMethod;
    this.withoutParameters       = true;
  }
}

@NgModule({
  declarations: [RestaurantControllerGetAllRemoteMethodTemplateCollectionDirective],
  exports:      [RestaurantControllerGetAllRemoteMethodTemplateCollectionDirective],
})
export class RestaurantControllerGetAllRemoteMethodTemplateCollectionDirectiveModule {
}

@Directive({
  selector: '[reviewRestaurantsRestaurantControllerGetAllRemoteMethod]',
  exportAs: 'reviewRestaurantsRestaurantControllerGetAllRemoteMethod',
})
export class RestaurantControllerGetAllRemoteMethodTemplateDirective
  extends RemoteMethodTemplateDirective<RestaurantControllerGetAllResponse, OpenApiRemoteMethodParameter<void, void>> {
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsRestaurantControllerGetAllRemoteMethodParameters')
  public parameters?: OpenApiRemoteMethodParameter<void, void>;
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsRestaurantControllerGetAllRemoteMethodError')
  public errorTemplate?: TemplateRef<RemoteMethodTemplateDirectiveErrorContext>;

  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(RestaurantControllerGetAllRemoteMethod) remoteMethod: RestaurantControllerGetAllRemoteMethod,
    @Inject(TemplateRef) template: TemplateRef<RemoteMethodTemplateDirectiveContext<RestaurantControllerGetAllResponse>>,
    @Inject(ViewContainerRef) viewContainerRef: ViewContainerRef,
    @Inject(ChangeDetectorRef) cdr: ChangeDetectorRef,
  ) {
    super(template, remoteMethodLoader, injector, viewContainerRef, cdr);
    this.remoteMethodOrIdOrToken = remoteMethod;
    this.withoutParameters       = true;
  }
}

@NgModule({
  declarations: [RestaurantControllerGetAllRemoteMethodTemplateDirective],
  exports:      [RestaurantControllerGetAllRemoteMethodTemplateDirective],
})
export class RestaurantControllerGetAllRemoteMethodTemplateDirectiveModule {
}

@Directive({
  selector: '[reviewRestaurantsRestaurantControllerGetAllRemoteMethod]',
  exportAs: 'reviewRestaurantsRestaurantControllerGetAllRemoteMethod',
})
export class RestaurantControllerGetAllRemoteMethodDirective
  extends RemoteMethodDirective<RestaurantControllerGetAllResponse, OpenApiRemoteMethodParameter<void, void>> {
  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(RestaurantControllerGetAllRemoteMethod) remoteMethod: RestaurantControllerGetAllRemoteMethod,
  ) {
    super(remoteMethodLoader, injector);
    this.remoteMethodOrIdOrToken = remoteMethod;
  }
}

@NgModule({
  declarations: [RestaurantControllerGetAllRemoteMethodDirective],
  exports:      [RestaurantControllerGetAllRemoteMethodDirective],
})
export class RestaurantControllerGetAllRemoteMethodDirectiveModule {
}
