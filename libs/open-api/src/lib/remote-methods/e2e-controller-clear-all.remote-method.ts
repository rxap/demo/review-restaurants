import {
  ChangeDetectorRef,
  Directive,
  Inject,
  Injectable,
  INJECTOR,
  Injector,
  Input,
  NgModule,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';
import {
  OpenApiRemoteMethod,
  OpenApiRemoteMethodParameter,
  RxapOpenApiRemoteMethod,
} from '@rxap/open-api/remote-method';
import { RemoteMethodLoader } from '@rxap/remote-method';
import {
  RemoteMethodDirective,
  RemoteMethodTemplateDirective,
  RemoteMethodTemplateDirectiveContext,
  RemoteMethodTemplateDirectiveErrorContext,
} from '@rxap/remote-method/directive';

@Injectable({
  providedIn: 'root',
})
@RxapOpenApiRemoteMethod('E2eController_clearAll')
export class E2eControllerClearAllRemoteMethod extends OpenApiRemoteMethod<void, void, void> {
  public call(): Promise<void> {
    return super.call();
  }
}

@Directive({
  selector: '[reviewRestaurantsE2eControllerClearAllRemoteMethod]',
  exportAs: 'reviewRestaurantsE2eControllerClearAllRemoteMethod',
})
export class E2eControllerClearAllRemoteMethodTemplateDirective
  extends RemoteMethodTemplateDirective<void, OpenApiRemoteMethodParameter<void, void>> {
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsE2eControllerClearAllRemoteMethodParameters')
  public parameters?: OpenApiRemoteMethodParameter<void, void>;
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsE2eControllerClearAllRemoteMethodError')
  public errorTemplate?: TemplateRef<RemoteMethodTemplateDirectiveErrorContext>;

  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(E2eControllerClearAllRemoteMethod) remoteMethod: E2eControllerClearAllRemoteMethod,
    @Inject(TemplateRef) template: TemplateRef<RemoteMethodTemplateDirectiveContext<void>>,
    @Inject(ViewContainerRef) viewContainerRef: ViewContainerRef,
    @Inject(ChangeDetectorRef) cdr: ChangeDetectorRef,
  ) {
    super(template, remoteMethodLoader, injector, viewContainerRef, cdr);
    this.remoteMethodOrIdOrToken = remoteMethod;
    this.withoutParameters       = true;
  }
}

@NgModule({
  declarations: [E2eControllerClearAllRemoteMethodTemplateDirective],
  exports:      [E2eControllerClearAllRemoteMethodTemplateDirective],
})
export class E2eControllerClearAllRemoteMethodTemplateDirectiveModule {
}

@Directive({
  selector: '[reviewRestaurantsE2eControllerClearAllRemoteMethod]',
  exportAs: 'reviewRestaurantsE2eControllerClearAllRemoteMethod',
})
export class E2eControllerClearAllRemoteMethodDirective
  extends RemoteMethodDirective<void, OpenApiRemoteMethodParameter<void, void>> {
  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(E2eControllerClearAllRemoteMethod) remoteMethod: E2eControllerClearAllRemoteMethod,
  ) {
    super(remoteMethodLoader, injector);
    this.remoteMethodOrIdOrToken = remoteMethod;
  }
}

@NgModule({
  declarations: [E2eControllerClearAllRemoteMethodDirective],
  exports:      [E2eControllerClearAllRemoteMethodDirective],
})
export class E2eControllerClearAllRemoteMethodDirectiveModule {
}
