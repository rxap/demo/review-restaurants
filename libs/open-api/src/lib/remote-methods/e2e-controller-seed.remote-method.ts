import {
  ChangeDetectorRef,
  Directive,
  Inject,
  Injectable,
  INJECTOR,
  Injector,
  Input,
  NgModule,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';
import {
  OpenApiRemoteMethod,
  OpenApiRemoteMethodParameter,
  RxapOpenApiRemoteMethod,
} from '@rxap/open-api/remote-method';
import { RemoteMethodLoader } from '@rxap/remote-method';
import {
  RemoteMethodDirective,
  RemoteMethodTemplateDirective,
  RemoteMethodTemplateDirectiveContext,
  RemoteMethodTemplateDirectiveErrorContext,
} from '@rxap/remote-method/directive';

@Injectable({
  providedIn: 'root',
})
@RxapOpenApiRemoteMethod('E2eController_seed')
export class E2eControllerSeedRemoteMethod extends OpenApiRemoteMethod<void, void, void> {
  public call(): Promise<void> {
    return super.call();
  }
}

@Directive({
  selector: '[reviewRestaurantsE2eControllerSeedRemoteMethod]',
  exportAs: 'reviewRestaurantsE2eControllerSeedRemoteMethod',
})
export class E2eControllerSeedRemoteMethodTemplateDirective
  extends RemoteMethodTemplateDirective<void, OpenApiRemoteMethodParameter<void, void>> {
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsE2eControllerSeedRemoteMethodParameters')
  public parameters?: OpenApiRemoteMethodParameter<void, void>;
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsE2eControllerSeedRemoteMethodError')
  public errorTemplate?: TemplateRef<RemoteMethodTemplateDirectiveErrorContext>;

  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(E2eControllerSeedRemoteMethod) remoteMethod: E2eControllerSeedRemoteMethod,
    @Inject(TemplateRef) template: TemplateRef<RemoteMethodTemplateDirectiveContext<void>>,
    @Inject(ViewContainerRef) viewContainerRef: ViewContainerRef,
    @Inject(ChangeDetectorRef) cdr: ChangeDetectorRef,
  ) {
    super(template, remoteMethodLoader, injector, viewContainerRef, cdr);
    this.remoteMethodOrIdOrToken = remoteMethod;
    this.withoutParameters       = true;
  }
}

@NgModule({
  declarations: [E2eControllerSeedRemoteMethodTemplateDirective],
  exports:      [E2eControllerSeedRemoteMethodTemplateDirective],
})
export class E2eControllerSeedRemoteMethodTemplateDirectiveModule {
}

@Directive({
  selector: '[reviewRestaurantsE2eControllerSeedRemoteMethod]',
  exportAs: 'reviewRestaurantsE2eControllerSeedRemoteMethod',
})
export class E2eControllerSeedRemoteMethodDirective
  extends RemoteMethodDirective<void, OpenApiRemoteMethodParameter<void, void>> {
  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(E2eControllerSeedRemoteMethod) remoteMethod: E2eControllerSeedRemoteMethod,
  ) {
    super(remoteMethodLoader, injector);
    this.remoteMethodOrIdOrToken = remoteMethod;
  }
}

@NgModule({
  declarations: [E2eControllerSeedRemoteMethodDirective],
  exports:      [E2eControllerSeedRemoteMethodDirective],
})
export class E2eControllerSeedRemoteMethodDirectiveModule {
}
