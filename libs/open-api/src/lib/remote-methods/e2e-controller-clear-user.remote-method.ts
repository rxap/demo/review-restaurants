import {
  ChangeDetectorRef,
  Directive,
  Inject,
  Injectable,
  INJECTOR,
  Injector,
  Input,
  NgModule,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';
import {
  OpenApiRemoteMethod,
  OpenApiRemoteMethodParameter,
  RxapOpenApiRemoteMethod,
} from '@rxap/open-api/remote-method';
import { RemoteMethodLoader } from '@rxap/remote-method';
import {
  RemoteMethodDirective,
  RemoteMethodTemplateDirective,
  RemoteMethodTemplateDirectiveContext,
  RemoteMethodTemplateDirectiveErrorContext,
} from '@rxap/remote-method/directive';

@Injectable({
  providedIn: 'root',
})
@RxapOpenApiRemoteMethod('E2eController_clearUser')
export class E2eControllerClearUserRemoteMethod extends OpenApiRemoteMethod<void, void, void> {
  public call(): Promise<void> {
    return super.call();
  }
}

@Directive({
  selector: '[reviewRestaurantsE2eControllerClearUserRemoteMethod]',
  exportAs: 'reviewRestaurantsE2eControllerClearUserRemoteMethod',
})
export class E2eControllerClearUserRemoteMethodTemplateDirective
  extends RemoteMethodTemplateDirective<void, OpenApiRemoteMethodParameter<void, void>> {
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsE2eControllerClearUserRemoteMethodParameters')
  public parameters?: OpenApiRemoteMethodParameter<void, void>;
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsE2eControllerClearUserRemoteMethodError')
  public errorTemplate?: TemplateRef<RemoteMethodTemplateDirectiveErrorContext>;

  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(E2eControllerClearUserRemoteMethod) remoteMethod: E2eControllerClearUserRemoteMethod,
    @Inject(TemplateRef) template: TemplateRef<RemoteMethodTemplateDirectiveContext<void>>,
    @Inject(ViewContainerRef) viewContainerRef: ViewContainerRef,
    @Inject(ChangeDetectorRef) cdr: ChangeDetectorRef,
  ) {
    super(template, remoteMethodLoader, injector, viewContainerRef, cdr);
    this.remoteMethodOrIdOrToken = remoteMethod;
    this.withoutParameters       = true;
  }
}

@NgModule({
  declarations: [E2eControllerClearUserRemoteMethodTemplateDirective],
  exports:      [E2eControllerClearUserRemoteMethodTemplateDirective],
})
export class E2eControllerClearUserRemoteMethodTemplateDirectiveModule {
}

@Directive({
  selector: '[reviewRestaurantsE2eControllerClearUserRemoteMethod]',
  exportAs: 'reviewRestaurantsE2eControllerClearUserRemoteMethod',
})
export class E2eControllerClearUserRemoteMethodDirective
  extends RemoteMethodDirective<void, OpenApiRemoteMethodParameter<void, void>> {
  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(E2eControllerClearUserRemoteMethod) remoteMethod: E2eControllerClearUserRemoteMethod,
  ) {
    super(remoteMethodLoader, injector);
    this.remoteMethodOrIdOrToken = remoteMethod;
  }
}

@NgModule({
  declarations: [E2eControllerClearUserRemoteMethodDirective],
  exports:      [E2eControllerClearUserRemoteMethodDirective],
})
export class E2eControllerClearUserRemoteMethodDirectiveModule {
}
