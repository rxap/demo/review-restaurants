import {
  ChangeDetectorRef,
  Directive,
  Inject,
  Injectable,
  INJECTOR,
  Injector,
  Input,
  NgModule,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';
import {
  OpenApiRemoteMethod,
  OpenApiRemoteMethodParameter,
  RxapOpenApiRemoteMethod,
} from '@rxap/open-api/remote-method';
import { RemoteMethodLoader } from '@rxap/remote-method';
import {
  RemoteMethodDirective,
  RemoteMethodTemplateDirective,
  RemoteMethodTemplateDirectiveContext,
  RemoteMethodTemplateDirectiveErrorContext,
} from '@rxap/remote-method/directive';
import { HealthControllerDatabaseResponse } from '../responses/health-controller-database.response';

@Injectable({
  providedIn: 'root',
})
@RxapOpenApiRemoteMethod('HealthController_database')
export class HealthControllerDatabaseRemoteMethod
  extends OpenApiRemoteMethod<HealthControllerDatabaseResponse, void, void> {
  public call(): Promise<HealthControllerDatabaseResponse> {
    return super.call();
  }
}

@Directive({
  selector: '[reviewRestaurantsHealthControllerDatabaseRemoteMethod]',
  exportAs: 'reviewRestaurantsHealthControllerDatabaseRemoteMethod',
})
export class HealthControllerDatabaseRemoteMethodTemplateDirective
  extends RemoteMethodTemplateDirective<HealthControllerDatabaseResponse, OpenApiRemoteMethodParameter<void, void>> {
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsHealthControllerDatabaseRemoteMethodParameters')
  public parameters?: OpenApiRemoteMethodParameter<void, void>;
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsHealthControllerDatabaseRemoteMethodError')
  public errorTemplate?: TemplateRef<RemoteMethodTemplateDirectiveErrorContext>;

  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(HealthControllerDatabaseRemoteMethod) remoteMethod: HealthControllerDatabaseRemoteMethod,
    @Inject(TemplateRef) template: TemplateRef<RemoteMethodTemplateDirectiveContext<HealthControllerDatabaseResponse>>,
    @Inject(ViewContainerRef) viewContainerRef: ViewContainerRef,
    @Inject(ChangeDetectorRef) cdr: ChangeDetectorRef,
  ) {
    super(template, remoteMethodLoader, injector, viewContainerRef, cdr);
    this.remoteMethodOrIdOrToken = remoteMethod;
    this.withoutParameters       = true;
  }
}

@NgModule({
  declarations: [HealthControllerDatabaseRemoteMethodTemplateDirective],
  exports:      [HealthControllerDatabaseRemoteMethodTemplateDirective],
})
export class HealthControllerDatabaseRemoteMethodTemplateDirectiveModule {
}

@Directive({
  selector: '[reviewRestaurantsHealthControllerDatabaseRemoteMethod]',
  exportAs: 'reviewRestaurantsHealthControllerDatabaseRemoteMethod',
})
export class HealthControllerDatabaseRemoteMethodDirective
  extends RemoteMethodDirective<HealthControllerDatabaseResponse, OpenApiRemoteMethodParameter<void, void>> {
  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(HealthControllerDatabaseRemoteMethod) remoteMethod: HealthControllerDatabaseRemoteMethod,
  ) {
    super(remoteMethodLoader, injector);
    this.remoteMethodOrIdOrToken = remoteMethod;
  }
}

@NgModule({
  declarations: [HealthControllerDatabaseRemoteMethodDirective],
  exports:      [HealthControllerDatabaseRemoteMethodDirective],
})
export class HealthControllerDatabaseRemoteMethodDirectiveModule {
}
