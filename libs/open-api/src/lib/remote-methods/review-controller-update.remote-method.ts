import {
  ChangeDetectorRef,
  Directive,
  Inject,
  Injectable,
  INJECTOR,
  Injector,
  Input,
  NgModule,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';
import {
  OpenApiRemoteMethod,
  OpenApiRemoteMethodParameter,
  RxapOpenApiRemoteMethod,
} from '@rxap/open-api/remote-method';
import { RemoteMethodLoader } from '@rxap/remote-method';
import {
  RemoteMethodDirective,
  RemoteMethodTemplateDirective,
  RemoteMethodTemplateDirectiveContext,
  RemoteMethodTemplateDirectiveErrorContext,
} from '@rxap/remote-method/directive';
import { ReviewControllerUpdateParameter } from '../parameters/review-controller-update.parameter';
import { ReviewControllerUpdateRequestBody } from '../request-bodies/review-controller-update.request-body';

@Injectable({
  providedIn: 'root',
})
@RxapOpenApiRemoteMethod('ReviewController_update')
export class ReviewControllerUpdateRemoteMethod
  extends OpenApiRemoteMethod<void, ReviewControllerUpdateParameter, ReviewControllerUpdateRequestBody> {
  public call(parameters: OpenApiRemoteMethodParameter<ReviewControllerUpdateParameter, ReviewControllerUpdateRequestBody>): Promise<void> {
    return super.call(parameters);
  }
}

@Directive({
  selector: '[reviewRestaurantsReviewControllerUpdateRemoteMethod]',
  exportAs: 'reviewRestaurantsReviewControllerUpdateRemoteMethod',
})
export class ReviewControllerUpdateRemoteMethodTemplateDirective
  extends RemoteMethodTemplateDirective<void, OpenApiRemoteMethodParameter<ReviewControllerUpdateParameter, ReviewControllerUpdateRequestBody>> {
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsReviewControllerUpdateRemoteMethodParameters')
  public parameters?: OpenApiRemoteMethodParameter<ReviewControllerUpdateParameter, ReviewControllerUpdateRequestBody>;
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsReviewControllerUpdateRemoteMethodError')
  public errorTemplate?: TemplateRef<RemoteMethodTemplateDirectiveErrorContext>;

  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(ReviewControllerUpdateRemoteMethod) remoteMethod: ReviewControllerUpdateRemoteMethod,
    @Inject(TemplateRef) template: TemplateRef<RemoteMethodTemplateDirectiveContext<void>>,
    @Inject(ViewContainerRef) viewContainerRef: ViewContainerRef,
    @Inject(ChangeDetectorRef) cdr: ChangeDetectorRef,
  ) {
    super(template, remoteMethodLoader, injector, viewContainerRef, cdr);
    this.remoteMethodOrIdOrToken = remoteMethod;
  }
}

@NgModule({
  declarations: [ReviewControllerUpdateRemoteMethodTemplateDirective],
  exports:      [ReviewControllerUpdateRemoteMethodTemplateDirective],
})
export class ReviewControllerUpdateRemoteMethodTemplateDirectiveModule {
}

@Directive({
  selector: '[reviewRestaurantsReviewControllerUpdateRemoteMethod]',
  exportAs: 'reviewRestaurantsReviewControllerUpdateRemoteMethod',
})
export class ReviewControllerUpdateRemoteMethodDirective
  extends RemoteMethodDirective<void, OpenApiRemoteMethodParameter<ReviewControllerUpdateParameter, ReviewControllerUpdateRequestBody>> {
  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(ReviewControllerUpdateRemoteMethod) remoteMethod: ReviewControllerUpdateRemoteMethod,
  ) {
    super(remoteMethodLoader, injector);
    this.remoteMethodOrIdOrToken = remoteMethod;
  }
}

@NgModule({
  declarations: [ReviewControllerUpdateRemoteMethodDirective],
  exports:      [ReviewControllerUpdateRemoteMethodDirective],
})
export class ReviewControllerUpdateRemoteMethodDirectiveModule {
}
