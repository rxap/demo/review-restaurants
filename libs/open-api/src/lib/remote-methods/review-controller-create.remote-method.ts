import {
  ChangeDetectorRef,
  Directive,
  Inject,
  Injectable,
  INJECTOR,
  Injector,
  Input,
  NgModule,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';
import {
  OpenApiRemoteMethod,
  OpenApiRemoteMethodParameter,
  RxapOpenApiRemoteMethod,
} from '@rxap/open-api/remote-method';
import { RemoteMethodLoader } from '@rxap/remote-method';
import {
  RemoteMethodDirective,
  RemoteMethodTemplateDirective,
  RemoteMethodTemplateDirectiveContext,
  RemoteMethodTemplateDirectiveErrorContext,
} from '@rxap/remote-method/directive';
import { ReviewControllerCreateParameter } from '../parameters/review-controller-create.parameter';
import { ReviewControllerCreateRequestBody } from '../request-bodies/review-controller-create.request-body';
import { ReviewControllerCreateResponse } from '../responses/review-controller-create.response';

@Injectable({
  providedIn: 'root',
})
@RxapOpenApiRemoteMethod('ReviewController_create')
export class ReviewControllerCreateRemoteMethod
  extends OpenApiRemoteMethod<ReviewControllerCreateResponse, ReviewControllerCreateParameter, ReviewControllerCreateRequestBody> {
  public call(parameters: OpenApiRemoteMethodParameter<ReviewControllerCreateParameter, ReviewControllerCreateRequestBody>): Promise<ReviewControllerCreateResponse> {
    return super.call(parameters);
  }
}

@Directive({
  selector: '[reviewRestaurantsReviewControllerCreateRemoteMethod]',
  exportAs: 'reviewRestaurantsReviewControllerCreateRemoteMethod',
})
export class ReviewControllerCreateRemoteMethodTemplateDirective
  extends RemoteMethodTemplateDirective<ReviewControllerCreateResponse, OpenApiRemoteMethodParameter<ReviewControllerCreateParameter, ReviewControllerCreateRequestBody>> {
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsReviewControllerCreateRemoteMethodParameters')
  public parameters?: OpenApiRemoteMethodParameter<ReviewControllerCreateParameter, ReviewControllerCreateRequestBody>;
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsReviewControllerCreateRemoteMethodError')
  public errorTemplate?: TemplateRef<RemoteMethodTemplateDirectiveErrorContext>;

  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(ReviewControllerCreateRemoteMethod) remoteMethod: ReviewControllerCreateRemoteMethod,
    @Inject(TemplateRef) template: TemplateRef<RemoteMethodTemplateDirectiveContext<ReviewControllerCreateResponse>>,
    @Inject(ViewContainerRef) viewContainerRef: ViewContainerRef,
    @Inject(ChangeDetectorRef) cdr: ChangeDetectorRef,
  ) {
    super(template, remoteMethodLoader, injector, viewContainerRef, cdr);
    this.remoteMethodOrIdOrToken = remoteMethod;
  }
}

@NgModule({
  declarations: [ReviewControllerCreateRemoteMethodTemplateDirective],
  exports:      [ReviewControllerCreateRemoteMethodTemplateDirective],
})
export class ReviewControllerCreateRemoteMethodTemplateDirectiveModule {
}

@Directive({
  selector: '[reviewRestaurantsReviewControllerCreateRemoteMethod]',
  exportAs: 'reviewRestaurantsReviewControllerCreateRemoteMethod',
})
export class ReviewControllerCreateRemoteMethodDirective
  extends RemoteMethodDirective<ReviewControllerCreateResponse, OpenApiRemoteMethodParameter<ReviewControllerCreateParameter, ReviewControllerCreateRequestBody>> {
  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(ReviewControllerCreateRemoteMethod) remoteMethod: ReviewControllerCreateRemoteMethod,
  ) {
    super(remoteMethodLoader, injector);
    this.remoteMethodOrIdOrToken = remoteMethod;
  }
}

@NgModule({
  declarations: [ReviewControllerCreateRemoteMethodDirective],
  exports:      [ReviewControllerCreateRemoteMethodDirective],
})
export class ReviewControllerCreateRemoteMethodDirectiveModule {
}
