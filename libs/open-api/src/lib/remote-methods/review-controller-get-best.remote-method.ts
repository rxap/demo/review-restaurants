import {
  ChangeDetectorRef,
  Directive,
  Inject,
  Injectable,
  INJECTOR,
  Injector,
  Input,
  NgModule,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';
import {
  OpenApiRemoteMethod,
  OpenApiRemoteMethodParameter,
  RxapOpenApiRemoteMethod,
} from '@rxap/open-api/remote-method';
import { RemoteMethodLoader } from '@rxap/remote-method';
import {
  RemoteMethodDirective,
  RemoteMethodTemplateDirective,
  RemoteMethodTemplateDirectiveContext,
  RemoteMethodTemplateDirectiveErrorContext,
} from '@rxap/remote-method/directive';
import { ReviewControllerGetBestParameter } from '../parameters/review-controller-get-best.parameter';
import { ReviewControllerGetBestResponse } from '../responses/review-controller-get-best.response';

@Injectable({
  providedIn: 'root',
})
@RxapOpenApiRemoteMethod('ReviewController_getBest')
export class ReviewControllerGetBestRemoteMethod
  extends OpenApiRemoteMethod<ReviewControllerGetBestResponse, ReviewControllerGetBestParameter, void> {
  public call(parameters: OpenApiRemoteMethodParameter<ReviewControllerGetBestParameter, void>): Promise<ReviewControllerGetBestResponse> {
    return super.call(parameters);
  }
}

@Directive({
  selector: '[reviewRestaurantsReviewControllerGetBestRemoteMethod]',
  exportAs: 'reviewRestaurantsReviewControllerGetBestRemoteMethod',
})
export class ReviewControllerGetBestRemoteMethodTemplateDirective
  extends RemoteMethodTemplateDirective<ReviewControllerGetBestResponse, OpenApiRemoteMethodParameter<ReviewControllerGetBestParameter, void>> {
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsReviewControllerGetBestRemoteMethodParameters')
  public parameters?: OpenApiRemoteMethodParameter<ReviewControllerGetBestParameter, void>;
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsReviewControllerGetBestRemoteMethodError')
  public errorTemplate?: TemplateRef<RemoteMethodTemplateDirectiveErrorContext>;

  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(ReviewControllerGetBestRemoteMethod) remoteMethod: ReviewControllerGetBestRemoteMethod,
    @Inject(TemplateRef) template: TemplateRef<RemoteMethodTemplateDirectiveContext<ReviewControllerGetBestResponse>>,
    @Inject(ViewContainerRef) viewContainerRef: ViewContainerRef,
    @Inject(ChangeDetectorRef) cdr: ChangeDetectorRef,
  ) {
    super(template, remoteMethodLoader, injector, viewContainerRef, cdr);
    this.remoteMethodOrIdOrToken = remoteMethod;
  }
}

@NgModule({
  declarations: [ReviewControllerGetBestRemoteMethodTemplateDirective],
  exports:      [ReviewControllerGetBestRemoteMethodTemplateDirective],
})
export class ReviewControllerGetBestRemoteMethodTemplateDirectiveModule {
}

@Directive({
  selector: '[reviewRestaurantsReviewControllerGetBestRemoteMethod]',
  exportAs: 'reviewRestaurantsReviewControllerGetBestRemoteMethod',
})
export class ReviewControllerGetBestRemoteMethodDirective
  extends RemoteMethodDirective<ReviewControllerGetBestResponse, OpenApiRemoteMethodParameter<ReviewControllerGetBestParameter, void>> {
  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(ReviewControllerGetBestRemoteMethod) remoteMethod: ReviewControllerGetBestRemoteMethod,
  ) {
    super(remoteMethodLoader, injector);
    this.remoteMethodOrIdOrToken = remoteMethod;
  }
}

@NgModule({
  declarations: [ReviewControllerGetBestRemoteMethodDirective],
  exports:      [ReviewControllerGetBestRemoteMethodDirective],
})
export class ReviewControllerGetBestRemoteMethodDirectiveModule {
}
