import {
  ChangeDetectorRef,
  Directive,
  Inject,
  Injectable,
  INJECTOR,
  Injector,
  Input,
  NgModule,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';
import {
  OpenApiRemoteMethod,
  OpenApiRemoteMethodParameter,
  RxapOpenApiRemoteMethod,
} from '@rxap/open-api/remote-method';
import { RemoteMethodLoader } from '@rxap/remote-method';
import {
  RemoteMethodDirective,
  RemoteMethodTemplateDirective,
  RemoteMethodTemplateDirectiveContext,
  RemoteMethodTemplateDirectiveErrorContext,
} from '@rxap/remote-method/directive';
import { RestaurantControllerDeleteParameter } from '../parameters/restaurant-controller-delete.parameter';

@Injectable({
  providedIn: 'root',
})
@RxapOpenApiRemoteMethod('RestaurantController_delete')
export class RestaurantControllerDeleteRemoteMethod
  extends OpenApiRemoteMethod<void, RestaurantControllerDeleteParameter, void> {
  public call(parameters: OpenApiRemoteMethodParameter<RestaurantControllerDeleteParameter, void>): Promise<void> {
    return super.call(parameters);
  }
}

@Directive({
  selector: '[reviewRestaurantsRestaurantControllerDeleteRemoteMethod]',
  exportAs: 'reviewRestaurantsRestaurantControllerDeleteRemoteMethod',
})
export class RestaurantControllerDeleteRemoteMethodTemplateDirective
  extends RemoteMethodTemplateDirective<void, OpenApiRemoteMethodParameter<RestaurantControllerDeleteParameter, void>> {
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsRestaurantControllerDeleteRemoteMethodParameters')
  public parameters?: OpenApiRemoteMethodParameter<RestaurantControllerDeleteParameter, void>;
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsRestaurantControllerDeleteRemoteMethodError')
  public errorTemplate?: TemplateRef<RemoteMethodTemplateDirectiveErrorContext>;

  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(RestaurantControllerDeleteRemoteMethod) remoteMethod: RestaurantControllerDeleteRemoteMethod,
    @Inject(TemplateRef) template: TemplateRef<RemoteMethodTemplateDirectiveContext<void>>,
    @Inject(ViewContainerRef) viewContainerRef: ViewContainerRef,
    @Inject(ChangeDetectorRef) cdr: ChangeDetectorRef,
  ) {
    super(template, remoteMethodLoader, injector, viewContainerRef, cdr);
    this.remoteMethodOrIdOrToken = remoteMethod;
  }
}

@NgModule({
  declarations: [RestaurantControllerDeleteRemoteMethodTemplateDirective],
  exports:      [RestaurantControllerDeleteRemoteMethodTemplateDirective],
})
export class RestaurantControllerDeleteRemoteMethodTemplateDirectiveModule {
}

@Directive({
  selector: '[reviewRestaurantsRestaurantControllerDeleteRemoteMethod]',
  exportAs: 'reviewRestaurantsRestaurantControllerDeleteRemoteMethod',
})
export class RestaurantControllerDeleteRemoteMethodDirective
  extends RemoteMethodDirective<void, OpenApiRemoteMethodParameter<RestaurantControllerDeleteParameter, void>> {
  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(RestaurantControllerDeleteRemoteMethod) remoteMethod: RestaurantControllerDeleteRemoteMethod,
  ) {
    super(remoteMethodLoader, injector);
    this.remoteMethodOrIdOrToken = remoteMethod;
  }
}

@NgModule({
  declarations: [RestaurantControllerDeleteRemoteMethodDirective],
  exports:      [RestaurantControllerDeleteRemoteMethodDirective],
})
export class RestaurantControllerDeleteRemoteMethodDirectiveModule {
}
