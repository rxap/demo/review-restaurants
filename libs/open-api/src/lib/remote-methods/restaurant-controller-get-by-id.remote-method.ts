import {
  ChangeDetectorRef,
  Directive,
  Inject,
  Injectable,
  INJECTOR,
  Injector,
  Input,
  NgModule,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';
import {
  OpenApiRemoteMethod,
  OpenApiRemoteMethodParameter,
  RxapOpenApiRemoteMethod,
} from '@rxap/open-api/remote-method';
import { RemoteMethodLoader } from '@rxap/remote-method';
import {
  RemoteMethodDirective,
  RemoteMethodTemplateDirective,
  RemoteMethodTemplateDirectiveContext,
  RemoteMethodTemplateDirectiveErrorContext,
} from '@rxap/remote-method/directive';
import { RestaurantControllerGetByIdParameter } from '../parameters/restaurant-controller-get-by-id.parameter';
import { RestaurantControllerGetByIdResponse } from '../responses/restaurant-controller-get-by-id.response';

@Injectable({
  providedIn: 'root',
})
@RxapOpenApiRemoteMethod('RestaurantController_getById')
export class RestaurantControllerGetByIdRemoteMethod
  extends OpenApiRemoteMethod<RestaurantControllerGetByIdResponse, RestaurantControllerGetByIdParameter, void> {
  public call(parameters: OpenApiRemoteMethodParameter<RestaurantControllerGetByIdParameter, void>): Promise<RestaurantControllerGetByIdResponse> {
    return super.call(parameters);
  }
}

@Directive({
  selector: '[reviewRestaurantsRestaurantControllerGetByIdRemoteMethod]',
  exportAs: 'reviewRestaurantsRestaurantControllerGetByIdRemoteMethod',
})
export class RestaurantControllerGetByIdRemoteMethodTemplateDirective
  extends RemoteMethodTemplateDirective<RestaurantControllerGetByIdResponse, OpenApiRemoteMethodParameter<RestaurantControllerGetByIdParameter, void>> {
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsRestaurantControllerGetByIdRemoteMethodParameters')
  public parameters?: OpenApiRemoteMethodParameter<RestaurantControllerGetByIdParameter, void>;
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsRestaurantControllerGetByIdRemoteMethodError')
  public errorTemplate?: TemplateRef<RemoteMethodTemplateDirectiveErrorContext>;

  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(RestaurantControllerGetByIdRemoteMethod) remoteMethod: RestaurantControllerGetByIdRemoteMethod,
    @Inject(TemplateRef) template: TemplateRef<RemoteMethodTemplateDirectiveContext<RestaurantControllerGetByIdResponse>>,
    @Inject(ViewContainerRef) viewContainerRef: ViewContainerRef,
    @Inject(ChangeDetectorRef) cdr: ChangeDetectorRef,
  ) {
    super(template, remoteMethodLoader, injector, viewContainerRef, cdr);
    this.remoteMethodOrIdOrToken = remoteMethod;
  }
}

@NgModule({
  declarations: [RestaurantControllerGetByIdRemoteMethodTemplateDirective],
  exports:      [RestaurantControllerGetByIdRemoteMethodTemplateDirective],
})
export class RestaurantControllerGetByIdRemoteMethodTemplateDirectiveModule {
}

@Directive({
  selector: '[reviewRestaurantsRestaurantControllerGetByIdRemoteMethod]',
  exportAs: 'reviewRestaurantsRestaurantControllerGetByIdRemoteMethod',
})
export class RestaurantControllerGetByIdRemoteMethodDirective
  extends RemoteMethodDirective<RestaurantControllerGetByIdResponse, OpenApiRemoteMethodParameter<RestaurantControllerGetByIdParameter, void>> {
  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(RestaurantControllerGetByIdRemoteMethod) remoteMethod: RestaurantControllerGetByIdRemoteMethod,
  ) {
    super(remoteMethodLoader, injector);
    this.remoteMethodOrIdOrToken = remoteMethod;
  }
}

@NgModule({
  declarations: [RestaurantControllerGetByIdRemoteMethodDirective],
  exports:      [RestaurantControllerGetByIdRemoteMethodDirective],
})
export class RestaurantControllerGetByIdRemoteMethodDirectiveModule {
}
