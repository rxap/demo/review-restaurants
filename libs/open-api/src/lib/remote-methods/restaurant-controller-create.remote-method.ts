import {
  ChangeDetectorRef,
  Directive,
  Inject,
  Injectable,
  INJECTOR,
  Injector,
  Input,
  NgModule,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';
import {
  OpenApiRemoteMethod,
  OpenApiRemoteMethodParameter,
  RxapOpenApiRemoteMethod,
} from '@rxap/open-api/remote-method';
import { RemoteMethodLoader } from '@rxap/remote-method';
import {
  RemoteMethodDirective,
  RemoteMethodTemplateDirective,
  RemoteMethodTemplateDirectiveContext,
  RemoteMethodTemplateDirectiveErrorContext,
} from '@rxap/remote-method/directive';
import { RestaurantControllerCreateRequestBody } from '../request-bodies/restaurant-controller-create.request-body';
import { RestaurantControllerCreateResponse } from '../responses/restaurant-controller-create.response';

@Injectable({
  providedIn: 'root',
})
@RxapOpenApiRemoteMethod('RestaurantController_create')
export class RestaurantControllerCreateRemoteMethod
  extends OpenApiRemoteMethod<RestaurantControllerCreateResponse, void, RestaurantControllerCreateRequestBody> {
  public call(parameters: OpenApiRemoteMethodParameter<void, RestaurantControllerCreateRequestBody>): Promise<RestaurantControllerCreateResponse> {
    return super.call(parameters);
  }
}

@Directive({
  selector: '[reviewRestaurantsRestaurantControllerCreateRemoteMethod]',
  exportAs: 'reviewRestaurantsRestaurantControllerCreateRemoteMethod',
})
export class RestaurantControllerCreateRemoteMethodTemplateDirective
  extends RemoteMethodTemplateDirective<RestaurantControllerCreateResponse, OpenApiRemoteMethodParameter<void, RestaurantControllerCreateRequestBody>> {
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsRestaurantControllerCreateRemoteMethodParameters')
  public parameters?: OpenApiRemoteMethodParameter<void, RestaurantControllerCreateRequestBody>;
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsRestaurantControllerCreateRemoteMethodError')
  public errorTemplate?: TemplateRef<RemoteMethodTemplateDirectiveErrorContext>;

  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(RestaurantControllerCreateRemoteMethod) remoteMethod: RestaurantControllerCreateRemoteMethod,
    @Inject(TemplateRef) template: TemplateRef<RemoteMethodTemplateDirectiveContext<RestaurantControllerCreateResponse>>,
    @Inject(ViewContainerRef) viewContainerRef: ViewContainerRef,
    @Inject(ChangeDetectorRef) cdr: ChangeDetectorRef,
  ) {
    super(template, remoteMethodLoader, injector, viewContainerRef, cdr);
    this.remoteMethodOrIdOrToken = remoteMethod;
    this.withoutParameters       = true;
  }
}

@NgModule({
  declarations: [RestaurantControllerCreateRemoteMethodTemplateDirective],
  exports:      [RestaurantControllerCreateRemoteMethodTemplateDirective],
})
export class RestaurantControllerCreateRemoteMethodTemplateDirectiveModule {
}

@Directive({
  selector: '[reviewRestaurantsRestaurantControllerCreateRemoteMethod]',
  exportAs: 'reviewRestaurantsRestaurantControllerCreateRemoteMethod',
})
export class RestaurantControllerCreateRemoteMethodDirective
  extends RemoteMethodDirective<RestaurantControllerCreateResponse, OpenApiRemoteMethodParameter<void, RestaurantControllerCreateRequestBody>> {
  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(RestaurantControllerCreateRemoteMethod) remoteMethod: RestaurantControllerCreateRemoteMethod,
  ) {
    super(remoteMethodLoader, injector);
    this.remoteMethodOrIdOrToken = remoteMethod;
  }
}

@NgModule({
  declarations: [RestaurantControllerCreateRemoteMethodDirective],
  exports:      [RestaurantControllerCreateRemoteMethodDirective],
})
export class RestaurantControllerCreateRemoteMethodDirectiveModule {
}
