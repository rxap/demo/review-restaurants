import {
  ChangeDetectorRef,
  Directive,
  Inject,
  Injectable,
  INJECTOR,
  Injector,
  Input,
  NgModule,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';
import {
  OpenApiRemoteMethod,
  OpenApiRemoteMethodParameter,
  RxapOpenApiRemoteMethod,
} from '@rxap/open-api/remote-method';
import { RemoteMethodLoader } from '@rxap/remote-method';
import {
  RemoteMethodDirective,
  RemoteMethodTemplateDirective,
  RemoteMethodTemplateDirectiveContext,
  RemoteMethodTemplateDirectiveErrorContext,
} from '@rxap/remote-method/directive';
import { ReviewControllerDeleteParameter } from '../parameters/review-controller-delete.parameter';

@Injectable({
  providedIn: 'root',
})
@RxapOpenApiRemoteMethod('ReviewController_delete')
export class ReviewControllerDeleteRemoteMethod
  extends OpenApiRemoteMethod<void, ReviewControllerDeleteParameter, void> {
  public call(parameters: OpenApiRemoteMethodParameter<ReviewControllerDeleteParameter, void>): Promise<void> {
    return super.call(parameters);
  }
}

@Directive({
  selector: '[reviewRestaurantsReviewControllerDeleteRemoteMethod]',
  exportAs: 'reviewRestaurantsReviewControllerDeleteRemoteMethod',
})
export class ReviewControllerDeleteRemoteMethodTemplateDirective
  extends RemoteMethodTemplateDirective<void, OpenApiRemoteMethodParameter<ReviewControllerDeleteParameter, void>> {
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsReviewControllerDeleteRemoteMethodParameters')
  public parameters?: OpenApiRemoteMethodParameter<ReviewControllerDeleteParameter, void>;
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsReviewControllerDeleteRemoteMethodError')
  public errorTemplate?: TemplateRef<RemoteMethodTemplateDirectiveErrorContext>;

  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(ReviewControllerDeleteRemoteMethod) remoteMethod: ReviewControllerDeleteRemoteMethod,
    @Inject(TemplateRef) template: TemplateRef<RemoteMethodTemplateDirectiveContext<void>>,
    @Inject(ViewContainerRef) viewContainerRef: ViewContainerRef,
    @Inject(ChangeDetectorRef) cdr: ChangeDetectorRef,
  ) {
    super(template, remoteMethodLoader, injector, viewContainerRef, cdr);
    this.remoteMethodOrIdOrToken = remoteMethod;
  }
}

@NgModule({
  declarations: [ReviewControllerDeleteRemoteMethodTemplateDirective],
  exports:      [ReviewControllerDeleteRemoteMethodTemplateDirective],
})
export class ReviewControllerDeleteRemoteMethodTemplateDirectiveModule {
}

@Directive({
  selector: '[reviewRestaurantsReviewControllerDeleteRemoteMethod]',
  exportAs: 'reviewRestaurantsReviewControllerDeleteRemoteMethod',
})
export class ReviewControllerDeleteRemoteMethodDirective
  extends RemoteMethodDirective<void, OpenApiRemoteMethodParameter<ReviewControllerDeleteParameter, void>> {
  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(ReviewControllerDeleteRemoteMethod) remoteMethod: ReviewControllerDeleteRemoteMethod,
  ) {
    super(remoteMethodLoader, injector);
    this.remoteMethodOrIdOrToken = remoteMethod;
  }
}

@NgModule({
  declarations: [ReviewControllerDeleteRemoteMethodDirective],
  exports:      [ReviewControllerDeleteRemoteMethodDirective],
})
export class ReviewControllerDeleteRemoteMethodDirectiveModule {
}
