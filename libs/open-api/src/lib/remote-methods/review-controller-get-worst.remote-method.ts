import {
  ChangeDetectorRef,
  Directive,
  Inject,
  Injectable,
  INJECTOR,
  Injector,
  Input,
  NgModule,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';
import {
  OpenApiRemoteMethod,
  OpenApiRemoteMethodParameter,
  RxapOpenApiRemoteMethod,
} from '@rxap/open-api/remote-method';
import { RemoteMethodLoader } from '@rxap/remote-method';
import {
  RemoteMethodDirective,
  RemoteMethodTemplateDirective,
  RemoteMethodTemplateDirectiveContext,
  RemoteMethodTemplateDirectiveErrorContext,
} from '@rxap/remote-method/directive';
import { ReviewControllerGetWorstParameter } from '../parameters/review-controller-get-worst.parameter';
import { ReviewControllerGetWorstResponse } from '../responses/review-controller-get-worst.response';

@Injectable({
  providedIn: 'root',
})
@RxapOpenApiRemoteMethod('ReviewController_getWorst')
export class ReviewControllerGetWorstRemoteMethod
  extends OpenApiRemoteMethod<ReviewControllerGetWorstResponse, ReviewControllerGetWorstParameter, void> {
  public call(parameters: OpenApiRemoteMethodParameter<ReviewControllerGetWorstParameter, void>): Promise<ReviewControllerGetWorstResponse> {
    return super.call(parameters);
  }
}

@Directive({
  selector: '[reviewRestaurantsReviewControllerGetWorstRemoteMethod]',
  exportAs: 'reviewRestaurantsReviewControllerGetWorstRemoteMethod',
})
export class ReviewControllerGetWorstRemoteMethodTemplateDirective
  extends RemoteMethodTemplateDirective<ReviewControllerGetWorstResponse, OpenApiRemoteMethodParameter<ReviewControllerGetWorstParameter, void>> {
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsReviewControllerGetWorstRemoteMethodParameters')
  public parameters?: OpenApiRemoteMethodParameter<ReviewControllerGetWorstParameter, void>;
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsReviewControllerGetWorstRemoteMethodError')
  public errorTemplate?: TemplateRef<RemoteMethodTemplateDirectiveErrorContext>;

  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(ReviewControllerGetWorstRemoteMethod) remoteMethod: ReviewControllerGetWorstRemoteMethod,
    @Inject(TemplateRef) template: TemplateRef<RemoteMethodTemplateDirectiveContext<ReviewControllerGetWorstResponse>>,
    @Inject(ViewContainerRef) viewContainerRef: ViewContainerRef,
    @Inject(ChangeDetectorRef) cdr: ChangeDetectorRef,
  ) {
    super(template, remoteMethodLoader, injector, viewContainerRef, cdr);
    this.remoteMethodOrIdOrToken = remoteMethod;
  }
}

@NgModule({
  declarations: [ReviewControllerGetWorstRemoteMethodTemplateDirective],
  exports:      [ReviewControllerGetWorstRemoteMethodTemplateDirective],
})
export class ReviewControllerGetWorstRemoteMethodTemplateDirectiveModule {
}

@Directive({
  selector: '[reviewRestaurantsReviewControllerGetWorstRemoteMethod]',
  exportAs: 'reviewRestaurantsReviewControllerGetWorstRemoteMethod',
})
export class ReviewControllerGetWorstRemoteMethodDirective
  extends RemoteMethodDirective<ReviewControllerGetWorstResponse, OpenApiRemoteMethodParameter<ReviewControllerGetWorstParameter, void>> {
  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(ReviewControllerGetWorstRemoteMethod) remoteMethod: ReviewControllerGetWorstRemoteMethod,
  ) {
    super(remoteMethodLoader, injector);
    this.remoteMethodOrIdOrToken = remoteMethod;
  }
}

@NgModule({
  declarations: [ReviewControllerGetWorstRemoteMethodDirective],
  exports:      [ReviewControllerGetWorstRemoteMethodDirective],
})
export class ReviewControllerGetWorstRemoteMethodDirectiveModule {
}
