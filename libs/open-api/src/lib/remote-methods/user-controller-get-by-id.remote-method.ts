import {
  ChangeDetectorRef,
  Directive,
  Inject,
  Injectable,
  INJECTOR,
  Injector,
  Input,
  NgModule,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';
import {
  OpenApiRemoteMethod,
  OpenApiRemoteMethodParameter,
  RxapOpenApiRemoteMethod,
} from '@rxap/open-api/remote-method';
import { RemoteMethodLoader } from '@rxap/remote-method';
import {
  RemoteMethodDirective,
  RemoteMethodTemplateDirective,
  RemoteMethodTemplateDirectiveContext,
  RemoteMethodTemplateDirectiveErrorContext,
} from '@rxap/remote-method/directive';
import { UserControllerGetByIdParameter } from '../parameters/user-controller-get-by-id.parameter';
import { UserControllerGetByIdResponse } from '../responses/user-controller-get-by-id.response';

@Injectable({
  providedIn: 'root',
})
@RxapOpenApiRemoteMethod('UserController_getById')
export class UserControllerGetByIdRemoteMethod
  extends OpenApiRemoteMethod<UserControllerGetByIdResponse, UserControllerGetByIdParameter, void> {
  public call(parameters: OpenApiRemoteMethodParameter<UserControllerGetByIdParameter, void>): Promise<UserControllerGetByIdResponse> {
    return super.call(parameters);
  }
}

@Directive({
  selector: '[reviewRestaurantsUserControllerGetByIdRemoteMethod]',
  exportAs: 'reviewRestaurantsUserControllerGetByIdRemoteMethod',
})
export class UserControllerGetByIdRemoteMethodTemplateDirective
  extends RemoteMethodTemplateDirective<UserControllerGetByIdResponse, OpenApiRemoteMethodParameter<UserControllerGetByIdParameter, void>> {
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsUserControllerGetByIdRemoteMethodParameters')
  public parameters?: OpenApiRemoteMethodParameter<UserControllerGetByIdParameter, void>;
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsUserControllerGetByIdRemoteMethodError')
  public errorTemplate?: TemplateRef<RemoteMethodTemplateDirectiveErrorContext>;

  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(UserControllerGetByIdRemoteMethod) remoteMethod: UserControllerGetByIdRemoteMethod,
    @Inject(TemplateRef) template: TemplateRef<RemoteMethodTemplateDirectiveContext<UserControllerGetByIdResponse>>,
    @Inject(ViewContainerRef) viewContainerRef: ViewContainerRef,
    @Inject(ChangeDetectorRef) cdr: ChangeDetectorRef,
  ) {
    super(template, remoteMethodLoader, injector, viewContainerRef, cdr);
    this.remoteMethodOrIdOrToken = remoteMethod;
  }
}

@NgModule({
  declarations: [UserControllerGetByIdRemoteMethodTemplateDirective],
  exports:      [UserControllerGetByIdRemoteMethodTemplateDirective],
})
export class UserControllerGetByIdRemoteMethodTemplateDirectiveModule {
}

@Directive({
  selector: '[reviewRestaurantsUserControllerGetByIdRemoteMethod]',
  exportAs: 'reviewRestaurantsUserControllerGetByIdRemoteMethod',
})
export class UserControllerGetByIdRemoteMethodDirective
  extends RemoteMethodDirective<UserControllerGetByIdResponse, OpenApiRemoteMethodParameter<UserControllerGetByIdParameter, void>> {
  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(UserControllerGetByIdRemoteMethod) remoteMethod: UserControllerGetByIdRemoteMethod,
  ) {
    super(remoteMethodLoader, injector);
    this.remoteMethodOrIdOrToken = remoteMethod;
  }
}

@NgModule({
  declarations: [UserControllerGetByIdRemoteMethodDirective],
  exports:      [UserControllerGetByIdRemoteMethodDirective],
})
export class UserControllerGetByIdRemoteMethodDirectiveModule {
}
