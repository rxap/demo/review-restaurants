import {
  ChangeDetectorRef,
  Directive,
  Inject,
  Injectable,
  INJECTOR,
  Injector,
  Input,
  NgModule,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';
import {
  OpenApiRemoteMethod,
  OpenApiRemoteMethodParameter,
  RxapOpenApiRemoteMethod,
} from '@rxap/open-api/remote-method';
import { RemoteMethodLoader } from '@rxap/remote-method';
import {
  RemoteMethodDirective,
  RemoteMethodTemplateDirective,
  RemoteMethodTemplateDirectiveContext,
  RemoteMethodTemplateDirectiveErrorContext,
} from '@rxap/remote-method/directive';

@Injectable({
  providedIn: 'root',
})
@RxapOpenApiRemoteMethod('E2eController_clearRestaurant')
export class E2eControllerClearRestaurantRemoteMethod extends OpenApiRemoteMethod<void, void, void> {
  public call(): Promise<void> {
    return super.call();
  }
}

@Directive({
  selector: '[reviewRestaurantsE2eControllerClearRestaurantRemoteMethod]',
  exportAs: 'reviewRestaurantsE2eControllerClearRestaurantRemoteMethod',
})
export class E2eControllerClearRestaurantRemoteMethodTemplateDirective
  extends RemoteMethodTemplateDirective<void, OpenApiRemoteMethodParameter<void, void>> {
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsE2eControllerClearRestaurantRemoteMethodParameters')
  public parameters?: OpenApiRemoteMethodParameter<void, void>;
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsE2eControllerClearRestaurantRemoteMethodError')
  public errorTemplate?: TemplateRef<RemoteMethodTemplateDirectiveErrorContext>;

  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(E2eControllerClearRestaurantRemoteMethod) remoteMethod: E2eControllerClearRestaurantRemoteMethod,
    @Inject(TemplateRef) template: TemplateRef<RemoteMethodTemplateDirectiveContext<void>>,
    @Inject(ViewContainerRef) viewContainerRef: ViewContainerRef,
    @Inject(ChangeDetectorRef) cdr: ChangeDetectorRef,
  ) {
    super(template, remoteMethodLoader, injector, viewContainerRef, cdr);
    this.remoteMethodOrIdOrToken = remoteMethod;
    this.withoutParameters       = true;
  }
}

@NgModule({
  declarations: [E2eControllerClearRestaurantRemoteMethodTemplateDirective],
  exports:      [E2eControllerClearRestaurantRemoteMethodTemplateDirective],
})
export class E2eControllerClearRestaurantRemoteMethodTemplateDirectiveModule {
}

@Directive({
  selector: '[reviewRestaurantsE2eControllerClearRestaurantRemoteMethod]',
  exportAs: 'reviewRestaurantsE2eControllerClearRestaurantRemoteMethod',
})
export class E2eControllerClearRestaurantRemoteMethodDirective
  extends RemoteMethodDirective<void, OpenApiRemoteMethodParameter<void, void>> {
  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(E2eControllerClearRestaurantRemoteMethod) remoteMethod: E2eControllerClearRestaurantRemoteMethod,
  ) {
    super(remoteMethodLoader, injector);
    this.remoteMethodOrIdOrToken = remoteMethod;
  }
}

@NgModule({
  declarations: [E2eControllerClearRestaurantRemoteMethodDirective],
  exports:      [E2eControllerClearRestaurantRemoteMethodDirective],
})
export class E2eControllerClearRestaurantRemoteMethodDirectiveModule {
}
