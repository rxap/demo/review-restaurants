import {
  ChangeDetectorRef,
  Directive,
  Inject,
  Injectable,
  INJECTOR,
  Injector,
  Input,
  NgModule,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';
import {
  OpenApiRemoteMethod,
  OpenApiRemoteMethodParameter,
  RxapOpenApiRemoteMethod,
} from '@rxap/open-api/remote-method';
import { RemoteMethodLoader } from '@rxap/remote-method';
import {
  RemoteMethodDirective,
  RemoteMethodTemplateDirective,
  RemoteMethodTemplateDirectiveContext,
  RemoteMethodTemplateDirectiveErrorContext,
} from '@rxap/remote-method/directive';
import { AuthControllerLoginRequestBody } from '../request-bodies/auth-controller-login.request-body';
import { AuthControllerLoginResponse } from '../responses/auth-controller-login.response';

@Injectable({
  providedIn: 'root',
})
@RxapOpenApiRemoteMethod('AuthController_login')
export class AuthControllerLoginRemoteMethod
  extends OpenApiRemoteMethod<AuthControllerLoginResponse, void, AuthControllerLoginRequestBody> {
  public call(parameters: OpenApiRemoteMethodParameter<void, AuthControllerLoginRequestBody>): Promise<AuthControllerLoginResponse> {
    return super.call(parameters);
  }
}

@Directive({
  selector: '[reviewRestaurantsAuthControllerLoginRemoteMethod]',
  exportAs: 'reviewRestaurantsAuthControllerLoginRemoteMethod',
})
export class AuthControllerLoginRemoteMethodTemplateDirective
  extends RemoteMethodTemplateDirective<AuthControllerLoginResponse, OpenApiRemoteMethodParameter<void, AuthControllerLoginRequestBody>> {
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsAuthControllerLoginRemoteMethodParameters')
  public parameters?: OpenApiRemoteMethodParameter<void, AuthControllerLoginRequestBody>;
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsAuthControllerLoginRemoteMethodError')
  public errorTemplate?: TemplateRef<RemoteMethodTemplateDirectiveErrorContext>;

  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(AuthControllerLoginRemoteMethod) remoteMethod: AuthControllerLoginRemoteMethod,
    @Inject(TemplateRef) template: TemplateRef<RemoteMethodTemplateDirectiveContext<AuthControllerLoginResponse>>,
    @Inject(ViewContainerRef) viewContainerRef: ViewContainerRef,
    @Inject(ChangeDetectorRef) cdr: ChangeDetectorRef,
  ) {
    super(template, remoteMethodLoader, injector, viewContainerRef, cdr);
    this.remoteMethodOrIdOrToken = remoteMethod;
    this.withoutParameters       = true;
  }
}

@NgModule({
  declarations: [AuthControllerLoginRemoteMethodTemplateDirective],
  exports:      [AuthControllerLoginRemoteMethodTemplateDirective],
})
export class AuthControllerLoginRemoteMethodTemplateDirectiveModule {
}

@Directive({
  selector: '[reviewRestaurantsAuthControllerLoginRemoteMethod]',
  exportAs: 'reviewRestaurantsAuthControllerLoginRemoteMethod',
})
export class AuthControllerLoginRemoteMethodDirective
  extends RemoteMethodDirective<AuthControllerLoginResponse, OpenApiRemoteMethodParameter<void, AuthControllerLoginRequestBody>> {
  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(AuthControllerLoginRemoteMethod) remoteMethod: AuthControllerLoginRemoteMethod,
  ) {
    super(remoteMethodLoader, injector);
    this.remoteMethodOrIdOrToken = remoteMethod;
  }
}

@NgModule({
  declarations: [AuthControllerLoginRemoteMethodDirective],
  exports:      [AuthControllerLoginRemoteMethodDirective],
})
export class AuthControllerLoginRemoteMethodDirectiveModule {
}
