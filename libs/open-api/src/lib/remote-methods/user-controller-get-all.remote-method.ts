import {
  ChangeDetectorRef,
  Directive,
  Inject,
  Injectable,
  INJECTOR,
  Injector,
  Input,
  IterableDiffers,
  NgModule,
  NgZone,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';
import {
  OpenApiRemoteMethod,
  OpenApiRemoteMethodParameter,
  RxapOpenApiRemoteMethod,
} from '@rxap/open-api/remote-method';
import { RemoteMethodLoader } from '@rxap/remote-method';
import {
  RemoteMethodDirective,
  RemoteMethodTemplateCollectionDirective,
  RemoteMethodTemplateCollectionDirectiveContext,
  RemoteMethodTemplateCollectionDirectiveErrorContext,
  RemoteMethodTemplateDirective,
  RemoteMethodTemplateDirectiveContext,
  RemoteMethodTemplateDirectiveErrorContext,
} from '@rxap/remote-method/directive';
import { ArrayElement } from '@rxap/utilities';
import { UserControllerGetAllResponse } from '../responses/user-controller-get-all.response';

@Injectable({
  providedIn: 'root',
})
@RxapOpenApiRemoteMethod('UserController_getAll')
export class UserControllerGetAllRemoteMethod extends OpenApiRemoteMethod<UserControllerGetAllResponse, void, void> {
  public call(): Promise<UserControllerGetAllResponse> {
    return super.call();
  }
}

@Directive({
  selector: '[reviewRestaurantsUserControllerGetAllCollectionRemoteMethod]',
  exportAs: 'reviewRestaurantsUserControllerGetAllCollectionRemoteMethod',
})
export class UserControllerGetAllRemoteMethodTemplateCollectionDirective
  extends RemoteMethodTemplateCollectionDirective<ArrayElement<UserControllerGetAllResponse>, OpenApiRemoteMethodParameter<void, void>> {
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsUserControllerGetAllCollectionRemoteMethodParameters')
  public parameters?: OpenApiRemoteMethodParameter<void, void>;
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsUserControllerGetAllCollectionRemoteMethodError')
  public errorTemplate?: TemplateRef<RemoteMethodTemplateCollectionDirectiveErrorContext>;
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsUserControllerGetAllCollectionRemoteMethodEmpty')
  public emptyTemplate?: TemplateRef<void>;

  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(UserControllerGetAllRemoteMethod) remoteMethod: UserControllerGetAllRemoteMethod,
    @Inject(TemplateRef) template: TemplateRef<RemoteMethodTemplateCollectionDirectiveContext<ArrayElement<UserControllerGetAllResponse>>>,
    @Inject(IterableDiffers) differs: IterableDiffers,
    @Inject(NgZone) zone: NgZone,
    @Inject(ViewContainerRef) viewContainerRef: ViewContainerRef,
    @Inject(ChangeDetectorRef) cdr: ChangeDetectorRef,
  ) {
    super(template, remoteMethodLoader, injector, viewContainerRef, cdr, differs, zone);
    this.remoteMethodOrIdOrToken = remoteMethod;
    this.withoutParameters       = true;
  }
}

@NgModule({
  declarations: [UserControllerGetAllRemoteMethodTemplateCollectionDirective],
  exports:      [UserControllerGetAllRemoteMethodTemplateCollectionDirective],
})
export class UserControllerGetAllRemoteMethodTemplateCollectionDirectiveModule {
}

@Directive({
  selector: '[reviewRestaurantsUserControllerGetAllRemoteMethod]',
  exportAs: 'reviewRestaurantsUserControllerGetAllRemoteMethod',
})
export class UserControllerGetAllRemoteMethodTemplateDirective
  extends RemoteMethodTemplateDirective<UserControllerGetAllResponse, OpenApiRemoteMethodParameter<void, void>> {
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsUserControllerGetAllRemoteMethodParameters')
  public parameters?: OpenApiRemoteMethodParameter<void, void>;
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsUserControllerGetAllRemoteMethodError')
  public errorTemplate?: TemplateRef<RemoteMethodTemplateDirectiveErrorContext>;

  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(UserControllerGetAllRemoteMethod) remoteMethod: UserControllerGetAllRemoteMethod,
    @Inject(TemplateRef) template: TemplateRef<RemoteMethodTemplateDirectiveContext<UserControllerGetAllResponse>>,
    @Inject(ViewContainerRef) viewContainerRef: ViewContainerRef,
    @Inject(ChangeDetectorRef) cdr: ChangeDetectorRef,
  ) {
    super(template, remoteMethodLoader, injector, viewContainerRef, cdr);
    this.remoteMethodOrIdOrToken = remoteMethod;
    this.withoutParameters       = true;
  }
}

@NgModule({
  declarations: [UserControllerGetAllRemoteMethodTemplateDirective],
  exports:      [UserControllerGetAllRemoteMethodTemplateDirective],
})
export class UserControllerGetAllRemoteMethodTemplateDirectiveModule {
}

@Directive({
  selector: '[reviewRestaurantsUserControllerGetAllRemoteMethod]',
  exportAs: 'reviewRestaurantsUserControllerGetAllRemoteMethod',
})
export class UserControllerGetAllRemoteMethodDirective
  extends RemoteMethodDirective<UserControllerGetAllResponse, OpenApiRemoteMethodParameter<void, void>> {
  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(UserControllerGetAllRemoteMethod) remoteMethod: UserControllerGetAllRemoteMethod,
  ) {
    super(remoteMethodLoader, injector);
    this.remoteMethodOrIdOrToken = remoteMethod;
  }
}

@NgModule({
  declarations: [UserControllerGetAllRemoteMethodDirective],
  exports:      [UserControllerGetAllRemoteMethodDirective],
})
export class UserControllerGetAllRemoteMethodDirectiveModule {
}
