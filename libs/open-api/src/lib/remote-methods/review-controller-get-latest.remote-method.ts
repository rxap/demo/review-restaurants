import {
  ChangeDetectorRef,
  Directive,
  Inject,
  Injectable,
  INJECTOR,
  Injector,
  Input,
  NgModule,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';
import {
  OpenApiRemoteMethod,
  OpenApiRemoteMethodParameter,
  RxapOpenApiRemoteMethod,
} from '@rxap/open-api/remote-method';
import { RemoteMethodLoader } from '@rxap/remote-method';
import {
  RemoteMethodDirective,
  RemoteMethodTemplateDirective,
  RemoteMethodTemplateDirectiveContext,
  RemoteMethodTemplateDirectiveErrorContext,
} from '@rxap/remote-method/directive';
import { ReviewControllerGetLatestParameter } from '../parameters/review-controller-get-latest.parameter';
import { ReviewControllerGetLatestResponse } from '../responses/review-controller-get-latest.response';

@Injectable({
  providedIn: 'root',
})
@RxapOpenApiRemoteMethod('ReviewController_getLatest')
export class ReviewControllerGetLatestRemoteMethod
  extends OpenApiRemoteMethod<ReviewControllerGetLatestResponse, ReviewControllerGetLatestParameter, void> {
  public call(parameters: OpenApiRemoteMethodParameter<ReviewControllerGetLatestParameter, void>): Promise<ReviewControllerGetLatestResponse> {
    return super.call(parameters);
  }
}

@Directive({
  selector: '[reviewRestaurantsReviewControllerGetLatestRemoteMethod]',
  exportAs: 'reviewRestaurantsReviewControllerGetLatestRemoteMethod',
})
export class ReviewControllerGetLatestRemoteMethodTemplateDirective
  extends RemoteMethodTemplateDirective<ReviewControllerGetLatestResponse, OpenApiRemoteMethodParameter<ReviewControllerGetLatestParameter, void>> {
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsReviewControllerGetLatestRemoteMethodParameters')
  public parameters?: OpenApiRemoteMethodParameter<ReviewControllerGetLatestParameter, void>;
  // tslint:disable-next-line:no-input-rename
  @Input('reviewRestaurantsReviewControllerGetLatestRemoteMethodError')
  public errorTemplate?: TemplateRef<RemoteMethodTemplateDirectiveErrorContext>;

  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(ReviewControllerGetLatestRemoteMethod) remoteMethod: ReviewControllerGetLatestRemoteMethod,
    @Inject(TemplateRef) template: TemplateRef<RemoteMethodTemplateDirectiveContext<ReviewControllerGetLatestResponse>>,
    @Inject(ViewContainerRef) viewContainerRef: ViewContainerRef,
    @Inject(ChangeDetectorRef) cdr: ChangeDetectorRef,
  ) {
    super(template, remoteMethodLoader, injector, viewContainerRef, cdr);
    this.remoteMethodOrIdOrToken = remoteMethod;
  }
}

@NgModule({
  declarations: [ReviewControllerGetLatestRemoteMethodTemplateDirective],
  exports:      [ReviewControllerGetLatestRemoteMethodTemplateDirective],
})
export class ReviewControllerGetLatestRemoteMethodTemplateDirectiveModule {
}

@Directive({
  selector: '[reviewRestaurantsReviewControllerGetLatestRemoteMethod]',
  exportAs: 'reviewRestaurantsReviewControllerGetLatestRemoteMethod',
})
export class ReviewControllerGetLatestRemoteMethodDirective
  extends RemoteMethodDirective<ReviewControllerGetLatestResponse, OpenApiRemoteMethodParameter<ReviewControllerGetLatestParameter, void>> {
  constructor(
    @Inject(RemoteMethodLoader) remoteMethodLoader: RemoteMethodLoader,
    @Inject(INJECTOR) injector: Injector,
    @Inject(ReviewControllerGetLatestRemoteMethod) remoteMethod: ReviewControllerGetLatestRemoteMethod,
  ) {
    super(remoteMethodLoader, injector);
    this.remoteMethodOrIdOrToken = remoteMethod;
  }
}

@NgModule({
  declarations: [ReviewControllerGetLatestRemoteMethodDirective],
  exports:      [ReviewControllerGetLatestRemoteMethodDirective],
})
export class ReviewControllerGetLatestRemoteMethodDirectiveModule {
}
