export interface UserControllerUpdateRequestBody {
  username: string;
  role: 'admin' | 'member';
}
