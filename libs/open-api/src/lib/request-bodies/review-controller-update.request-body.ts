export interface ReviewControllerUpdateRequestBody {
  rating: number;
  comment: string;
  dateOfVisit: string;
}
