export interface AuthControllerLoginRequestBody {
  username: string;
  password: string;
}
