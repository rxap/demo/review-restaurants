export interface ReviewControllerCreateRequestBody {
  rating: number;
  comment: string;
  dateOfVisit: string;
}
