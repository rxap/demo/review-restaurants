export interface AuthControllerRegisterRequestBody {
  username: string;
  password: string;
  role: 'admin' | 'member';
}
