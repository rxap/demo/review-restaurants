export interface UserControllerCreateRequestBody {
  username: string;
  password: string;
  role: 'admin' | 'member';
}
