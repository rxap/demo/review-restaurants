import { Provider } from '@angular/core';
import { AppControllerGetDataRemoteMethod } from './remote-methods/app-controller-get-data.remote-method';
import { AuthControllerCheckRemoteMethod } from './remote-methods/auth-controller-check.remote-method';
import { AuthControllerLoginRemoteMethod } from './remote-methods/auth-controller-login.remote-method';
import { AuthControllerRegisterRemoteMethod } from './remote-methods/auth-controller-register.remote-method';
import { E2eControllerClearAllRemoteMethod } from './remote-methods/e2e-controller-clear-all.remote-method';
import {
  E2eControllerClearRestaurantRemoteMethod,
} from './remote-methods/e2e-controller-clear-restaurant.remote-method';
import { E2eControllerClearReviewRemoteMethod } from './remote-methods/e2e-controller-clear-review.remote-method';
import { E2eControllerClearUserRemoteMethod } from './remote-methods/e2e-controller-clear-user.remote-method';
import { E2eControllerSeedRemoteMethod } from './remote-methods/e2e-controller-seed.remote-method';
import { HealthControllerDatabaseRemoteMethod } from './remote-methods/health-controller-database.remote-method';
import { HealthControllerHealthCheckRemoteMethod } from './remote-methods/health-controller-health-check.remote-method';
import { RestaurantControllerCreateRemoteMethod } from './remote-methods/restaurant-controller-create.remote-method';
import { RestaurantControllerDeleteRemoteMethod } from './remote-methods/restaurant-controller-delete.remote-method';
import { RestaurantControllerGetAllRemoteMethod } from './remote-methods/restaurant-controller-get-all.remote-method';
import {
  RestaurantControllerGetByIdRemoteMethod,
} from './remote-methods/restaurant-controller-get-by-id.remote-method';
import { RestaurantControllerUpdateRemoteMethod } from './remote-methods/restaurant-controller-update.remote-method';
import { ReviewControllerCreateRemoteMethod } from './remote-methods/review-controller-create.remote-method';
import { ReviewControllerDeleteRemoteMethod } from './remote-methods/review-controller-delete.remote-method';
import { ReviewControllerGetAllRemoteMethod } from './remote-methods/review-controller-get-all.remote-method';
import { ReviewControllerGetBestRemoteMethod } from './remote-methods/review-controller-get-best.remote-method';
import { ReviewControllerGetLatestRemoteMethod } from './remote-methods/review-controller-get-latest.remote-method';
import { ReviewControllerGetWorstRemoteMethod } from './remote-methods/review-controller-get-worst.remote-method';
import { ReviewControllerUpdateRemoteMethod } from './remote-methods/review-controller-update.remote-method';
import { UserControllerCreateRemoteMethod } from './remote-methods/user-controller-create.remote-method';
import { UserControllerDeleteRemoteMethod } from './remote-methods/user-controller-delete.remote-method';
import { UserControllerGetAllRemoteMethod } from './remote-methods/user-controller-get-all.remote-method';
import { UserControllerGetByIdRemoteMethod } from './remote-methods/user-controller-get-by-id.remote-method';
import { UserControllerUpdateRemoteMethod } from './remote-methods/user-controller-update.remote-method';

export const OPEN_API_PROVIDERS: Provider[] = [
  AppControllerGetDataRemoteMethod,
  HealthControllerDatabaseRemoteMethod,
  HealthControllerHealthCheckRemoteMethod,
  ReviewControllerGetBestRemoteMethod,
  ReviewControllerGetWorstRemoteMethod,
  ReviewControllerGetLatestRemoteMethod,
  ReviewControllerDeleteRemoteMethod,
  ReviewControllerUpdateRemoteMethod,
  ReviewControllerGetAllRemoteMethod,
  ReviewControllerCreateRemoteMethod,
  RestaurantControllerCreateRemoteMethod,
  RestaurantControllerGetAllRemoteMethod,
  RestaurantControllerUpdateRemoteMethod,
  RestaurantControllerGetByIdRemoteMethod,
  RestaurantControllerDeleteRemoteMethod,
  AuthControllerLoginRemoteMethod,
  AuthControllerRegisterRemoteMethod,
  AuthControllerCheckRemoteMethod,
  E2eControllerClearUserRemoteMethod,
  E2eControllerClearRestaurantRemoteMethod,
  E2eControllerClearReviewRemoteMethod,
  E2eControllerClearAllRemoteMethod,
  E2eControllerSeedRemoteMethod,
  UserControllerCreateRemoteMethod,
  UserControllerGetAllRemoteMethod,
  UserControllerUpdateRemoteMethod,
  UserControllerDeleteRemoteMethod,
  UserControllerGetByIdRemoteMethod,
];
