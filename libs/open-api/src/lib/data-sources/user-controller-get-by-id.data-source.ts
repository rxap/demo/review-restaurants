import { Injectable } from '@angular/core';
import { OpenApiDataSource, RxapOpenApiDataSource } from '@rxap/open-api/data-source';
import { UserControllerGetByIdParameter } from '../parameters/user-controller-get-by-id.parameter';
import { UserControllerGetByIdResponse } from '../responses/user-controller-get-by-id.response';

@Injectable({
  providedIn: 'root',
})
@RxapOpenApiDataSource('UserController_getById')
export class UserControllerGetByIdDataSource
  extends OpenApiDataSource<UserControllerGetByIdResponse, UserControllerGetByIdParameter> {
}
