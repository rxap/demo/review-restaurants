import { Injectable } from '@angular/core';
import { OpenApiDataSource, RxapOpenApiDataSource } from '@rxap/open-api/data-source';
import { ReviewControllerGetBestParameter } from '../parameters/review-controller-get-best.parameter';
import { ReviewControllerGetBestResponse } from '../responses/review-controller-get-best.response';

@Injectable({
  providedIn: 'root',
})
@RxapOpenApiDataSource('ReviewController_getBest')
export class ReviewControllerGetBestDataSource
  extends OpenApiDataSource<ReviewControllerGetBestResponse, ReviewControllerGetBestParameter> {
}
