import { Injectable } from '@angular/core';
import { OpenApiDataSource, RxapOpenApiDataSource } from '@rxap/open-api/data-source';
import { HealthControllerDatabaseResponse } from '../responses/health-controller-database.response';

@Injectable({
  providedIn: 'root',
})
@RxapOpenApiDataSource('HealthController_database')
export class HealthControllerDatabaseDataSource extends OpenApiDataSource<HealthControllerDatabaseResponse, void> {
}
