import { Injectable } from '@angular/core';
import { OpenApiDataSource, RxapOpenApiDataSource } from '@rxap/open-api/data-source';
import { RestaurantControllerGetAllResponse } from '../responses/restaurant-controller-get-all.response';

@Injectable({
  providedIn: 'root',
})
@RxapOpenApiDataSource('RestaurantController_getAll')
export class RestaurantControllerGetAllDataSource extends OpenApiDataSource<RestaurantControllerGetAllResponse, void> {
}
