import { Injectable } from '@angular/core';
import { OpenApiDataSource, RxapOpenApiDataSource } from '@rxap/open-api/data-source';
import { AppControllerGetDataResponse } from '../responses/app-controller-get-data.response';

@Injectable({
  providedIn: 'root',
})
@RxapOpenApiDataSource('AppController_getData')
export class AppControllerGetDataDataSource extends OpenApiDataSource<AppControllerGetDataResponse, void> {
}
