import { Injectable } from '@angular/core';
import { OpenApiDataSource, RxapOpenApiDataSource } from '@rxap/open-api/data-source';
import { ReviewControllerGetAllParameter } from '../parameters/review-controller-get-all.parameter';
import { ReviewControllerGetAllResponse } from '../responses/review-controller-get-all.response';

@Injectable({
  providedIn: 'root',
})
@RxapOpenApiDataSource('ReviewController_getAll')
export class ReviewControllerGetAllDataSource
  extends OpenApiDataSource<ReviewControllerGetAllResponse, ReviewControllerGetAllParameter> {
}
