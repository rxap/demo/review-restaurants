import { Injectable } from '@angular/core';
import { OpenApiDataSource, RxapOpenApiDataSource } from '@rxap/open-api/data-source';
import { ReviewControllerGetWorstParameter } from '../parameters/review-controller-get-worst.parameter';
import { ReviewControllerGetWorstResponse } from '../responses/review-controller-get-worst.response';

@Injectable({
  providedIn: 'root',
})
@RxapOpenApiDataSource('ReviewController_getWorst')
export class ReviewControllerGetWorstDataSource
  extends OpenApiDataSource<ReviewControllerGetWorstResponse, ReviewControllerGetWorstParameter> {
}
