import { Injectable } from '@angular/core';
import { OpenApiDataSource, RxapOpenApiDataSource } from '@rxap/open-api/data-source';
import { UserControllerGetAllResponse } from '../responses/user-controller-get-all.response';

@Injectable({
  providedIn: 'root',
})
@RxapOpenApiDataSource('UserController_getAll')
export class UserControllerGetAllDataSource extends OpenApiDataSource<UserControllerGetAllResponse, void> {
}
