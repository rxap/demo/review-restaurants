import { Injectable } from '@angular/core';
import { OpenApiDataSource, RxapOpenApiDataSource } from '@rxap/open-api/data-source';
import { RestaurantControllerGetByIdParameter } from '../parameters/restaurant-controller-get-by-id.parameter';
import { RestaurantControllerGetByIdResponse } from '../responses/restaurant-controller-get-by-id.response';

@Injectable({
  providedIn: 'root',
})
@RxapOpenApiDataSource('RestaurantController_getById')
export class RestaurantControllerGetByIdDataSource
  extends OpenApiDataSource<RestaurantControllerGetByIdResponse, RestaurantControllerGetByIdParameter> {
}
