import { Injectable } from '@angular/core';
import { OpenApiDataSource, RxapOpenApiDataSource } from '@rxap/open-api/data-source';
import { AuthControllerCheckResponse } from '../responses/auth-controller-check.response';

@Injectable({
  providedIn: 'root',
})
@RxapOpenApiDataSource('AuthController_check')
export class AuthControllerCheckDataSource extends OpenApiDataSource<AuthControllerCheckResponse, void> {
}
