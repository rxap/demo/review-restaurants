import { Injectable } from '@angular/core';
import { OpenApiDataSource, RxapOpenApiDataSource } from '@rxap/open-api/data-source';
import { ReviewControllerGetLatestParameter } from '../parameters/review-controller-get-latest.parameter';
import { ReviewControllerGetLatestResponse } from '../responses/review-controller-get-latest.response';

@Injectable({
  providedIn: 'root',
})
@RxapOpenApiDataSource('ReviewController_getLatest')
export class ReviewControllerGetLatestDataSource
  extends OpenApiDataSource<ReviewControllerGetLatestResponse, ReviewControllerGetLatestParameter> {
}
