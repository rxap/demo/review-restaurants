export interface UserControllerGetByIdResponse {
  _id: string;
  username: string;
  role: 'admin' | 'member';
}
