import { UserDtoResponse } from './user-dto.response';

export type UserControllerGetAllResponse = Array<UserDtoResponse>;
