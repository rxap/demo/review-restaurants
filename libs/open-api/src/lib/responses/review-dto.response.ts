import { RestaurantDtoResponse } from './restaurant-dto.response';
import { UserDtoResponse } from './user-dto.response';

export interface ReviewDtoResponse {
  _id: string;
  rating: number;
  date: string;
  comment: string;
  user: UserDtoResponse;
  restaurant: RestaurantDtoResponse;
  dateOfVisit: string;
}
