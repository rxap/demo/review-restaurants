export interface RestaurantControllerCreateResponse {
  _id: string;
  name: string;
  rating: number;
}
