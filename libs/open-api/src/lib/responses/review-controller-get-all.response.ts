import { ReviewDtoResponse } from './review-dto.response';

export type ReviewControllerGetAllResponse = Array<ReviewDtoResponse>;
