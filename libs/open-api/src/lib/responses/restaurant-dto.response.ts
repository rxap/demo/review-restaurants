export interface RestaurantDtoResponse {
  _id: string;
  name: string;
  rating: number;
}
