export interface UserDtoResponse {
  _id: string;
  username: string;
  role: 'admin' | 'member';
}
