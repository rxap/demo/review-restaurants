import { RestaurantDtoResponse } from './restaurant-dto.response';

export type RestaurantControllerGetAllResponse = Array<RestaurantDtoResponse>;
