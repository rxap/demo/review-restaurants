export interface AuthControllerLoginResponse {
  accessToken: string;
}
