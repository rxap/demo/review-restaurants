export interface RestaurantControllerGetByIdResponse {
  _id: string;
  name: string;
  rating: number;
}
