export interface CreateReviewDto {
  rating: number;
  comment: string;
  dateOfVisit: string;
}
