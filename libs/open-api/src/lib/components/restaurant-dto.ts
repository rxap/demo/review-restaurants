export interface RestaurantDto {
  _id: string;
  name: string;
  rating: number;
}
