export interface RegisterDto {
  username: string;
  password: string;
  role: 'admin' | 'member';
}
