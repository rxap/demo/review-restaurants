export interface UpdateUserDto {
  username: string;
  role: 'admin' | 'member';
}
