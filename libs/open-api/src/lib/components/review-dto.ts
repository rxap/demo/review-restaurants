import { RestaurantDto } from './restaurant-dto';
import { UserDto } from './user-dto';

export interface ReviewDto {
  _id: string;
  rating: number;
  date: string;
  comment: string;
  user: UserDto;
  restaurant: RestaurantDto;
  dateOfVisit: string;
}
