export interface UserDto {
  _id: string;
  username: string;
  role: 'admin' | 'member';
}
