export interface UpdateReviewDto {
  rating: number;
  comment: string;
  dateOfVisit: string;
}
