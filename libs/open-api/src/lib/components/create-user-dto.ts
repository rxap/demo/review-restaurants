export interface CreateUserDto {
  username: string;
  password: string;
  role: 'admin' | 'member';
}
