import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { RatingComponent } from './rating.component';


@NgModule({
  declarations: [
    RatingComponent,
  ],
  imports:      [
    CommonModule,
    MatIconModule,
    FlexLayoutModule,
    MatButtonModule,
    MatTooltipModule,
  ],
  exports:      [
    RatingComponent,
  ],
})
export class RatingComponentModule {
}
