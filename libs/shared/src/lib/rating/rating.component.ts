import { ChangeDetectionStrategy, Component, forwardRef, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { ControlValueAccessor } from '@rxap/forms';

@Component({
  selector:        'rr-rating',
  templateUrl:     './rating.component.html',
  styleUrls:       ['./rating.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  host:            { class: 'rr-rating' },
  providers:       [
    {
      provide:     NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => RatingComponent),
      multi:       true,
    },
  ],
})
export class RatingComponent extends ControlValueAccessor<number> {

  get rating(): number {
    return Math.round(this.value);
  }

  get valueText(): string {
    return (Math.round(this.value * 100) / 100).toFixed(2);
  }

  @Input()
  value: number = 0;
  @Input()
  disabled      = false;

  stars = [1, 2, 3, 4, 5];

  constructor() {
    super();
  }

  public writeValue(value: number): void {
    this.value = value;
  }


  public rate(star: number) {
    this.value = star;
    if (this.onChange) {
      this.onChange(this.value);
    }
  }

}
