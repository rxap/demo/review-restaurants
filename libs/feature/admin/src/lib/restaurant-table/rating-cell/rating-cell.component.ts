import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { IRestaurantTable } from '../restaurant-table';

@Component({
  selector:        'td[rxap-rating-cell]',
  templateUrl:     './rating-cell.component.html',
  styleUrls:       ['./rating-cell.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  host:            { class: 'rxap-rating-cell' },
})
export class RatingCellComponent {
  @Input()
  public value!: number;
  @Input()
  public element!: IRestaurantTable;

}
