import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatTableModule } from '@angular/material/table';
import { ConfirmComponentModule } from '@rxap/components';
import { CardProgressBarModule } from '@rxap/material-directives/card';
import {
  PersistentPaginatorDirectiveModule,
  TableColumnMenuComponentModule,
  TableCreateButtonDirectiveModule,
  TableDataSourceModule,
  TableRowActionsModule,
} from '@rxap/material-table-system';
import { RatingCellComponentModule } from './rating-cell/rating-cell.component.module';
import { RestaurantCreateFormComponentModule } from './restaurant-create-form/restaurant-create-form.component.module';
import { RestaurantEditFormComponentModule } from './restaurant-edit-form/restaurant-edit-form.component.module';
import { RestaurantTableComponent } from './restaurant-table.component';

@NgModule({
  declarations: [RestaurantTableComponent],
  imports:      [
    MatCardModule,
    MatProgressBarModule,
    CardProgressBarModule,
    MatTableModule,
    FlexLayoutModule,
    TableDataSourceModule,
    TableColumnMenuComponentModule,
    TableCreateButtonDirectiveModule,
    MatIconModule,
    MatButtonModule,
    RestaurantCreateFormComponentModule,
    MatPaginatorModule,
    PersistentPaginatorDirectiveModule,
    RestaurantEditFormComponentModule,
    ConfirmComponentModule,
    TableRowActionsModule,
    RatingCellComponentModule,
  ],
  exports:      [RestaurantTableComponent],
})
export class RestaurantTableComponentModule {
}
