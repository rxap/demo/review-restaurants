import { RXAP_TABLE_ROW_ACTION_METHOD } from '@rxap/material-table-system';
import { RestaurantTableDeleteTableRowActionMethod } from './restaurant-table-delete-table-row-action.method';
import { RestaurantTableEditTableRowActionMethod } from './restaurant-table-edit-table-row-action.method';

export const TABLE_ROW_ACTION_METHODS = [
  {
    provide:  RXAP_TABLE_ROW_ACTION_METHOD,
    useClass: RestaurantTableEditTableRowActionMethod,
    multi:    true,
  },
  {
    provide:  RXAP_TABLE_ROW_ACTION_METHOD,
    useClass: RestaurantTableDeleteTableRowActionMethod,
    multi:    true,
  },
];
