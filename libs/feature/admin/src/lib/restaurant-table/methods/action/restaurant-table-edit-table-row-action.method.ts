import { Inject, Injectable, INJECTOR, Injector } from '@angular/core';
import { TableActionMethod, TableRowActionTypeMethod } from '@rxap/material-table-system';
import { Method } from '@rxap/utilities/rxjs';
import { OpenRestaurantEditFormWindowMethod } from '../../restaurant-edit-form/open-restaurant-edit-form-window.method';
import { IRestaurantTable } from '../../restaurant-table';

@TableActionMethod('edit')
@Injectable()
export class RestaurantTableEditTableRowActionMethod
  implements Method<any, IRestaurantTable>,
    TableRowActionTypeMethod<IRestaurantTable> {
  constructor(
    @Inject(OpenRestaurantEditFormWindowMethod)
    public readonly openFormWindow: OpenRestaurantEditFormWindowMethod,
    @Inject(INJECTOR) private readonly injector: Injector,
  ) {
  }

  public call(parameters: IRestaurantTable): any {
    console.log(`action row type: edit`, parameters);
    return this.openFormWindow
      .call(parameters, { injector: this.injector })
      .toPromise();
  }
}
