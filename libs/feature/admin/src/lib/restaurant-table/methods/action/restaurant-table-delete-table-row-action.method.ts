import { Injectable } from '@angular/core';
import {
  RestaurantControllerDeleteRemoteMethod,
} from '@review-restaurants/open-api/remote-methods/restaurant-controller-delete.remote-method';
import { TableActionMethod, TableRowActionTypeMethod } from '@rxap/material-table-system';
import { Method } from '@rxap/utilities/rxjs';
import { IRestaurantTable } from '../../restaurant-table';

@TableActionMethod('delete')
@Injectable()
export class RestaurantTableDeleteTableRowActionMethod
  implements Method<any, IRestaurantTable>,
    TableRowActionTypeMethod<IRestaurantTable> {
  constructor(
    private readonly restaurantControllerDelete: RestaurantControllerDeleteRemoteMethod,
  ) {
  }

  public async call(parameters: IRestaurantTable) {
    await this.restaurantControllerDelete.call({
      parameters: {
        restaurantId: parameters._id,
      },
    });
  }
}
