import { Injectable } from '@angular/core';
import {
  RestaurantControllerCreateRemoteMethod,
} from '@review-restaurants/open-api/remote-methods/restaurant-controller-create.remote-method';
import { Method } from '@rxap/utilities/rxjs';
import { IRestaurantCreateForm } from '../restaurant-create-form/restaurant-create.form';

@Injectable()
export class CreateRestaurantMethod
  implements Method<any, IRestaurantCreateForm> {
  constructor(private readonly restaurantControllerCreate: RestaurantControllerCreateRemoteMethod) {
  }

  public async call(parameters: IRestaurantCreateForm) {
    await this.restaurantControllerCreate.call({
      requestBody: {
        name: parameters.name,
      },
    });
  }
}
