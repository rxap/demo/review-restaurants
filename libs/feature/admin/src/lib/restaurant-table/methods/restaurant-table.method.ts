import { Injectable } from '@angular/core';
import { RestaurantDto } from '@review-restaurants/open-api/components/restaurant-dto';
import {
  RestaurantControllerGetAllRemoteMethod,
} from '@review-restaurants/open-api/remote-methods/restaurant-controller-get-all.remote-method';
import { TableEvent } from '@rxap/data-source/table';
import { Method } from '@rxap/utilities/rxjs';

@Injectable()
export class RestaurantTableMethod implements Method<any, any> {
  constructor(private readonly restaurantControllerGetAll: RestaurantControllerGetAllRemoteMethod) {
  }

  public async call({ page, setTotalLength }: TableEvent): Promise<RestaurantDto[]> {
    const restaurantList = await this.restaurantControllerGetAll.call();
    const pageIndex      = page?.pageIndex ?? 0;
    const pageSize       = page?.pageSize ?? Number.MAX_SAFE_INTEGER;
    if (setTotalLength) {
      setTotalLength(restaurantList.length);
    }
    return restaurantList.slice(pageIndex * pageSize, (pageIndex + 1) * pageSize);
  }
}
