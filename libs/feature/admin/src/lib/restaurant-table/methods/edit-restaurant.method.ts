import { Inject, Injectable } from '@angular/core';
import {
  RestaurantControllerUpdateRemoteMethod,
} from '@review-restaurants/open-api/remote-methods/restaurant-controller-update.remote-method';
import { RXAP_FORM_INITIAL_STATE } from '@rxap/forms';
import { Method } from '@rxap/utilities/rxjs';
import { IRestaurantTable } from '../../restaurant-table/restaurant-table';
import { IRestaurantEditForm } from '../restaurant-edit-form/restaurant-edit.form';

@Injectable()
export class EditRestaurantMethod implements Method<any, IRestaurantEditForm> {
  constructor(
    private readonly restaurantControllerUpdate: RestaurantControllerUpdateRemoteMethod,
    @Inject(RXAP_FORM_INITIAL_STATE)
    private readonly context: IRestaurantTable,
  ) {
  }

  public async call(parameters: IRestaurantEditForm) {
    await this.restaurantControllerUpdate.call({
      parameters:  {
        restaurantId: this.context._id,
      },
      requestBody: {
        name: parameters.name,
      },
    });
  }
}
