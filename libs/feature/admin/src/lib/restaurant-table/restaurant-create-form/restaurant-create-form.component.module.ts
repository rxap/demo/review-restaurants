import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormWindowFooterDirectiveModule } from '@rxap/form-window-system';
import { RxapFormsModule } from '@rxap/forms';
import {
  ControlErrorDirectiveModule,
  FormControlsComponentModule,
  RequiredDirectiveModule,
} from '@rxap/material-form-system';
import { OpenRestaurantCreateFormWindowMethod } from './open-restaurant-create-form-window.method';
import { RestaurantCreateFormComponent } from './restaurant-create-form.component';

@NgModule({
  declarations: [RestaurantCreateFormComponent],
  imports:      [
    ReactiveFormsModule,
    FlexLayoutModule,
    MatFormFieldModule,
    ControlErrorDirectiveModule,
    MatInputModule,
    RequiredDirectiveModule,
    FormWindowFooterDirectiveModule,
    FormControlsComponentModule,
    RxapFormsModule,
  ],
  exports:      [RestaurantCreateFormComponent],
  providers:    [OpenRestaurantCreateFormWindowMethod],
})
export class RestaurantCreateFormComponentModule {
}
