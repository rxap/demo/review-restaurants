import { INJECTOR, Injector, Optional, Provider } from '@angular/core';
import {
  RXAP_FORM_DEFINITION,
  RXAP_FORM_DEFINITION_BUILDER,
  RXAP_FORM_INITIAL_STATE,
  RxapFormBuilder,
} from '@rxap/forms';
import { IRestaurantCreateForm, RestaurantCreateForm } from './restaurant-create.form';

export const FormProviders: Provider[]          = [RestaurantCreateForm];
export const FormComponentProviders: Provider[] = [
  {
    provide:    RXAP_FORM_DEFINITION,
    useFactory: FormFactory,
    deps:       [INJECTOR, [new Optional(), RXAP_FORM_INITIAL_STATE]],
  },
];

export function FormFactory(
  injector: Injector,
  state: IRestaurantCreateForm | null,
): RestaurantCreateForm {
  return new RxapFormBuilder<IRestaurantCreateForm>(
    RestaurantCreateForm,
    injector,
  ).build(state ?? {});
}

export const FormBuilderProviders: Provider[] = [
  {
    provide:    RXAP_FORM_DEFINITION_BUILDER,
    useFactory: FormBuilderFactory,
    deps:       [INJECTOR],
  },
];

export function FormBuilderFactory(
  injector: Injector,
): RxapFormBuilder<RestaurantCreateForm> {
  return new RxapFormBuilder<IRestaurantCreateForm>(
    RestaurantCreateForm,
    injector,
  );
}
