import { ChangeDetectionStrategy, Component } from '@angular/core';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { RXAP_FORM_SUBMIT_METHOD } from '@rxap/forms';
import { RXAP_WINDOW_SETTINGS } from '@rxap/window-system';
import { CreateRestaurantMethod } from '../methods/create-restaurant.method';
import { FormComponentProviders, FormProviders } from './form.providers';

@Component({
  selector:        'review-restaurants-restaurant-create-form',
  templateUrl:     './restaurant-create-form.component.html',
  styleUrls:       ['./restaurant-create-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  host:            { class: 'review-restaurants-restaurant-create-form' },
  providers:       [
    {
      provide:  MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'fill',
      },
    },
    FormProviders,
    FormComponentProviders,
    {
      provide:  RXAP_WINDOW_SETTINGS,
      useValue: {
        title: $localize`Create Restaurant`,
      },
    },
    {
      provide:  RXAP_FORM_SUBMIT_METHOD,
      useClass: CreateRestaurantMethod,
    },
  ],
})
export class RestaurantCreateFormComponent {
}

export function WindowOptionsFactory() {
  return {
    title: $localize`Create Restaurant`,
  };
}
