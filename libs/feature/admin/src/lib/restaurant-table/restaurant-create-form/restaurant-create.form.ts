import { Injectable } from '@angular/core';
import { Validators } from '@angular/forms';
import { FormType, RxapForm, RxapFormControl, RxapFormGroup, RxapValidators, UseFormControl } from '@rxap/forms';

export interface IRestaurantCreateForm {
  name: string;
}

@Injectable()
@RxapForm('restaurant-create')
export class RestaurantCreateForm implements FormType<IRestaurantCreateForm> {
  public rxapFormGroup!: RxapFormGroup<IRestaurantCreateForm>;
  @UseFormControl({
    validators: [Validators.required, RxapValidators.IsString()],
  })
  public name!: RxapFormControl<string>;

  [key: string]: any;
}
