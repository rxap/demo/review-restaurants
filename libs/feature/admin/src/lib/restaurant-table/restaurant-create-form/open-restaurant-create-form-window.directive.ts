import { Directive, Inject, INJECTOR, Injector, NgModule } from '@angular/core';
import { OpenFormWindowMethodDirective } from '@rxap/form-window-system';
import { OpenRestaurantCreateFormWindowMethod } from './open-restaurant-create-form-window.method';
import { RestaurantCreateFormComponentModule } from './restaurant-create-form.component.module';
import { IRestaurantCreateForm } from './restaurant-create.form';

@Directive({
  selector: '[mfdOpenRestaurantCreateFormWindow]',
})
export class OpenRestaurantCreateFormWindowMethodDirective extends OpenFormWindowMethodDirective<IRestaurantCreateForm,
  Partial<IRestaurantCreateForm>> {
  constructor(
    public readonly method: OpenRestaurantCreateFormWindowMethod,
    @Inject(INJECTOR) injector: Injector,
  ) {
    super(injector);
  }
}

@NgModule({
  imports:      [RestaurantCreateFormComponentModule],
  exports:      [OpenRestaurantCreateFormWindowMethodDirective],
  declarations: [OpenRestaurantCreateFormWindowMethodDirective],
  providers:    [OpenRestaurantCreateFormWindowMethod],
})
export class OpenRestaurantCreateFormWindowMethodDirectiveModule {
}
