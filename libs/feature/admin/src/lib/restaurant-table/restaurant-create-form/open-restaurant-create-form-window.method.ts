import { Inject, Injectable, INJECTOR, Injector, Optional } from '@angular/core';
import {
  FormWindowOptions,
  FormWindowService,
  OpenFormWindowMethod,
  RXAP_FORM_WINDOW_SYSTEM_OPEN_FORM_DEFAULT_OPTIONS,
} from '@rxap/form-window-system';
import { RestaurantCreateFormComponent } from './restaurant-create-form.component';
import { IRestaurantCreateForm, RestaurantCreateForm } from './restaurant-create.form';

@Injectable()
export class OpenRestaurantCreateFormWindowMethod extends OpenFormWindowMethod<IRestaurantCreateForm> {
  constructor(
    @Inject(FormWindowService) formWindowService: FormWindowService,
    @Inject(INJECTOR) injector: Injector,
    @Optional()
    @Inject(RXAP_FORM_WINDOW_SYSTEM_OPEN_FORM_DEFAULT_OPTIONS)
      defaultOptions: FormWindowOptions<IRestaurantCreateForm> | null,
  ) {
    super(
      formWindowService,
      RestaurantCreateForm,
      injector,
      RestaurantCreateFormComponent,
      defaultOptions,
    );
  }
}
