import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { RowAnimation, RXAP_TABLE_METHOD, TABLE_CREATE_REMOTE_METHOD } from '@rxap/material-table-system';
import { Observable } from 'rxjs';
import { TABLE_ROW_ACTION_METHODS } from './methods/action/index';
import { RestaurantTableMethod } from './methods/restaurant-table.method';
import { FormProviders as CreateFormProviders } from './restaurant-create-form/form.providers';
import {
  OpenRestaurantCreateFormWindowMethod,
} from './restaurant-create-form/open-restaurant-create-form-window.method';
import { FormProviders as EditRowActionFormProviders } from './restaurant-edit-form/form.providers';

@Component({
  selector:        'review-restaurants-restaurant-table',
  templateUrl:     './restaurant-table.component.html',
  styleUrls:       ['./restaurant-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  host:            { class: 'review-restaurants-restaurant-table' },
  animations:      [RowAnimation],
  providers:       [
    {
      provide:  RXAP_TABLE_METHOD,
      useClass: RestaurantTableMethod,
    },
    {
      provide:  TABLE_CREATE_REMOTE_METHOD,
      useClass: OpenRestaurantCreateFormWindowMethod,
    },
    CreateFormProviders,
    EditRowActionFormProviders,
    TABLE_ROW_ACTION_METHODS,
  ],
})
export class RestaurantTableComponent {
  @Input()
  public parameters?: Observable<Record<string, any>>;
}
