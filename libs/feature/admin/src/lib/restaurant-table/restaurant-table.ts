import { TableRowMetadata } from '@rxap/material-table-system';

export interface IRestaurantTable
  extends Record<string, unknown>,
    TableRowMetadata {
  _id: any;
  name: any;
  rating: any;
}
