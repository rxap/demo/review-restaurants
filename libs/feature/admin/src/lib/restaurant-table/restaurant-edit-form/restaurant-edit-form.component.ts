import { ChangeDetectionStrategy, Component } from '@angular/core';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { RXAP_FORM_SUBMIT_METHOD } from '@rxap/forms';
import { RXAP_WINDOW_SETTINGS } from '@rxap/window-system';
import { EditRestaurantMethod } from '../methods/edit-restaurant.method';
import { FormComponentProviders, FormProviders } from './form.providers';

@Component({
  selector:        'review-restaurants-restaurant-edit-form',
  templateUrl:     './restaurant-edit-form.component.html',
  styleUrls:       ['./restaurant-edit-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  host:            { class: 'review-restaurants-restaurant-edit-form' },
  providers:       [
    {
      provide:  MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'fill',
      },
    },
    FormProviders,
    FormComponentProviders,
    {
      provide:  RXAP_WINDOW_SETTINGS,
      useValue: {
        title: $localize`Edit Restaurant`,
      },
    },
    {
      provide:  RXAP_FORM_SUBMIT_METHOD,
      useClass: EditRestaurantMethod,
    },
  ],
})
export class RestaurantEditFormComponent {
}

export function WindowOptionsFactory() {
  return {
    title: $localize`Edit Restaurant`,
  };
}
