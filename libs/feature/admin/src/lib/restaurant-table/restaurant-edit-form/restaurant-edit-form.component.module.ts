import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormWindowFooterDirectiveModule } from '@rxap/form-window-system';
import { RxapFormsModule } from '@rxap/forms';
import {
  ControlErrorDirectiveModule,
  FormControlsComponentModule,
  RequiredDirectiveModule,
} from '@rxap/material-form-system';
import { OpenRestaurantEditFormWindowMethod } from './open-restaurant-edit-form-window.method';
import { RestaurantEditFormComponent } from './restaurant-edit-form.component';

@NgModule({
  declarations: [RestaurantEditFormComponent],
  imports:      [
    ReactiveFormsModule,
    FlexLayoutModule,
    MatFormFieldModule,
    ControlErrorDirectiveModule,
    MatInputModule,
    RequiredDirectiveModule,
    FormWindowFooterDirectiveModule,
    FormControlsComponentModule,
    RxapFormsModule,
  ],
  exports:      [RestaurantEditFormComponent],
  providers:    [OpenRestaurantEditFormWindowMethod],
})
export class RestaurantEditFormComponentModule {
}
