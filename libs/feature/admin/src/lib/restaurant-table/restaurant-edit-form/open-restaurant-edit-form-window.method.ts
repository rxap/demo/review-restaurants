import { Inject, Injectable, INJECTOR, Injector, Optional } from '@angular/core';
import {
  FormWindowOptions,
  FormWindowService,
  OpenFormWindowMethod,
  RXAP_FORM_WINDOW_SYSTEM_OPEN_FORM_DEFAULT_OPTIONS,
} from '@rxap/form-window-system';
import { RestaurantEditFormComponent } from './restaurant-edit-form.component';
import { IRestaurantEditForm, RestaurantEditForm } from './restaurant-edit.form';

@Injectable()
export class OpenRestaurantEditFormWindowMethod extends OpenFormWindowMethod<IRestaurantEditForm> {
  constructor(
    @Inject(FormWindowService) formWindowService: FormWindowService,
    @Inject(INJECTOR) injector: Injector,
    @Optional()
    @Inject(RXAP_FORM_WINDOW_SYSTEM_OPEN_FORM_DEFAULT_OPTIONS)
      defaultOptions: FormWindowOptions<IRestaurantEditForm> | null,
  ) {
    super(
      formWindowService,
      RestaurantEditForm,
      injector,
      RestaurantEditFormComponent,
      defaultOptions,
    );
  }
}
