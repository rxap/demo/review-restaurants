import { Injectable } from '@angular/core';
import { Validators } from '@angular/forms';
import { FormType, RxapForm, RxapFormControl, RxapFormGroup, RxapValidators, UseFormControl } from '@rxap/forms';

export interface IRestaurantEditForm {
  name: string;
}

@Injectable()
@RxapForm('restaurant-edit')
export class RestaurantEditForm implements FormType<IRestaurantEditForm> {
  public rxapFormGroup!: RxapFormGroup<IRestaurantEditForm>;
  @UseFormControl({
    validators: [Validators.required, RxapValidators.IsString()],
  })
  public name!: RxapFormControl<string>;

  [key: string]: any;
}
