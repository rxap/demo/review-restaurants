import { Directive, Inject, INJECTOR, Injector, NgModule } from '@angular/core';
import { OpenFormWindowMethodDirective } from '@rxap/form-window-system';
import { OpenRestaurantEditFormWindowMethod } from './open-restaurant-edit-form-window.method';
import { RestaurantEditFormComponentModule } from './restaurant-edit-form.component.module';
import { IRestaurantEditForm } from './restaurant-edit.form';

@Directive({
  selector: '[mfdOpenRestaurantEditFormWindow]',
})
export class OpenRestaurantEditFormWindowMethodDirective extends OpenFormWindowMethodDirective<IRestaurantEditForm,
  Partial<IRestaurantEditForm>> {
  constructor(
    public readonly method: OpenRestaurantEditFormWindowMethod,
    @Inject(INJECTOR) injector: Injector,
  ) {
    super(injector);
  }
}

@NgModule({
  imports:      [RestaurantEditFormComponentModule],
  exports:      [OpenRestaurantEditFormWindowMethodDirective],
  declarations: [OpenRestaurantEditFormWindowMethodDirective],
  providers:    [OpenRestaurantEditFormWindowMethod],
})
export class OpenRestaurantEditFormWindowMethodDirectiveModule {
}
