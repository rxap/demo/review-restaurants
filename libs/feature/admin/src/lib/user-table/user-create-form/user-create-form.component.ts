import { ChangeDetectionStrategy, Component } from '@angular/core';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { RXAP_FORM_SUBMIT_METHOD } from '@rxap/forms';
import { RXAP_WINDOW_SETTINGS } from '@rxap/window-system';
import { CreateUserMethod } from '../methods/create-user.method';
import { FormComponentProviders, FormProviders } from './form.providers';

@Component({
  selector:        'review-restaurants-user-create-form',
  templateUrl:     './user-create-form.component.html',
  styleUrls:       ['./user-create-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  host:            { class: 'review-restaurants-user-create-form' },
  providers:       [
    FormProviders,
    FormComponentProviders,
    {
      provide:  MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'fill',
      },
    },
    {
      provide:  RXAP_WINDOW_SETTINGS,
      useValue: {
        title: $localize`Create User`,
      },
    },
    {
      provide:  RXAP_FORM_SUBMIT_METHOD,
      useClass: CreateUserMethod,
    },
  ],
})
export class UserCreateFormComponent {
}

export function WindowOptionsFactory() {
  return {
    title: $localize`Create User`,
  };
}
