import { Injectable } from '@angular/core';
import { Validators } from '@angular/forms';
import { UseOptionsDataSource } from '@rxap/form-system';
import { FormType, RxapForm, RxapFormControl, RxapFormGroup, RxapValidators, UseFormControl } from '@rxap/forms';
import { RoleOptionsDataSource } from './data-sources/role.options.data-source';

export interface IUserCreateForm {
  username: string;
  password: string;
  role: any;
}

@Injectable()
@RxapForm('user-create')
export class UserCreateForm implements FormType<IUserCreateForm> {
  public rxapFormGroup!: RxapFormGroup<IUserCreateForm>;
  @UseFormControl({
    validators: [Validators.required, RxapValidators.IsString()],
  })
  public username!: RxapFormControl<string>;
  @UseFormControl({
    validators: [Validators.required, RxapValidators.IsString()],
  })
  public password!: RxapFormControl<string>;

  @UseOptionsDataSource(RoleOptionsDataSource)
  @UseFormControl({
    validators: [Validators.required],
    state:      'member',
  })
  public role!: RxapFormControl<any>;

  [key: string]: any;
}
