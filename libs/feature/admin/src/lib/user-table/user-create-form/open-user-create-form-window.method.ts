import { Inject, Injectable, INJECTOR, Injector, Optional } from '@angular/core';
import {
  FormWindowOptions,
  FormWindowService,
  OpenFormWindowMethod,
  RXAP_FORM_WINDOW_SYSTEM_OPEN_FORM_DEFAULT_OPTIONS,
} from '@rxap/form-window-system';
import { UserCreateFormComponent } from './user-create-form.component';
import { IUserCreateForm, UserCreateForm } from './user-create.form';

@Injectable()
export class OpenUserCreateFormWindowMethod extends OpenFormWindowMethod<IUserCreateForm> {
  constructor(
    @Inject(FormWindowService) formWindowService: FormWindowService,
    @Inject(INJECTOR) injector: Injector,
    @Optional()
    @Inject(RXAP_FORM_WINDOW_SYSTEM_OPEN_FORM_DEFAULT_OPTIONS)
      defaultOptions: FormWindowOptions<IUserCreateForm> | null,
  ) {
    super(
      formWindowService,
      UserCreateForm,
      injector,
      UserCreateFormComponent,
      defaultOptions,
    );
  }
}
