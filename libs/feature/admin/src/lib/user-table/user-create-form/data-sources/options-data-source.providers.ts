import { Provider } from '@angular/core';
import { RoleOptionsDataSource } from './role.options.data-source';

export const OptionsProviders: Provider[] = [RoleOptionsDataSource];
