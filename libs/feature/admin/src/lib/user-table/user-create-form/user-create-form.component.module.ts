import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { InputSelectOptionsDirectiveModule } from '@rxap/form-system';
import { FormWindowFooterDirectiveModule } from '@rxap/form-window-system';
import { RxapFormsModule } from '@rxap/forms';
import {
  CompareWithDirectiveModule,
  ControlErrorDirectiveModule,
  FormControlsComponentModule,
  RequiredDirectiveModule,
} from '@rxap/material-form-system';
import { OpenUserCreateFormWindowMethod } from './open-user-create-form-window.method';
import { UserCreateFormComponent } from './user-create-form.component';

@NgModule({
  declarations: [UserCreateFormComponent],
  imports:      [
    ReactiveFormsModule,
    FlexLayoutModule,
    MatFormFieldModule,
    ControlErrorDirectiveModule,
    MatInputModule,
    RequiredDirectiveModule,
    CompareWithDirectiveModule,
    MatSelectModule,
    InputSelectOptionsDirectiveModule,
    FormWindowFooterDirectiveModule,
    FormControlsComponentModule,
    RxapFormsModule,
  ],
  exports:      [UserCreateFormComponent],
  providers:    [OpenUserCreateFormWindowMethod],
})
export class UserCreateFormComponentModule {
}
