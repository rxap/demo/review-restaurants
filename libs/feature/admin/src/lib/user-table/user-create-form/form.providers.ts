import { INJECTOR, Injector, Optional, Provider } from '@angular/core';
import {
  RXAP_FORM_DEFINITION,
  RXAP_FORM_DEFINITION_BUILDER,
  RXAP_FORM_INITIAL_STATE,
  RxapFormBuilder,
} from '@rxap/forms';
import { OptionsProviders } from './data-sources/options-data-source.providers';
import { IUserCreateForm, UserCreateForm } from './user-create.form';

export const FormProviders: Provider[]          = [UserCreateForm, OptionsProviders];
export const FormComponentProviders: Provider[] = [
  {
    provide:    RXAP_FORM_DEFINITION,
    useFactory: FormFactory,
    deps:       [INJECTOR, [new Optional(), RXAP_FORM_INITIAL_STATE]],
  },
];

export function FormFactory(
  injector: Injector,
  state: IUserCreateForm | null,
): UserCreateForm {
  return new RxapFormBuilder<IUserCreateForm>(UserCreateForm, injector).build(
    state ?? {},
  );
}

export const FormBuilderProviders: Provider[] = [
  {
    provide:    RXAP_FORM_DEFINITION_BUILDER,
    useFactory: FormBuilderFactory,
    deps:       [INJECTOR],
  },
];

export function FormBuilderFactory(
  injector: Injector,
): RxapFormBuilder<UserCreateForm> {
  return new RxapFormBuilder<IUserCreateForm>(UserCreateForm, injector);
}
