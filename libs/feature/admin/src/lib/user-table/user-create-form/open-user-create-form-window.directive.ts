import { Directive, Inject, INJECTOR, Injector, NgModule } from '@angular/core';
import { OpenFormWindowMethodDirective } from '@rxap/form-window-system';
import { OpenUserCreateFormWindowMethod } from './open-user-create-form-window.method';
import { UserCreateFormComponentModule } from './user-create-form.component.module';
import { IUserCreateForm } from './user-create.form';

@Directive({
  selector: '[mfdOpenUserCreateFormWindow]',
})
export class OpenUserCreateFormWindowMethodDirective extends OpenFormWindowMethodDirective<IUserCreateForm,
  Partial<IUserCreateForm>> {
  constructor(
    public readonly method: OpenUserCreateFormWindowMethod,
    @Inject(INJECTOR) injector: Injector,
  ) {
    super(injector);
  }
}

@NgModule({
  imports:      [UserCreateFormComponentModule],
  exports:      [OpenUserCreateFormWindowMethodDirective],
  declarations: [OpenUserCreateFormWindowMethodDirective],
  providers:    [OpenUserCreateFormWindowMethod],
})
export class OpenUserCreateFormWindowMethodDirectiveModule {
}
