import { Directive, Inject, INJECTOR, Injector, NgModule } from '@angular/core';
import { OpenFormWindowMethodDirective } from '@rxap/form-window-system';
import { OpenUserEditFormWindowMethod } from './open-user-edit-form-window.method';
import { UserEditFormComponentModule } from './user-edit-form.component.module';
import { IUserEditForm } from './user-edit.form';

@Directive({
  selector: '[mfdOpenUserEditFormWindow]',
})
export class OpenUserEditFormWindowMethodDirective extends OpenFormWindowMethodDirective<IUserEditForm,
  Partial<IUserEditForm>> {
  constructor(
    public readonly method: OpenUserEditFormWindowMethod,
    @Inject(INJECTOR) injector: Injector,
  ) {
    super(injector);
  }
}

@NgModule({
  imports:      [UserEditFormComponentModule],
  exports:      [OpenUserEditFormWindowMethodDirective],
  declarations: [OpenUserEditFormWindowMethodDirective],
  providers:    [OpenUserEditFormWindowMethod],
})
export class OpenUserEditFormWindowMethodDirectiveModule {
}
