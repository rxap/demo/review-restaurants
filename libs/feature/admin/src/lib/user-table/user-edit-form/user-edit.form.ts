import { Injectable } from '@angular/core';
import { Validators } from '@angular/forms';
import { UseOptionsDataSource } from '@rxap/form-system';
import { FormType, RxapForm, RxapFormControl, RxapFormGroup, RxapValidators, UseFormControl } from '@rxap/forms';
import { RoleOptionsDataSource } from './data-sources/role.options.data-source';

export interface IUserEditForm {
  username: string;
  role: any;
}

@Injectable()
@RxapForm('user-edit')
export class UserEditForm implements FormType<IUserEditForm> {
  public rxapFormGroup!: RxapFormGroup<IUserEditForm>;
  @UseFormControl({
    validators: [Validators.required, RxapValidators.IsString()],
  })
  public username!: RxapFormControl<string>;

  @UseOptionsDataSource(RoleOptionsDataSource)
  @UseFormControl({
    validators: [Validators.required],
    state:      'member',
  })
  public role!: RxapFormControl<any>;

  [key: string]: any;
}
