import { Inject, Injectable, INJECTOR, Injector, Optional } from '@angular/core';
import {
  FormWindowOptions,
  FormWindowService,
  OpenFormWindowMethod,
  RXAP_FORM_WINDOW_SYSTEM_OPEN_FORM_DEFAULT_OPTIONS,
} from '@rxap/form-window-system';
import { UserEditFormComponent } from './user-edit-form.component';
import { IUserEditForm, UserEditForm } from './user-edit.form';

@Injectable()
export class OpenUserEditFormWindowMethod extends OpenFormWindowMethod<IUserEditForm> {
  constructor(
    @Inject(FormWindowService) formWindowService: FormWindowService,
    @Inject(INJECTOR) injector: Injector,
    @Optional()
    @Inject(RXAP_FORM_WINDOW_SYSTEM_OPEN_FORM_DEFAULT_OPTIONS)
      defaultOptions: FormWindowOptions<IUserEditForm> | null,
  ) {
    super(
      formWindowService,
      UserEditForm,
      injector,
      UserEditFormComponent,
      defaultOptions,
    );
  }
}
