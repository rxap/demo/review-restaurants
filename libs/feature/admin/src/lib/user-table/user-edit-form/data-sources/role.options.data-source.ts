import { Injectable } from '@angular/core';
import { RxapStaticDataSource, StaticDataSource } from '@rxap/data-source';
import { ControlOptions } from '@rxap/utilities';

@Injectable()
@RxapStaticDataSource({
  id:   'role-options',
  data: [
    {
      value:   'admin',
      display: 'admin',
    },
    {
      value:   'member',
      display: 'member',
    },
  ],
})
export class RoleOptionsDataSource extends StaticDataSource<ControlOptions> {
}
