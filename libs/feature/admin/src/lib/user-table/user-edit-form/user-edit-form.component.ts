import { ChangeDetectionStrategy, Component } from '@angular/core';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { RXAP_FORM_SUBMIT_METHOD } from '@rxap/forms';
import { RXAP_WINDOW_SETTINGS } from '@rxap/window-system';
import { EditUserMethod } from '../methods/edit-user.method';
import { FormComponentProviders, FormProviders } from './form.providers';

@Component({
  selector:        'review-restaurants-user-edit-form',
  templateUrl:     './user-edit-form.component.html',
  styleUrls:       ['./user-edit-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  host:            { class: 'review-restaurants-user-edit-form' },
  providers:       [
    FormProviders,
    FormComponentProviders,
    {
      provide:  MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'fill',
      },
    },
    {
      provide:  RXAP_WINDOW_SETTINGS,
      useValue: {
        title: $localize`Edit User`,
      },
    },
    {
      provide:  RXAP_FORM_SUBMIT_METHOD,
      useClass: EditUserMethod,
    },
  ],
})
export class UserEditFormComponent {
}

export function WindowOptionsFactory() {
  return {
    title: $localize`Edit User`,
  };
}
