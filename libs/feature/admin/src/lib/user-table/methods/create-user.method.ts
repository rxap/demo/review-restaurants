import { Injectable } from '@angular/core';
import {
  UserControllerCreateRemoteMethod,
} from '@review-restaurants/open-api/remote-methods/user-controller-create.remote-method';
import { Method } from '@rxap/utilities/rxjs';
import { HashPassword } from '../../../../../authentication/src/lib/hash-password';
import { IUserCreateForm } from '../user-create-form/user-create.form';

@Injectable()
export class CreateUserMethod implements Method<any, IUserCreateForm> {

  constructor(private readonly userControllerCreate: UserControllerCreateRemoteMethod) {
  }

  public async call(parameters: IUserCreateForm) {
    await this.userControllerCreate.call({
      requestBody: {
        username: parameters.username,
        role:     parameters.role,
        password: await HashPassword(parameters.password),
      },
    });
  }
}
