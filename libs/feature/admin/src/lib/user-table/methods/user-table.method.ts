import { Injectable } from '@angular/core';
import { UserDto } from '@review-restaurants/open-api/components/user-dto';
import {
  UserControllerGetAllRemoteMethod,
} from '@review-restaurants/open-api/remote-methods/user-controller-get-all.remote-method';
import { TableEvent } from '@rxap/data-source/table';
import { Method } from '@rxap/utilities/rxjs';

@Injectable()
export class UserTableMethod implements Method<any, any> {

  constructor(private readonly userControllerGetAll: UserControllerGetAllRemoteMethod) {
  }

  public async call({ page, setTotalLength }: TableEvent): Promise<UserDto[]> {
    const userList  = await this.userControllerGetAll.call();
    const pageIndex = page?.pageIndex ?? 0;
    const pageSize  = page?.pageSize ?? Number.MAX_SAFE_INTEGER;
    if (setTotalLength) {
      setTotalLength(userList.length);
    }
    return userList.slice(pageIndex * pageSize, (pageIndex + 1) * pageSize);
  }
}
