import { RXAP_TABLE_ROW_ACTION_METHOD } from '@rxap/material-table-system';
import { UserTableDeleteTableRowActionMethod } from './user-table-delete-table-row-action.method';
import { UserTableEditTableRowActionMethod } from './user-table-edit-table-row-action.method';

export const TABLE_ROW_ACTION_METHODS = [
  {
    provide:  RXAP_TABLE_ROW_ACTION_METHOD,
    useClass: UserTableEditTableRowActionMethod,
    multi:    true,
  },
  {
    provide:  RXAP_TABLE_ROW_ACTION_METHOD,
    useClass: UserTableDeleteTableRowActionMethod,
    multi:    true,
  },
];
