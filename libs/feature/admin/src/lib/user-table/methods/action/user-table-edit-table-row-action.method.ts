import { Inject, Injectable, INJECTOR, Injector } from '@angular/core';
import { TableActionMethod, TableRowActionTypeMethod } from '@rxap/material-table-system';
import { Method } from '@rxap/utilities/rxjs';
import { OpenUserEditFormWindowMethod } from '../../user-edit-form/open-user-edit-form-window.method';
import { IUserTable } from '../../user-table';

@TableActionMethod('edit')
@Injectable()
export class UserTableEditTableRowActionMethod
  implements Method<any, IUserTable>, TableRowActionTypeMethod<IUserTable> {
  constructor(
    @Inject(OpenUserEditFormWindowMethod)
    public readonly openFormWindow: OpenUserEditFormWindowMethod,
    @Inject(INJECTOR) private readonly injector: Injector,
  ) {
  }

  public call(parameters: IUserTable): any {
    console.log(`action row type: edit`, parameters);
    return this.openFormWindow
      .call(parameters, { injector: this.injector })
      .toPromise();
  }
}
