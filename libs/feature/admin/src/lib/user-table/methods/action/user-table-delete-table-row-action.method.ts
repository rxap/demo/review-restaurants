import { Injectable } from '@angular/core';
import {
  UserControllerDeleteRemoteMethod,
} from '@review-restaurants/open-api/remote-methods/user-controller-delete.remote-method';
import { TableActionMethod, TableRowActionTypeMethod } from '@rxap/material-table-system';
import { Method } from '@rxap/utilities/rxjs';
import { IUserTable } from '../../user-table';

@TableActionMethod('delete')
@Injectable()
export class UserTableDeleteTableRowActionMethod
  implements Method<any, IUserTable>, TableRowActionTypeMethod<IUserTable> {
  constructor(
    private readonly userControllerDelete: UserControllerDeleteRemoteMethod,
  ) {
  }

  public async call(parameters: IUserTable) {
    await this.userControllerDelete.call({
      parameters: {
        userId: parameters._id,
      },
    });
  }
}
