import { Inject, Injectable } from '@angular/core';
import {
  UserControllerUpdateRemoteMethod,
} from '@review-restaurants/open-api/remote-methods/user-controller-update.remote-method';
import { RXAP_FORM_INITIAL_STATE } from '@rxap/forms';
import { Method } from '@rxap/utilities/rxjs';
import { IUserEditForm } from '../user-edit-form/user-edit.form';
import { IUserTable } from '../user-table';

@Injectable()
export class EditUserMethod implements Method<any, IUserEditForm> {
  constructor(
    private readonly userControllerUpdate: UserControllerUpdateRemoteMethod,
    @Inject(RXAP_FORM_INITIAL_STATE)
    private readonly context: IUserTable,
  ) {
  }

  public async call(parameters: IUserEditForm) {
    await this.userControllerUpdate.call({
      parameters: {
        userId: this.context._id,
      },
      requestBody: {
        username: parameters.username,
        role:     parameters.role,
      },
    });
  }
}
