import { TableRowMetadata } from '@rxap/material-table-system';

export interface IUserTable extends Record<string, unknown>, TableRowMetadata {
  _id: any;
  username: any;
  role: any;
}
