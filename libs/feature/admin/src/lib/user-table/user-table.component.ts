import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { RowAnimation, RXAP_TABLE_METHOD, TABLE_CREATE_REMOTE_METHOD } from '@rxap/material-table-system';
import { Observable } from 'rxjs';
import { TABLE_ROW_ACTION_METHODS } from './methods/action/index';
import { UserTableMethod } from './methods/user-table.method';
import { FormProviders as CreateFormProviders } from './user-create-form/form.providers';
import { OpenUserCreateFormWindowMethod } from './user-create-form/open-user-create-form-window.method';
import { FormProviders as EditRowActionFormProviders } from './user-edit-form/form.providers';

@Component({
  selector:        'review-restaurants-user-table',
  templateUrl:     './user-table.component.html',
  styleUrls:       ['./user-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  host:            { class: 'review-restaurants-user-table' },
  animations:      [RowAnimation],
  providers:       [
    CreateFormProviders,
    EditRowActionFormProviders,
    TABLE_ROW_ACTION_METHODS,
    {
      provide:  RXAP_TABLE_METHOD,
      useClass: UserTableMethod,
    },
    {
      provide:  TABLE_CREATE_REMOTE_METHOD,
      useClass: OpenUserCreateFormWindowMethod,
    },
  ],
})
export class UserTableComponent {
  @Input()
  public parameters?: Observable<Record<string, any>>;
}
