import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatTableModule } from '@angular/material/table';
import { ConfirmComponentModule, NavigateBackButtonComponentModule } from '@rxap/components';
import { CardProgressBarModule } from '@rxap/material-directives/card';
import {
  PersistentPaginatorDirectiveModule,
  TableColumnMenuComponentModule,
  TableCreateButtonDirectiveModule,
  TableDataSourceModule,
  TableRowActionsModule,
} from '@rxap/material-table-system';
import { UserCreateFormComponentModule } from './user-create-form/user-create-form.component.module';
import { UserEditFormComponentModule } from './user-edit-form/user-edit-form.component.module';
import { UserTableComponent } from './user-table.component';

@NgModule({
  declarations: [UserTableComponent],
  imports:      [
    MatCardModule,
    MatProgressBarModule,
    CardProgressBarModule,
    MatTableModule,
    FlexLayoutModule,
    TableDataSourceModule,
    TableColumnMenuComponentModule,
    TableCreateButtonDirectiveModule,
    MatIconModule,
    MatButtonModule,
    UserCreateFormComponentModule,
    MatPaginatorModule,
    UserEditFormComponentModule,
    MatDividerModule,
    NavigateBackButtonComponentModule,
    TableRowActionsModule,
    ConfirmComponentModule,
    PersistentPaginatorDirectiveModule,
  ],
  exports:      [UserTableComponent],
})
export class UserTableComponentModule {
}
