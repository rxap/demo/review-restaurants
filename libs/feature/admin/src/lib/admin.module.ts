import { NgModule } from '@angular/core';
import { MatNativeDateModule } from '@angular/material/core';
import { RouterModule } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
import { LayoutComponentModule } from './layout/layout.component.module';
import { RestaurantTableComponent } from './restaurant-table/restaurant-table.component';
import { RestaurantTableComponentModule } from './restaurant-table/restaurant-table.component.module';
import { ReviewTableComponent } from './review-table/review-table.component';
import { ReviewTableComponentModule } from './review-table/review-table.component.module';
import { UserTableComponent } from './user-table/user-table.component';
import { UserTableComponentModule } from './user-table/user-table.component.module';

@NgModule({
  imports: [
    UserTableComponentModule,
    RestaurantTableComponentModule,
    ReviewTableComponentModule,
    LayoutComponentModule,
    MatNativeDateModule,
    RouterModule.forChild([
      {
        path:      '',
        component: LayoutComponent,
        children:  [
          {
            path:      'user',
            component: UserTableComponent,
          },
          {
            path:      'restaurant',
            component: RestaurantTableComponent,
          },
          {
            path:      'review',
            component: ReviewTableComponent,
          },
          {
            path:       '**',
            redirectTo: 'restaurant',
          },
        ],
      },
      {
        path:       '',
        redirectTo: '',
      },
    ]),
  ],
})
export class FeatureAdminModule {
}
