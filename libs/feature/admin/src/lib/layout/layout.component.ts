import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector:        'review-restaurants-layout',
  templateUrl:     './layout.component.html',
  styleUrls:       ['./layout.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  host:            { class: 'review-restaurants-layout' },
})
export class LayoutComponent {
}
