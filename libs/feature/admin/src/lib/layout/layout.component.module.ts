import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatSidenavModule } from '@angular/material/sidenav';
import { RouterModule } from '@angular/router';
import { NavigationComponentModule } from '@rxap/layout';
import { LayoutComponent } from './layout.component';


@NgModule({
  declarations: [
    LayoutComponent,
  ],
  imports:      [
    MatSidenavModule,
    FlexLayoutModule,
    RouterModule,
    NavigationComponentModule,
  ],
  exports:      [
    LayoutComponent,
  ],
})
export class LayoutComponentModule {
}
