import { TableRowMetadata } from '@rxap/material-table-system';

export interface IReviewTableRestaurant
  extends Record<string, unknown>,
    TableRowMetadata {
  name: any;
}

export interface IReviewTableUser
  extends Record<string, unknown>,
    TableRowMetadata {
  username: any;
}

export interface IReviewTable
  extends Record<string, unknown>,
    TableRowMetadata {
  _id: any;
  rating: any;
  dateOfVisit: string | number | Date;
  comment: any;
  restaurant: IReviewTableRestaurant;
  user: IReviewTableUser;
}
