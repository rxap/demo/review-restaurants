import { Inject, Injectable } from '@angular/core';
import {
  ReviewControllerUpdateRemoteMethod,
} from '@review-restaurants/open-api/remote-methods/review-controller-update.remote-method';
import { RXAP_FORM_INITIAL_STATE } from '@rxap/forms';
import { Method } from '@rxap/utilities/rxjs';
import { IReviewTable } from '../../review-table/review-table';
import { IReviewEditForm } from '../review-edit-form/review-edit.form';

@Injectable()
export class EditReviewMethod implements Method<any, IReviewEditForm> {
  constructor(
    private readonly reviewControllerUpdate: ReviewControllerUpdateRemoteMethod,
    @Inject(RXAP_FORM_INITIAL_STATE)
    private readonly context: IReviewTable,
  ) {
  }

  public async call(parameters: IReviewEditForm) {
    await this.reviewControllerUpdate.call({
      parameters:  {
        reviewId:     this.context._id,
        restaurantId: this.context.restaurant['_id'] as string,
      },
      requestBody: {
        rating:      parameters.rating,
        comment:     parameters.comment,
        dateOfVisit: parameters.dateOfVisit.toISOString(),
      },
    });
  }
}
