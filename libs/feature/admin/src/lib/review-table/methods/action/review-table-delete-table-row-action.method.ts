import { Injectable } from '@angular/core';
import {
  ReviewControllerDeleteRemoteMethod,
} from '@review-restaurants/open-api/remote-methods/review-controller-delete.remote-method';
import { TableActionMethod, TableRowActionTypeMethod } from '@rxap/material-table-system';
import { Method } from '@rxap/utilities/rxjs';
import { IReviewTable } from '../../review-table';

@TableActionMethod('delete')
@Injectable()
export class ReviewTableDeleteTableRowActionMethod
  implements Method<any, IReviewTable>, TableRowActionTypeMethod<IReviewTable> {
  constructor(
    private readonly reviewControllerDelete: ReviewControllerDeleteRemoteMethod,
  ) {
  }

  public async call(parameters: IReviewTable) {
    console.log(parameters);
    await this.reviewControllerDelete.call({
      parameters: {
        restaurantId: parameters.restaurant['_id'] as string,
        reviewId:     parameters._id,
      },
    });
  }
}
