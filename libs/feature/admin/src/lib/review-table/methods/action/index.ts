import { RXAP_TABLE_ROW_ACTION_METHOD } from '@rxap/material-table-system';
import { ReviewTableDeleteTableRowActionMethod } from './review-table-delete-table-row-action.method';
import { ReviewTableEditTableRowActionMethod } from './review-table-edit-table-row-action.method';

export const TABLE_ROW_ACTION_METHODS = [
  {
    provide:  RXAP_TABLE_ROW_ACTION_METHOD,
    useClass: ReviewTableEditTableRowActionMethod,
    multi:    true,
  },
  {
    provide:  RXAP_TABLE_ROW_ACTION_METHOD,
    useClass: ReviewTableDeleteTableRowActionMethod,
    multi:    true,
  },
];
