import { Inject, Injectable, INJECTOR, Injector } from '@angular/core';
import { TableActionMethod, TableRowActionTypeMethod } from '@rxap/material-table-system';
import { Method } from '@rxap/utilities/rxjs';
import { OpenReviewEditFormWindowMethod } from '../../review-edit-form/open-review-edit-form-window.method';
import { IReviewTable } from '../../review-table';

@TableActionMethod('edit')
@Injectable()
export class ReviewTableEditTableRowActionMethod
  implements Method<any, IReviewTable>, TableRowActionTypeMethod<IReviewTable> {
  constructor(
    @Inject(OpenReviewEditFormWindowMethod)
    public readonly openFormWindow: OpenReviewEditFormWindowMethod,
    @Inject(INJECTOR) private readonly injector: Injector,
  ) {
  }

  public call(parameters: IReviewTable): any {
    console.log(`action row type: edit`, parameters);
    return this.openFormWindow
      .call({
        ...parameters,
        dateOfVisit: new Date(parameters.dateOfVisit),
      }, { injector: this.injector })
      .toPromise();
  }
}
