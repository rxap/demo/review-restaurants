import { Injectable } from '@angular/core';
import { ReviewDto } from '@review-restaurants/open-api/components/review-dto';
import {
  ReviewControllerGetAllRemoteMethod,
} from '@review-restaurants/open-api/remote-methods/review-controller-get-all.remote-method';
import { TableEvent } from '@rxap/data-source/table';
import { Method } from '@rxap/utilities/rxjs';

@Injectable()
export class ReviewTableMethod implements Method<any, any> {
  constructor(
    private readonly reviewControllerGetAll: ReviewControllerGetAllRemoteMethod,
  ) {
  }

  public async call({
    page,
    setTotalLength,
  }: TableEvent): Promise<ReviewDto[]> {
    const reviewList = await this.reviewControllerGetAll.call({
      parameters: {},
    });
    const pageIndex  = page?.pageIndex ?? 0;
    const pageSize   = page?.pageSize ?? Number.MAX_SAFE_INTEGER;
    if (setTotalLength) {
      setTotalLength(reviewList.length);
    }
    return reviewList.slice(pageIndex * pageSize, (pageIndex + 1) * pageSize);
  }
}
