import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCardModule } from '@angular/material/card';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatTableModule } from '@angular/material/table';
import { ConfirmComponentModule } from '@rxap/components';
import { CardProgressBarModule } from '@rxap/material-directives/card';
import {
  DateCellComponentModule,
  PersistentPaginatorDirectiveModule,
  TableColumnMenuComponentModule,
  TableDataSourceModule,
  TableRowActionsModule,
} from '@rxap/material-table-system';
import { RatingCellComponentModule } from './rating-cell/rating-cell.component.module';
import { ReviewEditFormComponentModule } from './review-edit-form/review-edit-form.component.module';
import { ReviewTableComponent } from './review-table.component';

@NgModule({
  declarations: [ReviewTableComponent],
  imports:      [
    MatCardModule,
    MatProgressBarModule,
    CardProgressBarModule,
    MatTableModule,
    FlexLayoutModule,
    TableDataSourceModule,
    TableColumnMenuComponentModule,
    MatPaginatorModule,
    PersistentPaginatorDirectiveModule,
    ReviewEditFormComponentModule,
    ConfirmComponentModule,
    TableRowActionsModule,
    RatingCellComponentModule,
    CommonModule,
    DateCellComponentModule,
  ],
  exports:      [ReviewTableComponent],
})
export class ReviewTableComponentModule {
}
