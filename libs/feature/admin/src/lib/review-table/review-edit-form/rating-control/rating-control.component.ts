import { ChangeDetectionStrategy, Component, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { ControlValueAccessor } from '@rxap/forms';

@Component({
  selector:        'rxap-rating-control',
  templateUrl:     './rating-control.component.html',
  styleUrls:       ['./rating-control.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  host:            {
    class: 'rxap-rating-control',
  },
  providers:       [
    {
      provide:     NG_VALUE_ACCESSOR,
      multi:       true,
      useExisting: forwardRef(() => RatingControlComponent),
    },
  ],
})
export class RatingControlComponent extends ControlValueAccessor {
  public value: any | null = null;

  public writeValue(value: any | null): void {
    this.value = value;
  }

  public updateValue(value: number) {
    if (this.onChange) {
      this.onChange(value);
    }
  }

}
