import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RatingComponentModule } from '@review-restaurants/shared';
import { RatingControlComponent } from './rating-control.component';

@NgModule({
  declarations: [RatingControlComponent],
  imports:      [
    RatingComponentModule,
    FormsModule,
  ],
  exports:      [RatingControlComponent],
})
export class RatingControlComponentModule {
}
