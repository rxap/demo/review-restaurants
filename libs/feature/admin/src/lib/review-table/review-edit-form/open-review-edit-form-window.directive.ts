import { Directive, Inject, INJECTOR, Injector, NgModule } from '@angular/core';
import { OpenFormWindowMethodDirective } from '@rxap/form-window-system';
import { OpenReviewEditFormWindowMethod } from './open-review-edit-form-window.method';
import { ReviewEditFormComponentModule } from './review-edit-form.component.module';
import { IReviewEditForm } from './review-edit.form';

@Directive({
  selector: '[mfdOpenReviewEditFormWindow]',
})
export class OpenReviewEditFormWindowMethodDirective extends OpenFormWindowMethodDirective<IReviewEditForm,
  Partial<IReviewEditForm>> {
  constructor(
    public readonly method: OpenReviewEditFormWindowMethod,
    @Inject(INJECTOR) injector: Injector,
  ) {
    super(injector);
  }
}

@NgModule({
  imports:      [ReviewEditFormComponentModule],
  exports:      [OpenReviewEditFormWindowMethodDirective],
  declarations: [OpenReviewEditFormWindowMethodDirective],
  providers:    [OpenReviewEditFormWindowMethod],
})
export class OpenReviewEditFormWindowMethodDirectiveModule {
}
