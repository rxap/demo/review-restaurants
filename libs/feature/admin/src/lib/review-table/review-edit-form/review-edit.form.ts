import { Injectable } from '@angular/core';
import { Validators } from '@angular/forms';
import { FormType, RxapForm, RxapFormControl, RxapFormGroup, RxapValidators, UseFormControl } from '@rxap/forms';

export interface IReviewEditForm {
  comment: string;
  rating: any;
  dateOfVisit: Date;
}

@Injectable()
@RxapForm('review-edit')
export class ReviewEditForm implements FormType<IReviewEditForm> {
  public rxapFormGroup!: RxapFormGroup<IReviewEditForm>;
  @UseFormControl({
    validators: [Validators.required, RxapValidators.IsString()],
  })
  public comment!: RxapFormControl<string>;
  @UseFormControl({
    validators: [Validators.required],
  })
  public rating!: RxapFormControl<any>;
  @UseFormControl({
    validators: [Validators.required],
  })
  public dateOfVisit!: RxapFormControl<Date>;

  [key: string]: any;
}
