import { ChangeDetectionStrategy, Component } from '@angular/core';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { RXAP_FORM_SUBMIT_METHOD } from '@rxap/forms';
import { RXAP_WINDOW_SETTINGS } from '@rxap/window-system';
import { EditReviewMethod } from '../methods/edit-review.method';
import { FormComponentProviders, FormProviders } from './form.providers';

@Component({
  selector:        'review-restaurants-review-edit-form',
  templateUrl:     './review-edit-form.component.html',
  styleUrls:       ['./review-edit-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  host:            { class: 'review-restaurants-review-edit-form' },
  providers:       [
    FormProviders,
    FormComponentProviders,
    {
      provide:  MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'fill',
      },
    },
    {
      provide:  RXAP_WINDOW_SETTINGS,
      useValue: {
        title: $localize`Edit Review`,
      },
    },
    {
      provide:  RXAP_FORM_SUBMIT_METHOD,
      useClass: EditReviewMethod,
    },
  ],
})
export class ReviewEditFormComponent {
}

export function WindowOptionsFactory() {
  return {
    title: $localize`Edit Review`,
  };
}
