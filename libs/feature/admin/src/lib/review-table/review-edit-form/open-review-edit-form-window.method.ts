import { Inject, Injectable, INJECTOR, Injector, Optional } from '@angular/core';
import {
  FormWindowOptions,
  FormWindowService,
  OpenFormWindowMethod,
  RXAP_FORM_WINDOW_SYSTEM_OPEN_FORM_DEFAULT_OPTIONS,
} from '@rxap/form-window-system';
import { ReviewEditFormComponent } from './review-edit-form.component';
import { IReviewEditForm, ReviewEditForm } from './review-edit.form';

@Injectable()
export class OpenReviewEditFormWindowMethod extends OpenFormWindowMethod<IReviewEditForm> {
  constructor(
    @Inject(FormWindowService) formWindowService: FormWindowService,
    @Inject(INJECTOR) injector: Injector,
    @Optional()
    @Inject(RXAP_FORM_WINDOW_SYSTEM_OPEN_FORM_DEFAULT_OPTIONS)
      defaultOptions: FormWindowOptions<IReviewEditForm> | null,
  ) {
    super(
      formWindowService,
      ReviewEditForm,
      injector,
      ReviewEditFormComponent,
      defaultOptions,
    );
  }
}
