import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormWindowFooterDirectiveModule } from '@rxap/form-window-system';
import { RxapFormsModule } from '@rxap/forms';
import {
  ControlErrorDirectiveModule,
  FormControlsComponentModule,
  RequiredDirectiveModule,
} from '@rxap/material-form-system';
import { OpenReviewEditFormWindowMethod } from './open-review-edit-form-window.method';
import { RatingControlComponentModule } from './rating-control/rating-control.component.module';
import { ReviewEditFormComponent } from './review-edit-form.component';

@NgModule({
  declarations: [ReviewEditFormComponent],
  imports:      [
    ReactiveFormsModule,
    FlexLayoutModule,
    RatingControlComponentModule,
    MatFormFieldModule,
    ControlErrorDirectiveModule,
    MatInputModule,
    RequiredDirectiveModule,
    FormWindowFooterDirectiveModule,
    FormControlsComponentModule,
    RxapFormsModule,
    MatDatepickerModule,
  ],
  exports:      [ReviewEditFormComponent],
  providers:    [OpenReviewEditFormWindowMethod],
})
export class ReviewEditFormComponentModule {
}
