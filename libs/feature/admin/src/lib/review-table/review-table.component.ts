import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { RowAnimation, RXAP_TABLE_METHOD } from '@rxap/material-table-system';
import { Observable } from 'rxjs';
import { TABLE_ROW_ACTION_METHODS } from './methods/action/index';
import { ReviewTableMethod } from './methods/review-table.method';
import { FormProviders as EditRowActionFormProviders } from './review-edit-form/form.providers';

@Component({
  selector:        'review-restaurants-review-table',
  templateUrl:     './review-table.component.html',
  styleUrls:       ['./review-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  host:            { class: 'review-restaurants-review-table' },
  animations:      [RowAnimation],
  providers:       [
    EditRowActionFormProviders,
    TABLE_ROW_ACTION_METHODS,
    {
      provide:  RXAP_TABLE_METHOD,
      useClass: ReviewTableMethod,
    },
  ],
})
export class ReviewTableComponent {
  @Input()
  public parameters?: Observable<Record<string, any>>;
}
