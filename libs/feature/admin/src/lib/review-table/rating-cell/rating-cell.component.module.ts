import { NgModule } from '@angular/core';
import { RatingComponentModule } from '@review-restaurants/shared';
import { RatingCellComponent } from './rating-cell.component';

@NgModule({
  declarations: [RatingCellComponent],
  imports:      [RatingComponentModule],
  exports:      [RatingCellComponent],
})
export class RatingCellComponentModule {
}
