import { NgModule } from '@angular/core';
import { MatNativeDateModule } from '@angular/material/core';
import { RouterModule } from '@angular/router';
import { RestaurantListComponent } from './restaurant-list/restaurant-list.component';
import { RestaurantListComponentModule } from './restaurant-list/restaurant-list.component.module';

@NgModule({
  imports: [
    RestaurantListComponentModule,
    MatNativeDateModule,
    RouterModule.forChild([
      {
        path:      '',
        component: RestaurantListComponent,
      },
    ]),
  ],
})
export class FeatureRestaurantModule {
}
