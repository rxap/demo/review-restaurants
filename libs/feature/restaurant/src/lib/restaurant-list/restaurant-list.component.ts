import { ChangeDetectionStrategy, Component } from '@angular/core';
import {
  RestaurantControllerGetAllDataSource,
} from '@review-restaurants/open-api/data-sources/restaurant-controller-get-all.data-source';

@Component({
  selector:        'rr-restaurant-list',
  templateUrl:     './restaurant-list.component.html',
  styleUrls:       ['./restaurant-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  host:            { class: 'rr-restaurant-list' },
})
export class RestaurantListComponent {

  constructor(public readonly restaurantControllerGetAll: RestaurantControllerGetAllDataSource) {
  }

}
