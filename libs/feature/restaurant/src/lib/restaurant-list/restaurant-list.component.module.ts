import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { DataSourceCollectionDirectiveModule } from '@rxap/data-source/directive';
import { RestaurantListItemComponentModule } from './restaurant-list-item/restaurant-list-item.component.module';
import { RestaurantListComponent } from './restaurant-list.component';


@NgModule({
  declarations: [
    RestaurantListComponent,
  ],
  imports:      [
    MatListModule,
    DataSourceCollectionDirectiveModule,
    CommonModule,
    RestaurantListItemComponentModule,
    FlexLayoutModule,
    MatCardModule,
    MatButtonModule,
  ],
  exports:      [
    RestaurantListComponent,
  ],
})
export class RestaurantListComponentModule {
}
