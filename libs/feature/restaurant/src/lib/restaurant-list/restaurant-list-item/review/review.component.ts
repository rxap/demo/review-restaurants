import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { RestaurantDto } from '@review-restaurants/open-api/components/restaurant-dto';
import {
  ReviewControllerCreateRemoteMethod,
} from '@review-restaurants/open-api/remote-methods/review-controller-create.remote-method';

@Component({
  selector:    'rr-review',
  templateUrl: './review.component.html',
  styleUrls:   ['./review.component.scss'],
  host:        { class: 'rr-review' },
})
export class ReviewComponent {

  public rating      = 0;
  public comment     = '';
  public dateOfVisit = new Date();

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public readonly restaurant: RestaurantDto,
    private readonly dialogRef: MatDialogRef<ReviewComponent>,
    private readonly reviewControllerCreate: ReviewControllerCreateRemoteMethod,
  ) {
  }

  public post() {
    const response = this.reviewControllerCreate.call({
      parameters:  {
        restaurantId: this.restaurant._id,
      },
      requestBody: {
        rating:      this.rating,
        comment:     this.comment,
        dateOfVisit: this.dateOfVisit.toISOString(),
      },
    });

    // pass the promise to the dialog ref closed callback
    // used to trigger a RestaurantListDataSource refresh after the
    // create request is completed
    this.dialogRef.close(response);
  }
}
