import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RestaurantDto } from '@review-restaurants/open-api/components/restaurant-dto';
import {
  ReviewControllerCreateRemoteMethod,
} from '@review-restaurants/open-api/remote-methods/review-controller-create.remote-method';
import { RatingComponentModule } from '@review-restaurants/shared';
import { ReviewComponent } from './review.component';
import Mock = jest.Mock;

describe('ReviewComponent', () => {
  let component: ReviewComponent;
  let fixture: ComponentFixture<ReviewComponent>;

  let reviewControllerCreate: Record<keyof ReviewControllerCreateRemoteMethod, Mock>;
  let dialogRef: Record<keyof MatDialogRef<any>, Mock>;
  let dialogData: RestaurantDto;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ReviewComponent],
      imports:      [
        FormsModule,
        MatDatepickerModule,
        MatInputModule,
        MatNativeDateModule,
        RatingComponentModule,
        NoopAnimationsModule,
      ],
      providers:    [
        {
          provide:  ReviewControllerCreateRemoteMethod,
          useValue: { call: jest.fn() },
        },
        {
          provide:  MAT_DIALOG_DATA,
          useValue: { _id: 'restaurant' },
        },
        {
          provide:  MatDialogRef,
          useValue: { close: jest.fn() },
        },
      ],
      schemas:      [CUSTOM_ELEMENTS_SCHEMA],
    })
      .compileComponents();

    reviewControllerCreate = TestBed.inject(ReviewControllerCreateRemoteMethod) as any;
    dialogRef              = TestBed.inject(MatDialogRef) as any;
    dialogData             = TestBed.inject(MAT_DIALOG_DATA) as any;
  });

  beforeEach(() => {
    fixture   = TestBed.createComponent(ReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create a new review if the user clicks on the post button', () => {

    const promise = Promise.resolve();

    reviewControllerCreate.call.mockReturnValue(promise);

    component.rating      = 4;
    component.comment     = 'comment';
    component.dateOfVisit = new Date();

    component.post();

    expect(reviewControllerCreate.call).toBeCalledTimes(1);
    expect(reviewControllerCreate.call).toBeCalledWith({
      parameters:  {
        restaurantId: dialogData._id,
      },
      requestBody: {
        rating:      4,
        comment:     'comment',
        dateOfVisit: component.dateOfVisit.toISOString(),
      },
    });
    expect(dialogRef.close).toBeCalledTimes(1);
    expect(dialogRef.close).toBeCalledWith(promise);

  });

  it('should only enable the post button if a review is set', async () => {

    const postButton = fixture.debugElement.query(By.css('button[color="primary"]'));

    expect(postButton).toBeDefined();
    expect(postButton.nativeElement.disabled).toBeTruthy();

    const postSpy = jest.spyOn(component, 'post');

    component.rating = 2;

    fixture.detectChanges();

    expect(postButton.nativeElement.disabled).toBeFalsy();

    postButton.nativeElement.click();

    expect(postSpy).toBeCalledTimes(1);

  });

});
