import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatTooltipModule } from '@angular/material/tooltip';
import { RatingComponentModule } from '@review-restaurants/shared';
import { ReviewComponent } from './review.component';


@NgModule({
  declarations: [
    ReviewComponent,
  ],
  imports:      [
    MatDialogModule,
    MatButtonModule,
    RatingComponentModule,
    FormsModule,
    MatInputModule,
    FlexLayoutModule,
    MatTooltipModule,
    MatDatepickerModule,
  ],
  exports:      [
    ReviewComponent,
  ],
})
export class ReviewComponentModule {
}
