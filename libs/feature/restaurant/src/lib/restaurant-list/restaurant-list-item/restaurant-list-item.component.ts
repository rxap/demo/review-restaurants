import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { RestaurantDto } from '@review-restaurants/open-api/components/restaurant-dto';
import {
  RestaurantControllerGetAllDataSource,
} from '@review-restaurants/open-api/data-sources/restaurant-controller-get-all.data-source';
import { Required } from '@rxap/utilities';
import { take, tap } from 'rxjs';
import { DetailsComponent } from './details/details.component';
import { ReviewComponent } from './review/review.component';

@Component({
  selector:        'rr-restaurant-list-item',
  templateUrl:     './restaurant-list-item.component.html',
  styleUrls:       ['./restaurant-list-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  host:            { class: 'rr-restaurant-list-item' },
})
export class RestaurantListItemComponent {

  @Input()
  @Required
  public restaurant!: RestaurantDto;

  constructor(
    private readonly dialog: MatDialog,
    private readonly restaurantControllerGetAll: RestaurantControllerGetAllDataSource,
  ) {
  }

  public review() {
    const ref = this.dialog.open(ReviewComponent, {
      data: this.restaurant,
    });
    ref
      .afterClosed()
      .pipe(
        take(1),
        tap(async request => {
          // wait for the create request to complete
          await request;
          this.restaurantControllerGetAll.refresh();
        }),
      )
      .subscribe();
  }

  public details() {
    this.dialog.open(DetailsComponent, { data: this.restaurant });
  }

}
