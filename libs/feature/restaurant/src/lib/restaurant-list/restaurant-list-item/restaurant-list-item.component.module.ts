import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { RatingComponentModule } from '@review-restaurants/shared';
import { DetailsComponentModule } from './details/details.component.module';
import { RestaurantListItemComponent } from './restaurant-list-item.component';
import { ReviewComponentModule } from './review/review.component.module';


@NgModule({
  declarations: [
    RestaurantListItemComponent,
  ],
  imports:      [
    CommonModule,
    FlexModule,
    MatButtonModule,
    RatingComponentModule,
    MatDialogModule,
    ReviewComponentModule,
    DetailsComponentModule,
  ],
  exports:      [
    RestaurantListItemComponent,
  ],
})
export class RestaurantListItemComponentModule {
}
