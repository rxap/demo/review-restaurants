import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog } from '@angular/material/dialog';
import { By } from '@angular/platform-browser';
import {
  RestaurantControllerGetAllDataSource,
} from '@review-restaurants/open-api/data-sources/restaurant-controller-get-all.data-source';
import { EMPTY, of } from 'rxjs';
import { DetailsComponent } from './details/details.component';

import { RestaurantListItemComponent } from './restaurant-list-item.component';
import { ReviewComponent } from './review/review.component';
import Mock = jest.Mock;

describe('RestaurantListItemComponent', () => {
  let component: RestaurantListItemComponent;
  let fixture: ComponentFixture<RestaurantListItemComponent>;

  let matDialog: Record<keyof MatDialog, Mock>;
  let restaurantControllerGetAll: Record<keyof RestaurantControllerGetAllDataSource, Mock>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RestaurantListItemComponent],
      providers:    [
        {
          provide:  MatDialog,
          useValue: { open: jest.fn() },
        },
        {
          provide:  RestaurantControllerGetAllDataSource,
          useValue: { refresh: jest.fn() },
        },
      ],
      schemas:      [CUSTOM_ELEMENTS_SCHEMA],
    })
      .compileComponents();

    matDialog                  = TestBed.inject(MatDialog) as any;
    restaurantControllerGetAll = TestBed.inject(RestaurantControllerGetAllDataSource) as any;
  });

  beforeEach(() => {
    fixture              = TestBed.createComponent(RestaurantListItemComponent);
    component            = fixture.componentInstance;
    component.restaurant = { name: 'test', _id: 'restaurant', rating: 3.5 };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should open the ReviewComponent Dialog', () => {

    const button = fixture.debugElement.query(By.css('button[color="primary"]'));

    const afterClosedMock = jest.fn().mockReturnValue(EMPTY);

    matDialog.open.mockReturnValue({ afterClosed: afterClosedMock });

    button.nativeElement.click();

    expect(matDialog.open).toBeCalledTimes(1);
    expect(matDialog.open).toBeCalledWith(ReviewComponent, {
      data: component.restaurant,
    });
    expect(afterClosedMock).toBeCalledTimes(1);

    expect(restaurantControllerGetAll.refresh).toBeCalledTimes(0);

  });

  it('should open the ReviewComponent Dialog and refresh the data source if a review is created', async () => {

    const button = fixture.debugElement.query(By.css('button[color="primary"]'));

    const promise = Promise.resolve();

    const afterClosedMock = jest.fn().mockReturnValue(of(promise));

    matDialog.open.mockReturnValue({ afterClosed: afterClosedMock });

    button.nativeElement.click();

    expect(matDialog.open).toBeCalledTimes(1);
    expect(matDialog.open).toBeCalledWith(ReviewComponent, {
      data: component.restaurant,
    });
    expect(afterClosedMock).toBeCalledTimes(1);

    await promise;

    expect(restaurantControllerGetAll.refresh).toBeCalledTimes(1);

  });

  it('should open the DetailsComponent Dialog', () => {

    const button = fixture.debugElement.query(By.css('button[color="accent"]'));
    button.nativeElement.click();

    expect(matDialog.open).toBeCalledTimes(1);
    expect(matDialog.open).toBeCalledWith(DetailsComponent, {
      data: component.restaurant,
    });

  });

});
