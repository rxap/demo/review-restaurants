import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatInputModule } from '@angular/material/input';
import { RatingComponentModule } from '@review-restaurants/shared';
import { RemoteMethodTemplateDirectiveModule } from '@rxap/remote-method/directive';
import { DetailsComponent } from './details.component';


@NgModule({
  declarations: [
    DetailsComponent,
  ],
  imports:      [
    MatDialogModule,
    RatingComponentModule,
    FlexLayoutModule,
    RemoteMethodTemplateDirectiveModule,
    CommonModule,
    MatButtonModule,
    MatDividerModule,
    MatInputModule,
  ],
  exports:      [
    DetailsComponent,
  ],
})
export class DetailsComponentModule {
}
