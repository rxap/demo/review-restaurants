import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RestaurantDto } from '@review-restaurants/open-api/components/restaurant-dto';
import {
  ReviewControllerGetBestParameter,
} from '@review-restaurants/open-api/parameters/review-controller-get-best.parameter';
import {
  ReviewControllerGetLatestParameter,
} from '@review-restaurants/open-api/parameters/review-controller-get-latest.parameter';
import {
  ReviewControllerGetWorstParameter,
} from '@review-restaurants/open-api/parameters/review-controller-get-worst.parameter';
import {
  ReviewControllerGetBestRemoteMethod,
} from '@review-restaurants/open-api/remote-methods/review-controller-get-best.remote-method';
import {
  ReviewControllerGetLatestRemoteMethod,
} from '@review-restaurants/open-api/remote-methods/review-controller-get-latest.remote-method';
import {
  ReviewControllerGetWorstRemoteMethod,
} from '@review-restaurants/open-api/remote-methods/review-controller-get-worst.remote-method';
import { OpenApiRemoteMethodParameter } from '@rxap/open-api/remote-method';

@Component({
  selector:        'rr-details',
  templateUrl:     './details.component.html',
  styleUrls:       ['./details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  host:            { class: 'rr-details' },
})
export class DetailsComponent {

  public methodParameters: OpenApiRemoteMethodParameter<ReviewControllerGetBestParameter | ReviewControllerGetWorstParameter | ReviewControllerGetLatestParameter, void>;

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public readonly restaurant: RestaurantDto,
    public readonly reviewControllerGetBest: ReviewControllerGetBestRemoteMethod,
    public readonly reviewControllerGetWorst: ReviewControllerGetWorstRemoteMethod,
    public readonly reviewControllerGetLatest: ReviewControllerGetLatestRemoteMethod,
  ) {
    this.methodParameters = {
      parameters: {
        restaurantId: this.restaurant._id,
      },
    };
  }

}
