import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { RxapAuthenticationGuard } from '@rxap/authentication';
import {
  AuthenticationContainerComponent,
  AuthenticationContainerComponentModule,
  LoadingComponent,
  LoadingComponentModule,
} from '@rxap/authentication/components';
import { LoginComponent } from './login/login.component';
import { LoginComponentModule } from './login/login.component.module';
import { RegisterComponent } from './register/register.component';
import { RegisterComponentModule } from './register/register.component.module';

@NgModule({
  imports: [
    AuthenticationContainerComponentModule,
    LoadingComponentModule,
    LoginComponentModule,
    RegisterComponentModule,
    RouterModule.forChild([
      {
        path:      '',
        component: AuthenticationContainerComponent,
        children:  [
          {
            canActivate: [RxapAuthenticationGuard],
            path:        'login',
            component:   LoginComponent,
          },
          {
            path:      'register',
            component: RegisterComponent,
          },
          {
            path:      'loading',
            component: LoadingComponent,
          },
          {
            path:       '**',
            redirectTo: 'login',
          },
        ],
      },
      {
        path:       '**',
        redirectTo: '',
      },
    ]),
  ],
})
export class FeatureAuthenticationModule {
}
