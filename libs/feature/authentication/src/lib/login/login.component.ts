import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { ErrorStateMatcher, ShowOnDirtyErrorStateMatcher } from '@angular/material/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { RxapAuthenticationService } from '@rxap/authentication';
import { fadeAnimation } from '../fade-animation';
import { LoginFormProviders } from './login.form';

@Component({
  templateUrl:     './login.component.html',
  styleUrls:       ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  host:            { class: 'rr-login' },
  providers:       [
    LoginFormProviders,
    {
      provide:  ErrorStateMatcher,
      useClass: ShowOnDirtyErrorStateMatcher,
    },
  ],
  animations:      [
    fadeAnimation,
  ],
})
export class LoginComponent {

  constructor(
    @Inject(RxapAuthenticationService)
    private readonly authentication: RxapAuthenticationService,
    @Inject(MatSnackBar)
    private readonly snackBar: MatSnackBar,
  ) {
  }

}
