import { Inject, Injectable, INJECTOR, Injector, Provider } from '@angular/core';
import { Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RxapAuthenticationService } from '@rxap/authentication';
import { ConfigService } from '@rxap/config';
import {
  FormSubmitMethod,
  FormSubmitSuccessfulMethod,
  FormType,
  RXAP_FORM_DEFINITION,
  RXAP_FORM_DEFINITION_BUILDER,
  RXAP_FORM_SUBMIT_METHOD,
  RXAP_FORM_SUBMIT_SUCCESSFUL_METHOD,
  RxapForm,
  RxapFormBuilder,
  RxapFormControl,
  UseFormControl,
} from '@rxap/forms';
import { RxapOnInit } from '@rxap/utilities';
import { HashPassword } from '../hash-password';

export interface ILoginForm {
  username: string;
  password: string;
}

@RxapForm({
  id:        'login',
  providers: [
    {
      provide:     ConfigService,
      useExisting: ConfigService,
    },
  ],
})
@Injectable()
export class LoginForm implements FormType<ILoginForm>, RxapOnInit {

  public loading     = true;
  public loginFailed = false;

  @UseFormControl({
    validators: [Validators.required],
  })
  public username!: RxapFormControl<string>;

  @UseFormControl({
    validators: [Validators.required],
  })
  public password!: RxapFormControl;

  constructor(public readonly config: ConfigService<any>) {
  }

  public rxapOnInit() {
    const username = this.config.get<string>('authentication.default.username');
    if (username) {
      this.username.setValue(username);
    }
    const password = this.config.get<string>('authentication.default.password');
    if (password) {
      this.password.setValue(password);
    }
  }
}

@Injectable()
export class LoginFormSubmitMethod implements FormSubmitMethod<any> {
  constructor(private readonly authentication: RxapAuthenticationService) {
  }

  public async call(value: {
    username: string;
    password: string;
  }): Promise<any> {
    return this.authentication.signInWithEmailAndPassword(
      value.username,
      await HashPassword(value.password),
      false,
    );
  }
}

@Injectable()
export class LoginFormSubmitSuccessful
  implements FormSubmitSuccessfulMethod<any> {
  constructor(
    @Inject(Router)
    private readonly router: Router,
  ) {
  }

  public call(): Promise<any> {
    return this.router.navigate(['/']);
  }
}

export const LoginFormProviders: Provider[] = [
  LoginForm,
  {
    provide:    RXAP_FORM_DEFINITION_BUILDER,
    useFactory: (injector: Injector) =>
                  new RxapFormBuilder<ILoginForm>(LoginForm, injector),
    deps:       [INJECTOR],
  },
  {
    provide:    RXAP_FORM_DEFINITION,
    useFactory: (builder: RxapFormBuilder) => builder.build(),
    deps:       [RXAP_FORM_DEFINITION_BUILDER],
  },
  {
    provide:  RXAP_FORM_SUBMIT_METHOD,
    useClass: LoginFormSubmitMethod,
  },
  {
    provide:  RXAP_FORM_SUBMIT_SUCCESSFUL_METHOD,
    useClass: LoginFormSubmitSuccessful,
  },
];
