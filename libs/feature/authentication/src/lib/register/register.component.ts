import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ErrorStateMatcher, ShowOnDirtyErrorStateMatcher } from '@angular/material/core';
import { fadeAnimation } from '../fade-animation';
import { RegisterFormProviders } from './register.form';

@Component({
  templateUrl:     './register.component.html',
  styleUrls:       ['./register.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  host:            { class: 'rr-register' },
  providers:       [
    RegisterFormProviders,
    {
      provide:  ErrorStateMatcher,
      useClass: ShowOnDirtyErrorStateMatcher,
    },
  ],
  animations:      [
    fadeAnimation,
  ],
})
export class RegisterComponent {
}
