import { Inject, Injectable, INJECTOR, Injector, Provider } from '@angular/core';
import { Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {
  AuthControllerRegisterRemoteMethod,
} from '@review-restaurants/open-api/remote-methods/auth-controller-register.remote-method';
import { ConfigService } from '@rxap/config';
import {
  FormSubmitMethod,
  FormSubmitSuccessfulMethod,
  FormType,
  RXAP_FORM_DEFINITION,
  RXAP_FORM_DEFINITION_BUILDER,
  RXAP_FORM_SUBMIT_METHOD,
  RXAP_FORM_SUBMIT_SUCCESSFUL_METHOD,
  RxapForm,
  RxapFormBuilder,
  RxapFormControl,
  UseFormControl,
} from '@rxap/forms';
import { RxapOnInit } from '@rxap/utilities';
import { HashPassword } from '../hash-password';

export interface IRegisterForm {
  username: string;
  password: string;
  role: string;
}

@RxapForm({
  id:        'register',
  providers: [
    {
      provide:     ConfigService,
      useExisting: ConfigService,
    },
  ],
})
@Injectable()
export class RegisterForm implements FormType<IRegisterForm>, RxapOnInit {

  public loading        = true;
  public registerFailed = false;

  @UseFormControl({
    validators: [Validators.required],
  })
  public username!: RxapFormControl<string>;

  @UseFormControl({
    validators: [Validators.required],
  })
  public password!: RxapFormControl;

  @UseFormControl({
    validators: [Validators.required],
    state:      'member',
  })
  public role!: RxapFormControl;

  constructor(public readonly config: ConfigService<any>) {
  }

  public rxapOnInit() {
    const username = this.config.get<string>('authentication.default.username');
    if (username) {
      this.username.setValue(username);
    }
    const password = this.config.get<string>('authentication.default.password');
    if (password) {
      this.password.setValue(password);
    }
  }
}

@Injectable()
export class RegisterFormSubmitMethod implements FormSubmitMethod<any> {
  constructor(private readonly authControllerRegister: AuthControllerRegisterRemoteMethod) {
  }

  public async call(value: {
    username: string;
    password: string;
    role: 'member' | 'admin',
  }): Promise<any> {
    try {
      await this.authControllerRegister.call({
        requestBody: {
          username: value.username,
          password: await HashPassword(value.password),
          role:     value.role,
        },
      });
    } catch (e) {
      throw new Error(e.error?.message ?? e.message);
    }
  }
}

@Injectable()
export class RegisterFormSubmitSuccessful
  implements FormSubmitSuccessfulMethod<any> {
  constructor(
    @Inject(Router)
    private readonly router: Router,
  ) {
  }

  public call(): Promise<any> {
    return this.router.navigate(['/']);
  }
}

export const RegisterFormProviders: Provider[] = [
  RegisterForm,
  {
    provide:    RXAP_FORM_DEFINITION_BUILDER,
    useFactory: (injector: Injector) =>
                  new RxapFormBuilder<IRegisterForm>(RegisterForm, injector),
    deps:       [INJECTOR],
  },
  {
    provide:    RXAP_FORM_DEFINITION,
    useFactory: (builder: RxapFormBuilder) => builder.build(),
    deps:       [RXAP_FORM_DEFINITION_BUILDER],
  },
  {
    provide:  RXAP_FORM_SUBMIT_METHOD,
    useClass: RegisterFormSubmitMethod,
  },
  {
    provide:  RXAP_FORM_SUBMIT_SUCCESSFUL_METHOD,
    useClass: RegisterFormSubmitSuccessful,
  },
];
