Review Restaurants
===

# Development

Node Version: `v14.17.6`

Yarn Version: `1.12.11`

Docker Version: `20.10.12`

Docker Compose Version: `v2.2.2`

---

##### Unit Testing

1. run all available tests: `nx test`

##### E2E Testing

1. ensure the database and api are operable: [http://localhost:3000/health](http://localhost:3000/health)
2. run all available tests: `nx e2e`

## API

1. copy the `default.env` to `.env`: `cp default.env .env`
2. start the mongodb server: `docker-compose up -d`
3. start the api: `nx run api:serve`
4. (optional) open the open api configuration: [http://localhost:3000/api](http://localhost:3000/api)
5. (optional) open the mongodb explorer: [http://localhost:8081](http://localhost:8081)
6. (optional) check the api health status: [http://localhost:3000/health](http://localhost:3000/health)

##### Unit Testing

1. start the api unit test: `nx run api:test`

## PWA

1. start the pwa: `nx run pwa:serve`
2. open the pwa: [http://localhost:4200](http://localhost:4200)

##### E2E Testing

1. start the cypress e2e test in watch mode: `nx run pwa-e2e:e2e --watch`
2. select an integration from the cypress test window

##### Unit Testing

1. run the pwa unit test: `nx run pwa:test`
2. run the feature-admin unit test: `nx run feature-admin:test`
3. run the feature-authentication unit test: `nx run feature-authentication:test`
4. run the feature-restaurant unit test: `nx run feature-restaurant:test`
5. run the shared unit test: `nx run shared:test`
