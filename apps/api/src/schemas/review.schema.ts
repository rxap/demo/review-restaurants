import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { Document } from 'mongoose';
import { Restaurant } from './restaurant.schema';
import { User } from './user.schema';

export type ReviewDocument = Review & Document;

@Schema()
export class Review {

  @Prop({ type: mongoose.Schema.Types.Number })
  rating: number;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User' })
  user: User;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Restaurant' })
  restaurant: Restaurant;

  @Prop({ type: mongoose.Schema.Types.String })
  date: string;

  @Prop({ type: mongoose.Schema.Types.String })
  comment: string;

  @Prop({ type: mongoose.Schema.Types.String })
  dateOfVisit: string;

  _id: string;

}

export const ReviewSchema = SchemaFactory.createForClass(Review);
