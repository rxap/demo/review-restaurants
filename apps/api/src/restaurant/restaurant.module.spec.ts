import { INestApplication, ValidationPipe } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { Test } from '@nestjs/testing';
import * as request from 'supertest';
import { AuthModule } from '../auth/auth.module';
import { AuthService } from '../auth/auth.service';
import { RestaurantService } from '../crud/restaurant.service';
import { UserService } from '../crud/user.service';
import { UserRole } from '../dto/user.dto';
import { closeInMongodConnection, rootMongooseTestModule } from '../mongoose-test.module';
import { Restaurant } from '../schemas/restaurant.schema';
import { User } from '../schemas/user.schema';
import { transformOptions } from '../transform-options';
import { validatorOptions } from '../validator-options';
import { RestaurantModule } from './restaurant.module';

describe('RestaurantModule', () => {

  let app: INestApplication;
  let authService: AuthService;
  let restaurantService: RestaurantService;
  let adminUser: User;
  let memberUser: User;

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [
        rootMongooseTestModule(),
        AuthModule,
        ConfigModule.forRoot({
          isGlobal: true,
          load:     [() => ({ jwt: { secret: 'secret' } })],
        }),
        RestaurantModule,
      ],
    })
      .compile();

    app = moduleRef.createNestApplication();

    app.useGlobalPipes(
      new ValidationPipe({
        ...validatorOptions,
        transformOptions,
        transform: true,
      }),
    );

    await app.init();

    const userService = app.get(UserService);
    restaurantService = app.get(RestaurantService);
    authService       = app.get(AuthService);

    memberUser = await userService.create({ username: 'member', password: 'e2e', role: UserRole.MEMBER });
    adminUser  = await userService.create({ username: 'admin', password: 'e2e', role: UserRole.ADMIN });

  });

  afterEach(async () => {
    await restaurantService.clear();
  });

  afterAll(async () => {
    await app.close();
    await closeInMongodConnection();
    jest.resetAllMocks();
  });

  describe('POST /restaurant', () => {

    it('should response with 401 if the authentication bearer header is not defined', async () => {

      await request(app.getHttpServer())
        .post(`/restaurant`)
        .expect(401);

    });

    it('should response with 403 if the restaurant has not the admin role', async () => {

      const { accessToken } = await authService.login({
        username: 'member',
        _id:      memberUser._id,
        role:     UserRole.MEMBER,
      });

      await request(app.getHttpServer())
        .post(`/restaurant`)
        .set('Authorization', 'Bearer ' + accessToken)
        .expect(403);

    });

    it('should response with 201', async () => {

      const { accessToken } = await authService.login({ username: 'member', _id: adminUser._id, role: UserRole.ADMIN });

      await request(app.getHttpServer())
        .post(`/restaurant`)
        .set('Authorization', 'Bearer ' + accessToken)
        .send({
          name: 'name',
        }).expect(201);

    });

    it('should response with 409 if restaurant with the same name already exists', async () => {

      const { accessToken } = await authService.login({ username: 'member', _id: adminUser._id, role: UserRole.ADMIN });

      await restaurantService.create({ name: 'name' });

      await request(app.getHttpServer())
        .post(`/restaurant`)
        .set('Authorization', 'Bearer ' + accessToken)
        .send({
          name: 'name',
        }).expect(409);

    });

  });

  describe('GET /restaurant', () => {

    it('should response with 401 if the authentication bearer header is not defined', async () => {

      await request(app.getHttpServer())
        .get(`/restaurant`)
        .expect(401);

    });

    it('should response with 403 if the restaurant has not the admin role', async () => {

      const { accessToken } = await authService.login({ username: 'member', _id: adminUser._id, role: UserRole.ADMIN });

      await restaurantService.create({ name: 'name1' });
      await restaurantService.create({ name: 'name2' });
      await restaurantService.create({ name: 'name3' });

      const { statusCode, body } = await request(app.getHttpServer())
        .get(`/restaurant`)
        .set('Authorization', 'Bearer ' + accessToken);

      expect(statusCode).toEqual(200);
      expect(body).toHaveLength(3);

    });


  });

  describe('PUT /restaurant/:restaurantId', () => {

    let restaurant: Restaurant;

    beforeEach(async () => {

      restaurant = await restaurantService.create({ name: 'name' });

    });

    it('should response with 401 if the authentication bearer header is not defined', async () => {

      await request(app.getHttpServer())
        .put(`/restaurant/${restaurant._id}`)
        .expect(401);

    });

    it('should response with 403 if the restaurant has not the admin role', async () => {

      const { accessToken } = await authService.login({
        username: 'member',
        _id:      adminUser._id,
        role:     UserRole.MEMBER,
      });

      await request(app.getHttpServer())
        .put(`/restaurant/${restaurant._id}`)
        .set('Authorization', 'Bearer ' + accessToken)
        .expect(403);

    });

    it('should response with 200', async () => {

      const { accessToken } = await authService.login({ username: 'member', _id: adminUser._id, role: UserRole.ADMIN });

      await request(app.getHttpServer())
        .put(`/restaurant/${restaurant._id}`)
        .set('Authorization', 'Bearer ' + accessToken)
        .send({
          name: 'name-edit',
        })
        .expect(200);

      const updatedRestaurant = await restaurantService.findOne(restaurant._id);

      expect(updatedRestaurant).toHaveProperty('name', 'name-edit');

    });

  });

  describe('GET /restaurant/:restaurantId', () => {

    let restaurant: Restaurant;

    beforeEach(async () => {

      restaurant = await restaurantService.create({ name: 'name' });

    });

    it('should response with 401 if the authentication bearer header is not defined', async () => {

      await request(app.getHttpServer())
        .get(`/restaurant/${restaurant._id}`)
        .expect(401);

    });

    it('should response with 200', async () => {

      const { accessToken } = await authService.login({
        username: 'member',
        _id:      adminUser._id,
        role:     UserRole.MEMBER,
      });

      const { body, statusCode } = await request(app.getHttpServer())
        .get(`/restaurant/${restaurant._id}`)
        .set('Authorization', 'Bearer ' + accessToken);

      expect(statusCode).toEqual(200);
      expect(body).toEqual({
        name:   'name',
        _id:    restaurant._id.toString(),
        rating: 0,
      });

    });

  });

  describe('DELETE /restaurant/:restaurantId', () => {

    let restaurant: Restaurant;

    beforeEach(async () => {

      restaurant = await restaurantService.create({ name: 'name' });

    });

    it('should response with 401 if the authentication bearer header is not defined', async () => {

      await request(app.getHttpServer())
        .delete(`/restaurant/${restaurant._id}`)
        .expect(401);

    });

    it('should response with 403 if the restaurant has not the admin role', async () => {

      const { accessToken } = await authService.login({
        username: 'member',
        _id:      adminUser._id,
        role:     UserRole.MEMBER,
      });

      await request(app.getHttpServer())
        .delete(`/restaurant/${restaurant._id}`)
        .set('Authorization', 'Bearer ' + accessToken)
        .expect(403);

    });

    it('should response with 200', async () => {

      const { accessToken } = await authService.login({ username: 'member', _id: adminUser._id, role: UserRole.ADMIN });

      await request(app.getHttpServer())
        .delete(`/restaurant/${restaurant._id}`)
        .set('Authorization', 'Bearer ' + accessToken)
        .expect(200);

      await expect(restaurantService.findOne(restaurant._id)).resolves.toBe(null);

    });

  });

});
