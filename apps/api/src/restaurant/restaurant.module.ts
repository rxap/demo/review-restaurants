import { Logger, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { RestaurantService } from '../crud/restaurant.service';
import { Restaurant, RestaurantSchema } from '../schemas/restaurant.schema';
import { RestaurantController } from './restaurant.controller';

@Module({
  imports:     [
    MongooseModule.forFeature([
      { name: Restaurant.name, schema: RestaurantSchema },
    ]),
  ],
  controllers: [RestaurantController],
  providers:   [Logger, RestaurantService],
})
export class RestaurantModule {
}
