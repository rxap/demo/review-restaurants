import { Logger, NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { TestingLogger } from '@nestjs/testing/services/testing-logger.service';
import { validateSync } from 'class-validator';
import { RestaurantService } from '../crud/restaurant.service';
import { RestaurantDto } from '../dto/restaurant.dto';
import { RestaurantController } from './restaurant.controller';
import Mock = jest.Mock;

describe('RestaurantController', () => {
  let controller: RestaurantController;
  let restaurantService: Record<keyof RestaurantService, Mock>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RestaurantController],
      providers:   [
        {
          provide:  RestaurantService,
          useValue: { delete: jest.fn(), findOne: jest.fn(), find: jest.fn() },
        },
        {
          provide:  Logger,
          useClass: TestingLogger,
        },
      ],
    }).compile();

    controller        = module.get<RestaurantController>(RestaurantController);
    restaurantService = module.get(RestaurantService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should call the restaurant service delete method', async () => {

    await controller.delete('1');

    expect(restaurantService.delete).toBeCalledTimes(1);
    expect(restaurantService.delete).toBeCalledWith('1');

  });

  it('should call the restaurant service findOne method', async () => {

    restaurantService.findOne.mockResolvedValue({
      _id:    'id',
      name:   'name',
      image:  'https://example.com/image.png',
      rating: 1,
    });

    const restaurantDto = await controller.getById('1');

    expect(restaurantService.findOne).toBeCalledTimes(1);
    expect(restaurantService.findOne).toBeCalledWith('1');

    expect(restaurantDto).toBeInstanceOf(RestaurantDto);
    expect(validateSync(restaurantDto)).toHaveLength(0);

  });

  it('should throw 404 exception if restaurant does not exist', async () => {

    restaurantService.findOne.mockResolvedValue(null);

    await expect(() => controller.getById('1')).rejects.toThrowError(NotFoundException);

    expect(restaurantService.findOne).toBeCalledTimes(1);
    expect(restaurantService.findOne).toBeCalledWith('1');

  });

  it('should call the restaurant findAll method', async () => {

    restaurantService.find.mockResolvedValue([
      {
        _id:    'id',
        name:   'name',
        image:  'https://example.com/image.png',
        rating: 1,
      },
    ]);

    const restaurantDtoList = await controller.getAll();

    expect(restaurantService.find).toBeCalledTimes(1);

    for (const restaurantDto of restaurantDtoList) {
      expect(restaurantDto).toBeInstanceOf(RestaurantDto);
      expect(validateSync(restaurantDto)).toHaveLength(0);
    }

  });

});
