import {
  Body,
  ConflictException,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { ApiCreatedResponse, ApiOkResponse } from '@nestjs/swagger';
import { plainToClass } from 'class-transformer';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { Roles } from '../auth/roles.decorator';
import { RolesGuard } from '../auth/roles.guard';
import { RestaurantService } from '../crud/restaurant.service';
import { CreateRestaurantDto } from '../dto/create-restaurant.dto';
import { RestaurantDto } from '../dto/restaurant.dto';
import { UpdateRestaurantDto } from '../dto/update-restaurant.dto';
import { UserRole } from '../dto/user.dto';
import { transformOptions } from '../transform-options';

@UseGuards(JwtAuthGuard, RolesGuard)
@Controller('restaurant')
export class RestaurantController {

  constructor(private readonly restaurantService: RestaurantService) {
  }

  @ApiCreatedResponse({ type: RestaurantDto })
  @Post()
  @Roles(UserRole.ADMIN)
  async create(@Body() createRestaurantDto: CreateRestaurantDto): Promise<RestaurantDto> {
    if (await this.restaurantService.exists(createRestaurantDto.name)) {
      throw new ConflictException();
    }
    const restaurant = await this.restaurantService.create(createRestaurantDto);
    return plainToClass(RestaurantDto, restaurant, transformOptions);
  }

  @Put(':restaurantId')
  @Roles(UserRole.ADMIN)
  async update(
    @Param('restaurantId') restaurantId: string,
    @Body() updateRestaurantDto: UpdateRestaurantDto,
  ) {
    await this.restaurantService.update(restaurantId, updateRestaurantDto);
  }

  @ApiOkResponse({ type: RestaurantDto })
  @Get(':restaurantId')
  @Roles(UserRole.ADMIN, UserRole.MEMBER)
  async getById(
    @Param('restaurantId') restaurantId: string,
  ): Promise<RestaurantDto> {

    const restaurant = await this.restaurantService.findOne(restaurantId);

    if (restaurant) {
      return plainToClass(RestaurantDto, restaurant, transformOptions);
    }

    throw new NotFoundException(`The restaurant with id ${restaurantId} does not exist`);

  }

  @Delete(':restaurantId')
  @Roles(UserRole.ADMIN)
  async delete(
    @Param('restaurantId') restaurantId: string,
  ) {
    await this.restaurantService.delete(restaurantId);
  }

  @ApiOkResponse({ type: RestaurantDto, isArray: true })
  @Get()
  @Roles(UserRole.ADMIN, UserRole.MEMBER)
  async getAll(): Promise<RestaurantDto[]> {

    const restaurantList = await this.restaurantService.find();

    return plainToClass(RestaurantDto, restaurantList, transformOptions);

  }

}
