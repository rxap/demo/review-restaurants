import { Expose } from 'class-transformer';
import { IsString } from 'class-validator';
import { UserRole } from './user.dto';

export class UpdateUserDto {

  @Expose()
  @IsString()
  username!: string;

  @Expose()
  @IsString()
  role!: UserRole;

}
