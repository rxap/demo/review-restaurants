import { Expose, Type } from 'class-transformer';
import { IsDate, IsNumber, IsString, Max, Min } from 'class-validator';

export class CreateReviewDto {

  @Expose()
  @IsNumber({ maxDecimalPlaces: 0 })
  @Min(1)
  @Max(5)
  rating: number;

  @Expose()
  @IsString()
  comment: string;

  @Expose()
  @IsDate()
  @Type(() => Date)
  dateOfVisit: Date;

}
