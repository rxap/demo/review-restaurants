import { Expose } from 'class-transformer';
import { IsString } from 'class-validator';

export class AccessTokenDto {

  @Expose()
  @IsString()
  accessToken: string;

}
