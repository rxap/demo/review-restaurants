import { Expose, Type } from 'class-transformer';
import { IsString } from 'class-validator';

export enum UserRole {
  ADMIN  = 'admin',
  MEMBER = 'member'
}

export class UserDto {

  @Expose()
  @IsString()
  @Type(() => String)
  _id!: string;

  @Expose()
  @IsString()
  username!: string;

  @Expose()
  @IsString()
  role!: UserRole;

}
