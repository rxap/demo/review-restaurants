import { Expose, Type } from 'class-transformer';
import { IsNumber, IsString, Max, Min } from 'class-validator';

export class RestaurantDto {

  @Expose()
  @IsString()
  @Type(() => String)
  _id!: string;

  @Expose()
  @IsString()
  name!: string;

  @Expose()
  @IsNumber()
  @Min(0)
  @Max(5)
  rating!: number;

}
