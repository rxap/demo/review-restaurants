import { Expose } from 'class-transformer';
import { IsString } from 'class-validator';
import { UserRole } from './user.dto';

export class RegisterDto {

  @Expose()
  @IsString()
  username!: string;

  @Expose()
  @IsString()
  password!: string;

  @Expose()
  @IsString()
  role!: UserRole;

}
