import { Expose } from 'class-transformer';
import { IsString } from 'class-validator';

export class UpdateRestaurantDto {

  @Expose()
  @IsString()
  name!: string;

}
