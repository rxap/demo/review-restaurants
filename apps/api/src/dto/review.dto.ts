import { Expose, Type } from 'class-transformer';
import { IsDate, IsInstance, IsNumber, IsString, Max, Min } from 'class-validator';
import { RestaurantDto } from './restaurant.dto';
import { UserDto } from './user.dto';

export class ReviewDto {

  @Expose()
  @IsString()
  @Type(() => String)
  _id: string;

  @Expose()
  @IsNumber({ maxDecimalPlaces: 0 })
  @Min(1)
  @Max(5)
  rating: number;

  @Expose()
  @IsDate()
  @Type(() => Date)
  date: Date;

  @Expose()
  @IsString()
  comment: string;

  @Expose()
  @IsInstance(UserDto)
  @Type(() => UserDto)
  user: UserDto;

  @Expose()
  @IsInstance(RestaurantDto)
  @Type(() => RestaurantDto)
  restaurant: RestaurantDto;

  @Expose()
  @IsDate()
  @Type(() => Date)
  dateOfVisit: Date;

}
