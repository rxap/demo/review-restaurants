import { Logger, ValidationPipe } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { IsDevMode, RXAP_GLOBAL_STATE, Server } from '@rxap/nest';
import * as cookieParser from 'cookie-parser';
import * as helmet from 'helmet';
import { v4 as uuid } from 'uuid';
import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import { transformOptions } from './transform-options';
import { validatorOptions } from './validator-options';

const server = new Server();

server.before(
  options => (options.logger = ['log', 'error', 'warn', 'debug', 'verbose']),
);

server.after((app) =>
  app.enableCors({
    credentials: true,
    origin:      true,
  }),
);

server.after((app, config) =>
  app.use(cookieParser(config.get('cookieSecret', uuid()))),
);

// server.after((app) => app.use(csurf({ cookie: true })));
server.after((app) => app.use(helmet()));
server.after((app) =>
  app.useGlobalPipes(
    new ValidationPipe({
      ...validatorOptions,
      transformOptions,
      transform: true,
    }),
  ),
);
server.after((app, config) =>
  SwaggerModule.setup(
    'api',
    app,
    SwaggerModule.createDocument(
      app,
      new DocumentBuilder()
        .addBearerAuth()
        .setVersion('1.0')
        .addServer(
          'http://localhost:' +
          config.get('port'),
        )
        .build(),
    ),
  ),
);

server
  .bootstrap(AppModule, environment)
  .then(() => Logger.log('IsDevMode: ' + IsDevMode()))
  .then(() => Logger.log('RXAP_GLOBAL_STATE: ' + JSON.stringify(RXAP_GLOBAL_STATE)))
  .catch((e) => console.error('Server bootstrap failed: ' + e.message));
