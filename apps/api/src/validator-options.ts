import { ValidatorOptions } from 'class-validator';

export const validatorOptions: ValidatorOptions = {
  enableDebugMessages:     true,
  skipUndefinedProperties: false,
  skipNullProperties:      false,
  skipMissingProperties:   false,
  forbidUnknownValues:     true,
};
