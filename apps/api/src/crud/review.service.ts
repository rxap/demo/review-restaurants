import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateReviewDto } from '../dto/create-review.dto';
import { UpdateReviewDto } from '../dto/update-review.dto';
import { Review } from '../schemas/review.schema';

@Injectable()
export class ReviewService {

  constructor(@InjectModel(Review.name) private readonly reviewModel: Model<Review>) {
  }

  async create(
    { rating, comment, dateOfVisit }: CreateReviewDto,
    restaurantId: string,
    userId: string,
  ): Promise<Review> {
    const review = await new this.reviewModel({
      rating,
      comment,
      date:        new Date().toISOString(),
      dateOfVisit: dateOfVisit.toISOString(),
      user:        userId,
      restaurant:  restaurantId,
    }).save();
    return this.reviewModel.findById(review._id).populate([{ path: 'user' }, { path: 'restaurant' }]).exec();
  }

  async delete(reviewId: string) {
    await this.reviewModel.findByIdAndDelete(reviewId).exec();
  }

  async findLatest(restaurantId: string): Promise<Review | null> {
    return this.reviewModel.findOne({ restaurant: restaurantId }).sort({ date: -1 }).exec();
  }

  async findAverageRatingForRestaurant(restaurantId: string): Promise<number> {
    const restingList = await this.reviewModel.find({ restaurant: restaurantId }).select('rating').exec();
    return restingList.reduce((sum, review) => sum + review.rating, 0) / restingList.length;
  }

  async find(restaurantId?: string) {
    if (restaurantId) {
      return this.reviewModel.find({ restaurant: restaurantId }).populate([{ path: 'user' }, { path: 'restaurant' }])
        .exec();
    } else {
      return this.reviewModel.find().populate([{ path: 'user' }, { path: 'restaurant' }]).exec();
    }
  }

  public async findBest(restaurantId: string): Promise<Review | null> {
    return this.reviewModel.findOne({ restaurant: restaurantId }).sort({ rating: -1 }).exec();
  }

  public async findWorst(restaurantId: string) {
    return this.reviewModel.findOne({ restaurant: restaurantId }).sort({ rating: 1 }).exec();
  }

  async clear() {
    return this.reviewModel.deleteMany({}).exec();
  }

  public async update(reviewId: string, updateReviewDto: UpdateReviewDto) {
    await this.reviewModel.updateOne({ _id: reviewId }, updateReviewDto);
  }

  public findOne(reviewId: string) {
    return this.reviewModel.findById(reviewId);
  }
}
