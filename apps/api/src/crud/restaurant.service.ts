import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateRestaurantDto } from '../dto/create-restaurant.dto';
import { UpdateRestaurantDto } from '../dto/update-restaurant.dto';
import { Restaurant } from '../schemas/restaurant.schema';

@Injectable()
export class RestaurantService {

  constructor(@InjectModel(Restaurant.name) private readonly restaurantModel: Model<Restaurant>) {
  }

  async create({ name }: CreateRestaurantDto): Promise<Restaurant> {
    return new this.restaurantModel({
      name,
      rating: 0,
    }).save();
  }

  async find(): Promise<Restaurant[]> {
    return this.restaurantModel.find().sort({ rating: -1 }).exec();
  }

  async delete(restaurantId: string): Promise<void> {
    await this.restaurantModel.findByIdAndDelete(restaurantId).exec();
  }

  async updateRating(restaurantId: string, rating: number): Promise<void> {
    await this.restaurantModel.updateOne({ _id: restaurantId }, { rating }).exec();
  }

  async findOne(restaurantId: string): Promise<Restaurant | null> {
    return this.restaurantModel.findOne({ _id: restaurantId }).exec();
  }

  async clear() {
    return this.restaurantModel.deleteMany({}).exec();
  }

  public async update(restaurantId: string, updateRestaurantDto: UpdateRestaurantDto) {
    await this.restaurantModel.updateOne({ _id: restaurantId }, updateRestaurantDto);
  }

  public async exists(name: string): Promise<boolean> {
    return this.restaurantModel.exists({ name });
  }

}
