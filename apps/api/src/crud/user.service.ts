import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { compare, hash } from 'bcrypt';
import { Model } from 'mongoose';
import { RegisterDto } from '../dto/register.dto';
import { UpdateUserDto } from '../dto/update-user.dto';
import { User } from '../schemas/user.schema';

@Injectable()
export class UserService {

  constructor(
    @InjectModel(User.name) private readonly userModel: Model<User>,
    private readonly logger: Logger,
  ) {
  }

  async create({ username, password, role }: RegisterDto): Promise<User> {
    const passwordHash = await hash(password, 1);
    const createdUser  = new this.userModel({
      username: username,
      role:     role,
      password: passwordHash,
    });
    return createdUser.save();
  }

  async getUserWithCredentials(username: string, password: string): Promise<User | null> {
    const user = await this.userModel.findOne({ username }).exec();
    if (user) {
      const isValidPassword = await compare(password, user.password);
      if (isValidPassword) {
        return user;
      }
    }
    return null;
  }

  async exists(username: string): Promise<boolean> {
    return this.userModel.exists({ username });
  }

  async clear() {
    await this.userModel.deleteMany({}).exec();
  }

  public find() {
    return this.userModel.find().exec();
  }

  public async update(userId: string, { username, role }: UpdateUserDto) {
    await this.userModel.updateOne({ _id: userId }, { username, role });
  }

  public async delete(userId: string) {
    await this.userModel.findByIdAndDelete(userId);
  }

  public async findOne(userId: string) {
    return this.userModel.findOne({ _id: userId }).exec();
  }
}
