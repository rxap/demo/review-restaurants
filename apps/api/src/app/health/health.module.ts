import { Logger, Module } from '@nestjs/common';
import { TerminusModule } from '@nestjs/terminus';
import { DatabaseHealthIndicator } from './database.health-indicator';
import { HealthController } from './health.controller';

@Module({
  imports:     [TerminusModule],
  providers:   [Logger, DatabaseHealthIndicator],
  controllers: [HealthController],
})
export class HealthModule {
}
