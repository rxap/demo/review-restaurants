import { Injectable } from '@nestjs/common';
import { InjectConnection } from '@nestjs/mongoose';
import { HealthCheckError, HealthIndicator, HealthIndicatorResult } from '@nestjs/terminus';
import { Connection } from 'mongoose';

@Injectable()
export class DatabaseHealthIndicator extends HealthIndicator {

  constructor(@InjectConnection() private readonly connection: Connection) {
    super();
  }

  public async isHealthy(): Promise<HealthIndicatorResult> {

    if (this.connection.readyState === 1) {
      return this.getStatus('database', true);
    }

    throw new HealthCheckError(
      'Could not connect to database' + this.connection.readyState,
      this.getStatus('database', false),
    );

  }
}
