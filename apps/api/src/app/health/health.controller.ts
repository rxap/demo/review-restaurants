import { Controller, Get, Inject } from '@nestjs/common';
import { HealthCheck, HealthCheckResult, HealthCheckService } from '@nestjs/terminus';
import { DatabaseHealthIndicator } from './database.health-indicator';

@Controller('health')
export class HealthController {
  constructor(
    @Inject(HealthCheckService) private readonly health: HealthCheckService,
    @Inject(DatabaseHealthIndicator)
    private readonly databaseHealthIndicator: DatabaseHealthIndicator,
  ) {
  }

  @Get('database')
  @HealthCheck()
  public database(): Promise<HealthCheckResult> {
    return this.health.check([
      async () => this.databaseHealthIndicator.isHealthy(),
    ]);
  }

  @Get()
  @HealthCheck()
  public healthCheck(): Promise<HealthCheckResult> {
    return this.health.check([
      async () => this.databaseHealthIndicator.isHealthy(),
    ]);
  }
}
