import { Injectable, Logger, OnModuleInit } from '@nestjs/common';
import { IsDevMode } from '@rxap/nest';

@Injectable()
export class SeedService implements OnModuleInit {

  constructor(
    private readonly logger: Logger,
  ) {
  }

  public async onModuleInit() {

    if (!IsDevMode()) {
      return;
    }

    this.logger.debug('seed database', 'SeedService');

    const promiseList: Promise<any>[] = [];

    await Promise.all(promiseList);

  }

}
