import { Logger, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { APP_GUARD, APP_INTERCEPTOR } from '@nestjs/core';
import { MongooseModule } from '@nestjs/mongoose';
import { MongooseModuleOptions } from '@nestjs/mongoose/dist/interfaces/mongoose-options.interface';
import { ThrottlerGuard, ThrottlerModule } from '@nestjs/throttler';
import { LoadConfig } from '@rxap/nest';
import { RavenInterceptor, RavenModule } from 'nest-raven';
import { AuthModule } from '../auth/auth.module';
import { E2eModule } from '../e2e/e2e.module';
import { RestaurantModule } from '../restaurant/restaurant.module';
import { ReviewModule } from '../review/review.module';
import { UserModule } from '../user/user.module';

import { AppController } from './app.controller';
import { Config } from './config';
import { HealthModule } from './health/health.module';
import { SeedService } from './seed.service';

export function MongooseOptionsFactory(config: ConfigService): MongooseModuleOptions {
  return {
    uri: `mongodb://${config.get('mongodb.host') ?? 'mongodb'}/review`,
  };
}

@Module({
  imports:     [
    RavenModule,
    ThrottlerModule.forRoot({
      ttl:   1,
      limit: 10,
    }),
    ConfigModule.forRoot({
      isGlobal: true,
      load:     [LoadConfig(Config)],
    }),
    HealthModule,
    MongooseModule.forRootAsync({
      useFactory: MongooseOptionsFactory,
      inject:     [ConfigService],
    }),
    ReviewModule,
    RestaurantModule,
    AuthModule,
    E2eModule,
    UserModule,
  ],
  controllers: [AppController],
  providers: [
    ThrottlerGuard,
    {
      provide:  APP_INTERCEPTOR,
      useValue: new RavenInterceptor(),
    },
    {
      provide:  APP_GUARD,
      useClass: ThrottlerGuard,
    },
    Logger,
    SeedService,
  ],
})
export class AppModule {
}
