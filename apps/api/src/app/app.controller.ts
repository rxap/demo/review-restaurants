import { Controller, Get } from '@nestjs/common';
import { existsSync, readFileSync } from 'fs';
import { join } from 'path';

@Controller()
export class AppController {

  @Get()
  getData() {
    const buildFilePath = join(process.cwd(), 'build.json');
    if (existsSync(buildFilePath)) {
      return readFileSync(buildFilePath)?.toString('utf-8') ?? { version: 'local-dev' };
    }
    return { version: 'local-dev' };
  }

}
