import { BaseConfig } from '@rxap/nest';
import { Type } from 'class-transformer';
import { IsInstance, IsOptional, IsString } from 'class-validator';

export class JwtConfig {

  @IsOptional()
  @IsString()
  secret?: string;
}

export class MongoDBConfig {

  @IsOptional()
  @IsString()
  host?: string;

}

export class Config extends BaseConfig {

  @IsOptional()
  @IsInstance(JwtConfig)
  @Type(() => JwtConfig)
  jwt?: JwtConfig;

  @IsOptional()
  @IsInstance(MongoDBConfig)
  @Type(() => MongoDBConfig)
  mongodb?: MongoDBConfig;

}
