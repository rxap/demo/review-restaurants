import { Logger, Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { JwtModuleOptions } from '@nestjs/jwt/dist/interfaces/jwt-module-options.interface';
import { MongooseModule } from '@nestjs/mongoose';
import { UserService } from '../crud/user.service';
import { User, UserSchema } from '../schemas/user.schema';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { JwtStrategy } from './jwt-strategy';
import { LocalStrategy } from './local-strategy';

export function JwtOptionsFactory(config: ConfigService): JwtModuleOptions {
  return {
    secret:      config.get('jwt.secret') ?? 'secret',
    signOptions: { expiresIn: '1h' },
  };
}

@Module({
  providers:   [AuthService, UserService, LocalStrategy, Logger, JwtStrategy],
  imports:     [
    JwtModule.registerAsync({
      useFactory: JwtOptionsFactory,
      inject:     [ConfigService],
    }),
    MongooseModule.forFeature([
      { name: User.name, schema: UserSchema },
    ]),
  ],
  controllers: [AuthController],
})
export class AuthModule {
}
