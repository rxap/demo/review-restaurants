import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { UserDto } from '../dto/user.dto';

export const GetUser = createParamDecorator<any, any, UserDto>(
  (data: unknown, ctx: ExecutionContext) => ctx.switchToHttp().getRequest().user,
);
