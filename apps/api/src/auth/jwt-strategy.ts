import { Injectable, UnauthorizedException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import { plainToClass } from 'class-transformer';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { UserService } from '../crud/user.service';
import { UserDto } from '../dto/user.dto';
import { transformOptions } from '../transform-options';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    config: ConfigService,
    private readonly userService: UserService,
  ) {
    super({
      jwtFromRequest:   ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey:      config.get('jwt.secret'),
    });
  }

  async validate(payload: Record<string, string>): Promise<UserDto> {

    const user = plainToClass(
      UserDto,
      { _id: payload.sub, username: payload.username, role: payload.role },
      transformOptions,
    );

    if (!await this.userService.findOne(user._id)) {
      throw new UnauthorizedException('User from the jwt token does not exist');
    }

    return user;
  }

}
