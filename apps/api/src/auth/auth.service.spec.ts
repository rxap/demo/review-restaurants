import { JwtService } from '@nestjs/jwt';
import { Test, TestingModule } from '@nestjs/testing';
import { plainToClass } from 'class-transformer';
import { validateSync } from 'class-validator';
import { UserService } from '../crud/user.service';
import { AccessTokenDto } from '../dto/access-token.dto';
import { UserDto, UserRole } from '../dto/user.dto';
import { AuthService } from './auth.service';
import Mock = jest.Mock;

describe('AuthService', () => {
  let service: AuthService;

  let userService: Record<keyof UserService, Mock>;
  let jwtService: Record<keyof JwtService, Mock>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        {
          provide:  UserService,
          useValue: { getUserWithCredentials: jest.fn() },
        },
        {
          provide:  JwtService,
          useValue: { sign: jest.fn() },
        },
      ],
    }).compile();

    service     = module.get<AuthService>(AuthService);
    userService = module.get(UserService);
    jwtService  = module.get(JwtService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create access token dto from user dto', () => {

    jwtService.sign.mockReturnValue('accessToken');

    const userDto = plainToClass(UserDto, { username: 'user', _id: 'id', role: UserRole.MEMBER });

    const accessTokenDto = service.login(userDto);

    expect(jwtService.sign).toBeCalledTimes(1);
    expect(jwtService.sign).toBeCalledWith({
      username: userDto.username,
      sub:      userDto._id,
      role:     userDto.role,
    });
    expect(accessTokenDto).toBeInstanceOf(AccessTokenDto);
    expect(validateSync(accessTokenDto)).toHaveLength(0);

  });

  it('should return user dto if user is valid', async () => {

    userService.getUserWithCredentials.mockResolvedValue({ username: 'user', _id: 'id', role: UserRole.MEMBER });

    const userDto = await service.validateUser('user', 'password');

    expect(userService.getUserWithCredentials).toBeCalledTimes(1);
    expect(userService.getUserWithCredentials).toBeCalledWith('user', 'password');
    expect(userDto).toBeInstanceOf(UserDto);
    expect(validateSync(userDto)).toHaveLength(0);


  });

  it('should return null if user is not valid', async () => {

    userService.getUserWithCredentials.mockResolvedValue(null);

    const userDto = await service.validateUser('user', 'password');

    expect(userService.getUserWithCredentials).toBeCalledTimes(1);
    expect(userService.getUserWithCredentials).toBeCalledWith('user', 'password');
    expect(userDto).toBe(null);

  });

});
