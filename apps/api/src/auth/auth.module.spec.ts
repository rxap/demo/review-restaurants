import { INestApplication, ValidationPipe } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { Test } from '@nestjs/testing';
import * as request from 'supertest';
import { UserService } from '../crud/user.service';
import { UserRole } from '../dto/user.dto';
import { closeInMongodConnection, rootMongooseTestModule } from '../mongoose-test.module';
import { RestaurantModule } from '../restaurant/restaurant.module';
import { User } from '../schemas/user.schema';
import { transformOptions } from '../transform-options';
import { validatorOptions } from '../validator-options';
import { AuthModule } from './auth.module';
import { AuthService } from './auth.service';

describe('AuthModule', () => {

  let app: INestApplication;
  let authService: AuthService;
  let userService: UserService;
  let adminUser: User;
  let memberUser: User;

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [
        rootMongooseTestModule(),
        AuthModule,
        ConfigModule.forRoot({
          isGlobal: true,
          load:     [() => ({ jwt: { secret: 'secret' } })],
        }),
        RestaurantModule,
      ],
    })
      .compile();

    app = moduleRef.createNestApplication();

    app.useGlobalPipes(
      new ValidationPipe({
        ...validatorOptions,
        transformOptions,
        transform: true,
      }),
    );

    await app.init();

    userService = app.get(UserService);
    authService = app.get(AuthService);

  });

  beforeEach(async () => {
    memberUser = await userService.create({ username: 'member', password: 'e2e', role: UserRole.MEMBER });
    adminUser  = await userService.create({ username: 'admin', password: 'e2e', role: UserRole.ADMIN });
  });

  afterEach(async () => {
    await userService.clear();
  });

  afterAll(async () => {
    await app.close();
    await closeInMongodConnection();
    jest.resetAllMocks();
  });

  describe('POST /auth/login', () => {

    it('should response with 401 if the username does not exist', async () => {

      await request(app.getHttpServer())
        .post(`/auth/login`)
        .send({
          username: 'username',
          password: 'password',
        })
        .expect(401);

    });

    it('should response with 401 if the password is incorrect', async () => {

      await userService.create({ username: 'username', password: 'password', role: UserRole.ADMIN });

      await request(app.getHttpServer())
        .post(`/auth/login`)
        .send({
          username: 'username',
          password: 'password-incorrect',
        })
        .expect(401);

    });

    it('should response with 200 if the password and username is correct', async () => {

      await userService.create({ username: 'username', password: 'password', role: UserRole.ADMIN });

      await request(app.getHttpServer())
        .post(`/auth/login`)
        .send({
          username: 'username',
          password: 'password',
        })
        .expect(200);

    });

  });

  describe('POST /auth/register', () => {

    it('should response with 409 if a user with the same username already exists', async () => {

      await userService.create({ username: 'username', password: 'password', role: UserRole.ADMIN });

      await request(app.getHttpServer())
        .post(`/auth/register`)
        .send({
          username: 'username',
          password: 'password',
          role:     UserRole.ADMIN,
        })
        .expect(409);

    });

    it('should response with 201', async () => {

      await request(app.getHttpServer())
        .post(`/auth/register`)
        .send({
          username: 'username',
          password: 'password',
          role:     UserRole.ADMIN,
        })
        .expect(201);

    });

  });

  describe('GET /auth/check', () => {

    it('should response with 401 if the access token is invalid', async () => {

      await request(app.getHttpServer())
        .get(`/auth/check`)
        .set('Authorization', 'Bearer null')
        .expect(401);

    });

    it('should response with 200 if the access token is valid', async () => {

      const { accessToken } = await authService.login({ username: 'member', _id: adminUser._id, role: UserRole.ADMIN });

      await request(app.getHttpServer())
        .get(`/auth/check`)
        .set('Authorization', 'Bearer ' + accessToken)
        .expect(200);

    });

  });

});
