import { Body, ConflictException, Controller, Get, HttpCode, Logger, Post, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBody, ApiOkResponse } from '@nestjs/swagger';
import { UserService } from '../crud/user.service';
import { AccessTokenDto } from '../dto/access-token.dto';
import { LoginDto } from '../dto/login.dto';
import { RegisterDto } from '../dto/register.dto';
import { UserDto } from '../dto/user.dto';
import { AuthService } from './auth.service';
import { GetUser } from './get-user.decorator';
import { JwtAuthGuard } from './jwt-auth.guard';

@Controller('auth')
export class AuthController {

  constructor(
    private readonly authService: AuthService,
    private readonly userService: UserService,
    private readonly logger: Logger,
  ) {
  }

  @ApiBody({ type: LoginDto })
  @ApiOkResponse({ type: AccessTokenDto })
  @UseGuards(AuthGuard('local'))
  @HttpCode(200)
  @Post('login')
  login(
    @GetUser() user: UserDto,
  ): AccessTokenDto {
    return this.authService.login(user);
  }


  @Post('register')
  async register(
    @Body() register: RegisterDto,
  ) {
    const exists = await this.userService.exists(register.username);
    if (exists) {
      throw new ConflictException('Use a unique username. A user with that username already exists.');
    }
    await this.userService.create(register);
  }


  /**
   * Used to check if the jwt token is valid.
   */
  @Get('check')
  @UseGuards(JwtAuthGuard)
  check() {
    return true;
  }

}
