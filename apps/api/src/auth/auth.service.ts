import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { plainToClass } from 'class-transformer';
import { UserService } from '../crud/user.service';
import { AccessTokenDto } from '../dto/access-token.dto';
import { UserDto } from '../dto/user.dto';
import { transformOptions } from '../transform-options';

@Injectable()
export class AuthService {

  constructor(
    private userService: UserService,
    private jwtService: JwtService,
  ) {
  }

  async validateUser(username: string, password: string): Promise<UserDto | null> {
    const user = await this.userService.getUserWithCredentials(username, password);
    if (user) {
      return plainToClass(UserDto, user, transformOptions);
    }
    return null;
  }

  login(user: UserDto): AccessTokenDto {
    const payload = { username: user.username, sub: user._id, role: user.role };
    return plainToClass(AccessTokenDto, {
      accessToken: this.jwtService.sign(payload),
    }, transformOptions);
  }

}
