import { ConflictException, Logger } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { TestingLogger } from '@nestjs/testing/services/testing-logger.service';
import { plainToClass } from 'class-transformer';
import { validateSync } from 'class-validator';
import { UserService } from '../crud/user.service';
import { AccessTokenDto } from '../dto/access-token.dto';
import { RegisterDto } from '../dto/register.dto';
import { UserDto, UserRole } from '../dto/user.dto';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import Mock = jest.Mock;

describe('AuthController', () => {
  let controller: AuthController;

  let authService: Record<keyof AuthService, Mock>;
  let userService: Record<keyof UserService, Mock>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [
        {
          provide:  AuthService,
          useValue: { login: jest.fn() },
        },
        {
          provide:  UserService,
          useValue: { create: jest.fn(), exists: jest.fn() },
        },
        {
          provide:  Logger,
          useClass: TestingLogger,
        },
      ],
    }).compile();

    controller  = module.get<AuthController>(AuthController);
    userService = module.get(UserService);
    authService = module.get(AuthService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should call the login method with the user dto', () => {

    authService.login.mockReturnValue(plainToClass(AccessTokenDto, { accessToken: 'accessToken' }));

    const userDto = plainToClass(UserDto, { username: 'user', _id: 'id', role: UserRole.MEMBER });

    const accessTokenDto = controller.login(userDto);

    expect(authService.login).toBeCalledTimes(1);
    expect(authService.login).toBeCalledWith(userDto);
    expect(accessTokenDto).toBeInstanceOf(AccessTokenDto);
    expect(validateSync(accessTokenDto)).toHaveLength(0);

  });

  it('should call the create method with the register dto', async () => {

    userService.exists.mockResolvedValue(false);

    const registerDto = plainToClass(RegisterDto, { username: 'user', password: 'password' });

    await controller.register(registerDto);

    expect(userService.create).toBeCalledTimes(1);
    expect(userService.create).toBeCalledWith(registerDto);

  });

  it('should throw Conflict Exception is user already registered', async () => {

    userService.exists.mockResolvedValue(true);

    const registerDto = plainToClass(RegisterDto, { username: 'user', password: 'password' });

    await expect(() => controller.register(registerDto)).rejects.toThrowError(ConflictException);

  });

  it('should return true if check method is called', () => {
    expect(controller.check()).toBe(true);
  });

});
