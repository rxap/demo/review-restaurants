import { Body, Controller, Delete, Get, NotFoundException, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { ApiCreatedResponse, ApiOkResponse, ApiQuery } from '@nestjs/swagger';
import { plainToClass } from 'class-transformer';
import { GetUser } from '../auth/get-user.decorator';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { Roles } from '../auth/roles.decorator';
import { RolesGuard } from '../auth/roles.guard';
import { RestaurantService } from '../crud/restaurant.service';
import { ReviewService } from '../crud/review.service';
import { CreateReviewDto } from '../dto/create-review.dto';
import { ReviewDto } from '../dto/review.dto';
import { UpdateReviewDto } from '../dto/update-review.dto';
import { UserDto, UserRole } from '../dto/user.dto';
import { transformOptions } from '../transform-options';

@UseGuards(JwtAuthGuard, RolesGuard)
@Controller('review')
export class ReviewController {

  constructor(
    private readonly reviewService: ReviewService,
    private readonly restaurantService: RestaurantService,
  ) {
  }

  @Get('restaurant/:restaurantId/best')
  @Roles(UserRole.ADMIN, UserRole.MEMBER)
  async getBest(
    @Param('restaurantId')
      restaurantId: string,
  ) {
    const review = await this.reviewService.findBest(restaurantId);
    if (!review) {
      throw new NotFoundException();
    }
    return plainToClass(ReviewDto, review, transformOptions);
  }

  @Get('restaurant/:restaurantId/worst')
  @Roles(UserRole.ADMIN, UserRole.MEMBER)
  async getWorst(
    @Param('restaurantId')
      restaurantId: string,
  ) {
    const review = await this.reviewService.findWorst(restaurantId);
    if (!review) {
      throw new NotFoundException();
    }
    return plainToClass(ReviewDto, review, transformOptions);
  }

  @Get('restaurant/:restaurantId/latest')
  @Roles(UserRole.ADMIN, UserRole.MEMBER)
  async getLatest(
    @Param('restaurantId')
      restaurantId: string,
  ) {
    const review = await this.reviewService.findLatest(restaurantId);
    if (!review) {
      throw new NotFoundException();
    }
    return plainToClass(ReviewDto, review, transformOptions);
  }

  @Delete(':reviewId/restaurant/:restaurantId')
  @Roles(UserRole.ADMIN)
  async delete(
    @Param('restaurantId')
      restaurantId: string,
    @Param('reviewId')
      reviewId: string,
  ) {
    await this.reviewService.delete(reviewId);
    const averageRaging = await this.reviewService.findAverageRatingForRestaurant(restaurantId);
    await this.restaurantService.updateRating(restaurantId, averageRaging);
  }

  @Put(':reviewId/restaurant/:restaurantId')
  @Roles(UserRole.ADMIN)
  async update(
    @Param('restaurantId')
      restaurantId: string,
    @Param('reviewId')
      reviewId: string,
    @Body() updateReviewDto: UpdateReviewDto,
  ) {
    await this.reviewService.update(reviewId, updateReviewDto);
    const averageRaging = await this.reviewService.findAverageRatingForRestaurant(restaurantId);
    await this.restaurantService.updateRating(restaurantId, averageRaging);
  }

  @ApiQuery({ name: 'restaurantId', required: false })
  @ApiOkResponse({ type: ReviewDto, isArray: true })
  @Get()
  @Roles(UserRole.ADMIN, UserRole.MEMBER)
  async getAll(
    @Query('restaurantId')
      restaurantId?: string,
  ): Promise<ReviewDto[]> {
    const reviewList = await this.reviewService.find(restaurantId);
    return plainToClass(ReviewDto, reviewList, transformOptions);
  }

  @ApiCreatedResponse({ type: ReviewDto })
  @Post('restaurant/:restaurantId')
  @Roles(UserRole.ADMIN, UserRole.MEMBER)
  async create(
    @Body()
      createReviewDto: CreateReviewDto,
    @Param('restaurantId')
      restaurantId: string,
    @GetUser()
      user: UserDto,
  ): Promise<ReviewDto> {
    const review        = await this.reviewService.create(createReviewDto, restaurantId, user._id);
    const averageRaging = await this.reviewService.findAverageRatingForRestaurant(restaurantId);
    await this.restaurantService.updateRating(restaurantId, averageRaging);
    return plainToClass(ReviewDto, review, transformOptions);
  }

}
