import { INestApplication, ValidationPipe } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { Test } from '@nestjs/testing';
import { plainToClass } from 'class-transformer';
import * as request from 'supertest';
import { AuthModule } from '../auth/auth.module';
import { AuthService } from '../auth/auth.service';
import { RestaurantService } from '../crud/restaurant.service';
import { ReviewService } from '../crud/review.service';
import { UserService } from '../crud/user.service';
import { CreateReviewDto } from '../dto/create-review.dto';
import { UserRole } from '../dto/user.dto';
import { closeInMongodConnection, rootMongooseTestModule } from '../mongoose-test.module';
import { Restaurant } from '../schemas/restaurant.schema';
import { Review } from '../schemas/review.schema';
import { User } from '../schemas/user.schema';
import { transformOptions } from '../transform-options';
import { validatorOptions } from '../validator-options';
import { ReviewModule } from './review.module';

describe('ReviewModule', () => {

  let app: INestApplication;
  let authService: AuthService;
  let restaurant: Restaurant;
  let reviewService: ReviewService;
  let restaurantService: RestaurantService;
  let adminUser: User;
  let memberUser: User;

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [
        rootMongooseTestModule(),
        AuthModule,
        ConfigModule.forRoot({
          isGlobal: true,
          load:     [() => ({ jwt: { secret: 'secret' } })],
        }),
        ReviewModule,
      ],
    })
      .compile();

    app = moduleRef.createNestApplication();

    app.useGlobalPipes(
      new ValidationPipe({
        ...validatorOptions,
        transformOptions,
        transform: true,
      }),
    );

    await app.init();

    const userService = app.get(UserService);
    restaurantService = app.get(RestaurantService);
    authService       = app.get(AuthService);
    reviewService     = app.get(ReviewService);

    memberUser = await userService.create({ username: 'member', password: 'e2e', role: UserRole.MEMBER });
    adminUser  = await userService.create({ username: 'admin', password: 'e2e', role: UserRole.ADMIN });

    restaurant = await restaurantService.create({ name: 'e2e' });

  });

  afterEach(async () => {
    await reviewService.clear();
  });

  afterAll(async () => {
    await app.close();
    await closeInMongodConnection();
    jest.resetAllMocks();
  });

  describe('GET /review/restaurant/:restaurantId/best', () => {

    it('should response with 401 if the authentication bearer header is not defined', async () => {

      await request(app.getHttpServer())
        .get(`/review/restaurant/${restaurant._id}/best`)
        .expect(401);

    });

    it('should response with 404 if the restaurant does not have any reviews', async () => {

      const { accessToken } = await authService.login({
        username: 'member',
        _id:      adminUser._id,
        role:     UserRole.MEMBER,
      });

      await request(app.getHttpServer())
        .get(`/review/restaurant/${restaurant._id}/best`)
        .set('Authorization', 'Bearer ' + accessToken)
        .expect(404);

    });

    it('should response with 404 if the restaurant does not exist', async () => {

      const { accessToken } = await authService.login({
        username: 'member',
        _id:      adminUser._id,
        role:     UserRole.MEMBER,
      });

      await request(app.getHttpServer())
        .get(`/review/restaurant/not-existing/best`)
        .set('Authorization', 'Bearer ' + accessToken)
        .expect(404);

    });

    it('should response with 200 and the review dto', async () => {

      const dateOfVisit = new Date();

      const review = await reviewService.create(
        plainToClass(
          CreateReviewDto,
          { rating: 5, comment: 'comment', dateOfVisit: dateOfVisit.toISOString() },
          transformOptions,
        ),
        restaurant._id,
        memberUser._id,
      );

      await reviewService.create(
        plainToClass(
          CreateReviewDto,
          { rating: 4, comment: 'comment', dateOfVisit: dateOfVisit.toISOString() },
          transformOptions,
        ),
        restaurant._id,
        memberUser._id,
      );

      await reviewService.create(
        plainToClass(
          CreateReviewDto,
          { rating: 2, comment: 'comment', dateOfVisit: dateOfVisit.toISOString() },
          transformOptions,
        ),
        restaurant._id,
        memberUser._id,
      );

      await reviewService.create(
        plainToClass(
          CreateReviewDto,
          { rating: 1, comment: 'comment', dateOfVisit: dateOfVisit.toISOString() },
          transformOptions,
        ),
        restaurant._id,
        memberUser._id,
      );

      const { accessToken } = await authService.login({
        username: 'member',
        _id:      adminUser._id,
        role:     UserRole.MEMBER,
      });

      await request(app.getHttpServer())
        .get(`/review/restaurant/${restaurant._id}/best`)
        .set('Authorization', 'Bearer ' + accessToken)
        .expect(200)
        .expect({
          _id:         review._id.toString(),
          comment:     'comment',
          date:        review.date,
          dateOfVisit: review.dateOfVisit,
          rating:      5,
          restaurant:  { _id: restaurant._id.toString() },
          user:        { _id: memberUser._id.toString() },
        });

    });

  });

  describe('GET /review/restaurant/:restaurantId/worst', () => {

    it('should response with 401 if the authentication bearer header is not defined', async () => {

      await request(app.getHttpServer())
        .get(`/review/restaurant/${restaurant._id}/worst`)
        .expect(401);

    });

    it('should response with 404 if the restaurant does not have any reviews', async () => {

      const { accessToken } = await authService.login({
        username: 'member',
        _id:      adminUser._id,
        role:     UserRole.MEMBER,
      });

      await request(app.getHttpServer())
        .get(`/review/restaurant/${restaurant._id}/worst`)
        .set('Authorization', 'Bearer ' + accessToken)
        .expect(404);

    });

    it('should response with 404 if the restaurant does not exist', async () => {

      const { accessToken } = await authService.login({
        username: 'member',
        _id:      adminUser._id,
        role:     UserRole.MEMBER,
      });

      await request(app.getHttpServer())
        .get(`/review/restaurant/not-existing/worst`)
        .set('Authorization', 'Bearer ' + accessToken)
        .expect(404);

    });

    it('should response with 200 and the review dto', async () => {

      const dateOfVisit = new Date();

      const review = await reviewService.create(
        plainToClass(
          CreateReviewDto,
          { rating: 1, comment: 'comment', dateOfVisit: dateOfVisit.toISOString() },
          transformOptions,
        ),
        restaurant._id,
        memberUser._id,
      );

      await reviewService.create(
        plainToClass(
          CreateReviewDto,
          { rating: 4, comment: 'comment', dateOfVisit: dateOfVisit.toISOString() },
          transformOptions,
        ),
        restaurant._id,
        memberUser._id,
      );

      await reviewService.create(
        plainToClass(
          CreateReviewDto,
          { rating: 2, comment: 'comment', dateOfVisit: dateOfVisit.toISOString() },
          transformOptions,
        ),
        restaurant._id,
        memberUser._id,
      );

      await reviewService.create(
        plainToClass(
          CreateReviewDto,
          { rating: 5, comment: 'comment', dateOfVisit: dateOfVisit.toISOString() },
          transformOptions,
        ),
        restaurant._id,
        memberUser._id,
      );

      const { accessToken } = await authService.login({
        username: 'member',
        _id:      adminUser._id,
        role:     UserRole.MEMBER,
      });

      await request(app.getHttpServer())
        .get(`/review/restaurant/${restaurant._id}/worst`)
        .set('Authorization', 'Bearer ' + accessToken)
        .expect(200)
        .expect({
          _id:         review._id.toString(),
          comment:     'comment',
          date:        review.date,
          dateOfVisit: review.dateOfVisit,
          rating:      1,
          restaurant:  { _id: restaurant._id.toString() },
          user:        { _id: memberUser._id.toString() },
        });

    });

  });

  describe('GET /review/restaurant/:restaurantId/latest', () => {

    it('should response with 401 if the authentication bearer header is not defined', async () => {

      await request(app.getHttpServer())
        .get(`/review/restaurant/${restaurant._id}/latest`)
        .expect(401);

    });

    it('should response with 404 if the restaurant does not have any reviews', async () => {

      const { accessToken } = await authService.login({
        username: 'member',
        _id:      adminUser._id,
        role:     UserRole.MEMBER,
      });

      await request(app.getHttpServer())
        .get(`/review/restaurant/${restaurant._id}/latest`)
        .set('Authorization', 'Bearer ' + accessToken)
        .expect(404);

    });

    it('should response with 404 if the restaurant does not exist', async () => {

      const { accessToken } = await authService.login({
        username: 'member',
        _id:      adminUser._id,
        role:     UserRole.MEMBER,
      });

      await request(app.getHttpServer())
        .get(`/review/restaurant/not-existing/latest`)
        .set('Authorization', 'Bearer ' + accessToken)
        .expect(404);

    });

    it('should response with 200 and the review dto', async () => {

      const dateOfVisit = new Date();

      await reviewService.create(
        plainToClass(
          CreateReviewDto,
          { rating: 1, comment: 'comment', dateOfVisit: dateOfVisit.toISOString() },
          transformOptions,
        ),
        restaurant._id,
        memberUser._id,
      );

      await reviewService.create(
        plainToClass(
          CreateReviewDto,
          { rating: 4, comment: 'comment', dateOfVisit: dateOfVisit.toISOString() },
          transformOptions,
        ),
        restaurant._id,
        memberUser._id,
      );

      await reviewService.create(
        plainToClass(
          CreateReviewDto,
          { rating: 2, comment: 'comment', dateOfVisit: dateOfVisit.toISOString() },
          transformOptions,
        ),
        restaurant._id,
        memberUser._id,
      );

      const review = await reviewService.create(
        plainToClass(
          CreateReviewDto,
          { rating: 3, comment: 'comment', dateOfVisit: dateOfVisit.toISOString() },
          transformOptions,
        ),
        restaurant._id,
        memberUser._id,
      );

      const { accessToken } = await authService.login({
        username: 'member',
        _id:      adminUser._id,
        role:     UserRole.MEMBER,
      });

      await request(app.getHttpServer())
        .get(`/review/restaurant/${restaurant._id}/latest`)
        .set('Authorization', 'Bearer ' + accessToken)
        .expect(200)
        .expect({
          _id:         review._id.toString(),
          comment:     'comment',
          date:        review.date,
          dateOfVisit: review.dateOfVisit,
          rating:      3,
          restaurant:  { _id: restaurant._id.toString() },
          user:        { _id: memberUser._id.toString() },
        });

    });

  });

  describe('DELETE /review/:reviewId/restaurant/:restaurantId', () => {

    let review: Review;

    beforeEach(async () => {

      const dateOfVisit = new Date();

      await reviewService.create(
        plainToClass(
          CreateReviewDto,
          { rating: 4, comment: 'comment', dateOfVisit: dateOfVisit.toISOString() },
          transformOptions,
        ),
        restaurant._id,
        memberUser._id,
      );

      review = await reviewService.create(
        plainToClass(
          CreateReviewDto,
          { rating: 2, comment: 'comment', dateOfVisit: dateOfVisit.toISOString() },
          transformOptions,
        ),
        restaurant._id,
        memberUser._id,
      );

      await reviewService.create(
        plainToClass(
          CreateReviewDto,
          { rating: 3, comment: 'comment', dateOfVisit: dateOfVisit.toISOString() },
          transformOptions,
        ),
        restaurant._id,
        memberUser._id,
      );

      await restaurantService.updateRating(restaurant._id, (4 + 2 + 3) / 3);

    });

    it('should response with 401 if the authentication bearer header is not defined', async () => {

      await request(app.getHttpServer())
        .delete(`/review/${review._id}/restaurant/${restaurant._id}`)
        .expect(401);

    });

    it('should response with 403 if the user has not the admin role', async () => {

      const { accessToken } = await authService.login({
        username: 'member',
        _id:      adminUser._id,
        role:     UserRole.MEMBER,
      });

      await request(app.getHttpServer())
        .delete(`/review/${review._id}/restaurant/${restaurant._id}`)
        .set('Authorization', 'Bearer ' + accessToken)
        .expect(403);

    });

    it('should response with 200 if the delete is successful', async () => {

      const { accessToken } = await authService.login({ username: 'admin', _id: adminUser._id, role: UserRole.ADMIN });

      await request(app.getHttpServer())
        .delete(`/review/${review._id}/restaurant/${restaurant._id}`)
        .set('Authorization', 'Bearer ' + accessToken)
        .expect(200);

      await expect(reviewService.findOne(review._id.toString())).resolves.toBe(null);
      await expect(restaurantService.findOne(restaurant._id.toString())).resolves.toHaveProperty('rating', (4 + 3) / 2);

    });

  });

  describe('PUT /review/:reviewId/restaurant/:restaurantId', () => {

    let review: Review;

    beforeEach(async () => {

      const dateOfVisit = new Date();

      await reviewService.create(
        plainToClass(
          CreateReviewDto,
          { rating: 4, comment: 'comment', dateOfVisit: dateOfVisit.toISOString() },
          transformOptions,
        ),
        restaurant._id,
        memberUser._id,
      );

      review = await reviewService.create(
        plainToClass(
          CreateReviewDto,
          { rating: 2, comment: 'comment', dateOfVisit: dateOfVisit.toISOString() },
          transformOptions,
        ),
        restaurant._id,
        memberUser._id,
      );

      await reviewService.create(
        plainToClass(
          CreateReviewDto,
          { rating: 3, comment: 'comment', dateOfVisit: dateOfVisit.toISOString() },
          transformOptions,
        ),
        restaurant._id,
        memberUser._id,
      );

      await restaurantService.updateRating(restaurant._id, (4 + 2 + 3) / 3);

    });

    it('should response with 401 if the authentication bearer header is not defined', async () => {

      await request(app.getHttpServer())
        .put(`/review/${review._id}/restaurant/${restaurant._id}`)
        .expect(401);

    });

    it('should response with 403 if the user has not the admin role', async () => {

      const { accessToken } = await authService.login({
        username: 'member',
        _id:      adminUser._id,
        role:     UserRole.MEMBER,
      });

      await request(app.getHttpServer())
        .put(`/review/${review._id}/restaurant/${restaurant._id}`)
        .set('Authorization', 'Bearer ' + accessToken)
        .expect(403);

    });

    it('should response with 200 if the delete is successful', async () => {

      const { accessToken } = await authService.login({ username: 'admin', _id: adminUser._id, role: UserRole.ADMIN });

      await request(app.getHttpServer())
        .put(`/review/${review._id}/restaurant/${restaurant._id}`)
        .set('Authorization', 'Bearer ' + accessToken)
        .send({
          rating:      1,
          comment:     'new-comment',
          dateOfVisit: new Date().toISOString(),
        })
        .expect(200);

      const newReview = await reviewService.findOne(review._id.toString());

      expect(newReview).toHaveProperty('rating', 1);
      expect(newReview).toHaveProperty('comment', 'new-comment');
      await expect(restaurantService.findOne(restaurant._id.toString())).resolves
        .toHaveProperty('rating', (4 + 1 + 3) / 3);

    });

  });

  describe('GET /review', () => {

    beforeEach(async () => {

      const dateOfVisit = new Date();

      await reviewService.create(
        plainToClass(
          CreateReviewDto,
          { rating: 4, comment: 'comment', dateOfVisit: dateOfVisit.toISOString() },
          transformOptions,
        ),
        restaurant._id,
        memberUser._id,
      );

      await reviewService.create(
        plainToClass(
          CreateReviewDto,
          { rating: 2, comment: 'comment', dateOfVisit: dateOfVisit.toISOString() },
          transformOptions,
        ),
        restaurant._id,
        memberUser._id,
      );

      await reviewService.create(
        plainToClass(
          CreateReviewDto,
          { rating: 3, comment: 'comment', dateOfVisit: dateOfVisit.toISOString() },
          transformOptions,
        ),
        restaurant._id,
        memberUser._id,
      );

      const { _id } = await restaurantService.create({ name: 'test' });

      await reviewService.create(
        plainToClass(
          CreateReviewDto,
          { rating: 3, comment: 'comment', dateOfVisit: dateOfVisit.toISOString() },
          transformOptions,
        ),
        _id,
        memberUser._id,
      );

      await restaurantService.updateRating(restaurant._id, (4 + 2 + 3) / 3);

    });

    it('should response with 401 if the authentication bearer header is not defined', async () => {

      await request(app.getHttpServer())
        .get(`/review`)
        .expect(401);

    });

    it('should response with 200 and all reviews in the database', async () => {

      const { accessToken } = await authService.login({
        username: 'member',
        _id:      adminUser._id,
        role:     UserRole.MEMBER,
      });

      const { body, statusCode } = await request(app.getHttpServer())
        .get(`/review`)
        .set('Authorization', 'Bearer ' + accessToken);

      expect(statusCode).toEqual(200);
      expect(body).toHaveLength(4);

    });

    it('should response with 200 and reviews for the specific restaurant', async () => {

      const { accessToken } = await authService.login({
        username: 'member',
        _id:      adminUser._id,
        role:     UserRole.MEMBER,
      });

      const { body, statusCode } = await request(app.getHttpServer())
        .get(`/review?restaurantId=${restaurant._id}`)
        .set('Authorization', 'Bearer ' + accessToken);

      expect(statusCode).toEqual(200);
      expect(body).toHaveLength(3);

    });

  });

  describe('POST /review/restaurant/:restaurantId', () => {

    beforeEach(async () => {

      const dateOfVisit = new Date();

      await reviewService.create(
        plainToClass(
          CreateReviewDto,
          { rating: 4, comment: 'comment', dateOfVisit: dateOfVisit.toISOString() },
          transformOptions,
        ),
        restaurant._id,
        memberUser._id,
      );

      await reviewService.create(
        plainToClass(
          CreateReviewDto,
          { rating: 2, comment: 'comment', dateOfVisit: dateOfVisit.toISOString() },
          transformOptions,
        ),
        restaurant._id,
        memberUser._id,
      );

      await reviewService.create(
        plainToClass(
          CreateReviewDto,
          { rating: 3, comment: 'comment', dateOfVisit: dateOfVisit.toISOString() },
          transformOptions,
        ),
        restaurant._id,
        memberUser._id,
      );

      await restaurantService.updateRating(restaurant._id, (4 + 2 + 3) / 3);

    });

    it('should response with 401 if the authentication bearer header is not defined', async () => {

      await request(app.getHttpServer())
        .post(`/review/restaurant/${restaurant._id}`)
        .expect(401);

    });

    it('should response with 201', async () => {

      const { accessToken } = await authService.login({
        username: 'member',
        _id:      memberUser._id,
        role:     UserRole.MEMBER,
      });

      const { statusCode, body } = await request(app.getHttpServer())
        .post(`/review/restaurant/${restaurant._id}`)
        .set('Authorization', 'Bearer ' + accessToken)
        .send({
          rating:      5,
          comment:     'comment',
          dateOfVisit: new Date().toISOString(),
        });

      expect(statusCode).toEqual(201);
      expect(body).toHaveProperty('rating', 5);
      expect(body).toHaveProperty('comment', 'comment');
      expect(body).toHaveProperty('user._id', memberUser._id.toString());

      await expect(restaurantService.findOne(restaurant._id.toString())).resolves.toHaveProperty('rating', 3.5);


    });

  });

});
