import { Logger } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { TestingLogger } from '@nestjs/testing/services/testing-logger.service';
import { plainToClass } from 'class-transformer';
import { validateSync } from 'class-validator';
import { RestaurantService } from '../crud/restaurant.service';
import { ReviewService } from '../crud/review.service';
import { CreateReviewDto } from '../dto/create-review.dto';
import { ReviewDto } from '../dto/review.dto';
import { ReviewController } from './review.controller';
import Mock = jest.Mock;

describe('ReviewController', () => {
  let controller: ReviewController;
  let restaurantService: Record<keyof RestaurantService, Mock>;
  let reviewService: Record<keyof ReviewService, Mock>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ReviewController],
      providers:   [
        {
          provide:  ReviewService,
          useValue: {
            create:                         jest.fn(),
            delete:                         jest.fn(),
            findAverageRatingForRestaurant: jest.fn(),
            find:                           jest.fn(),
          },
        },
        {
          provide:  RestaurantService,
          useValue: { updateRating: jest.fn() },
        },
        {
          provide:  Logger,
          useClass: TestingLogger,
        },
      ],
    }).compile();

    controller        = module.get<ReviewController>(ReviewController);
    restaurantService = module.get(RestaurantService);
    reviewService     = module.get(ReviewService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should call the review service create method and update the restaurant rating', async () => {

    const dateOfVisit = new Date().toISOString();

    const createReviewDto = plainToClass(CreateReviewDto, { rating: 2, comment: 'comment', dateOfVisit });
    const restaurantId    = 'restaurant';
    const userId          = 'user';

    reviewService.findAverageRatingForRestaurant.mockResolvedValue(4);
    reviewService.create.mockResolvedValue({
      _id:        'id',
      rating:     3,
      comment:    'comment',
      dateOfVisit,
      date:       new Date().toISOString(),
      user:       { _id: 'id', username: 'test@example.com', role: 'member' },
      restaurant: { _id: 'id', name: 'test' },
    });

    const reviewDto = await controller.create(createReviewDto, restaurantId, { _id: userId } as any);

    expect(reviewDto).toBeInstanceOf(ReviewDto);
    expect(validateSync(reviewDto)).toHaveLength(0);
    expect(reviewService.create).toBeCalledTimes(1);
    expect(reviewService.create).toBeCalledWith(createReviewDto, restaurantId, userId);
    expect(reviewService.findAverageRatingForRestaurant).toBeCalledTimes(1);
    expect(reviewService.findAverageRatingForRestaurant).toBeCalledWith(restaurantId);
    expect(restaurantService.updateRating).toBeCalledTimes(1);
    expect(restaurantService.updateRating).toBeCalledWith(restaurantId, 4);

  });

  it('should call the review service delete and update the restaurant rating', async () => {

    const restaurantId = 'restaurant';
    const reviewId     = 'review';

    reviewService.findAverageRatingForRestaurant.mockResolvedValue(4);

    await controller.delete(restaurantId, reviewId);

    expect(reviewService.delete).toBeCalledTimes(1);
    expect(reviewService.delete).toBeCalledWith(reviewId);
    expect(reviewService.findAverageRatingForRestaurant).toBeCalledTimes(1);
    expect(reviewService.findAverageRatingForRestaurant).toBeCalledWith(restaurantId);
    expect(restaurantService.updateRating).toBeCalledTimes(1);
    expect(restaurantService.updateRating).toBeCalledWith(restaurantId, 4);

  });

  it('should call the restaurant findAll method', async () => {

    reviewService.find.mockResolvedValue([
      {
        _id:         'id',
        date:        new Date().toISOString(),
        comment:     'name',
        dateOfVisit: new Date().toISOString(),
        rating:      1,
        user:        { _id: 'user', username: 'test@example.com' },
        restaurant:  { _id: 'id', name: 'test' },
      },
    ]);

    const restaurantId = 'restaurant';

    const reviewDtoList = await controller.getAll(restaurantId);

    expect(reviewService.find).toBeCalledTimes(1);

    for (const reviewDto of reviewDtoList) {
      expect(reviewDto).toBeInstanceOf(ReviewDto);
      expect(validateSync(reviewDto)).toHaveLength(0);
    }

  });

});
