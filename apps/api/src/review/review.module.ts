import { Logger, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { RestaurantService } from '../crud/restaurant.service';
import { ReviewService } from '../crud/review.service';
import { Restaurant, RestaurantSchema } from '../schemas/restaurant.schema';
import { Review, ReviewSchema } from '../schemas/review.schema';
import { ReviewController } from './review.controller';

@Module({
  controllers: [ReviewController],
  imports:     [
    MongooseModule.forFeature([
      { name: Review.name, schema: ReviewSchema },
      { name: Restaurant.name, schema: RestaurantSchema },
    ]),
  ],
  providers:   [
    Logger,
    ReviewService,
    RestaurantService,
  ],
})
export class ReviewModule {
}
