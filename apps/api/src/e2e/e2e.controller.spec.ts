import { Logger } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { TestingLogger } from '@nestjs/testing/services/testing-logger.service';
import { RestaurantService } from '../crud/restaurant.service';
import { ReviewService } from '../crud/review.service';
import { UserService } from '../crud/user.service';
import { E2eController } from './e2e.controller';

describe('E2eController', () => {
  let controller: E2eController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [E2eController],
      providers:   [
        {
          provide:  UserService,
          useValue: {},
        },
        {
          provide:  ReviewService,
          useValue: {},
        },
        {
          provide:  RestaurantService,
          useValue: {},
        },
        {
          provide:  Logger,
          useValue: TestingLogger,
        },
      ],
    }).compile();

    controller = module.get<E2eController>(E2eController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
