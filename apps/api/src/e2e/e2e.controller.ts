import { Controller, Delete, Logger, Post, UseGuards } from '@nestjs/common';
import { createHash } from 'crypto';
import { RestaurantService } from '../crud/restaurant.service';
import { ReviewService } from '../crud/review.service';
import { UserService } from '../crud/user.service';
import { UserRole } from '../dto/user.dto';
import { E2eGuard } from './e2e.guard';

@UseGuards(E2eGuard)
@Controller('e2e')
export class E2eController {

  constructor(
    private readonly userService: UserService,
    private readonly reviewService: ReviewService,
    private readonly restaurantService: RestaurantService,
    private readonly logger: Logger,
  ) {
  }

  @Delete('user')
  async clearUser() {
    this.logger.log('clear user', 'E2eController');
    await this.userService.clear();
  }

  @Delete('restaurant')
  async clearRestaurant() {
    this.logger.log('clear restaurant', 'E2eController');
    await this.restaurantService.clear();
  }

  @Delete('review')
  async clearReview() {
    this.logger.log('clear review', 'E2eController');
    await this.reviewService.clear();
  }

  @Delete()
  async clearAll() {
    this.logger.log('clear all', 'E2eController');
    await Promise.all([
      this.userService.clear(),
      this.reviewService.clear(),
      this.restaurantService.clear(),
    ]);
  }

  @Post('seed')
  async seed() {
    await this.clearAll();
    const member = await this.userService.create({
      username: 'cypress-member',
      password: await this.hashPassword('cypress-member-password'),
      role:     UserRole.MEMBER,
    });
    await this.restaurantService.create({ name: 'Pizza' }).then(async restaurant => {
      await this.reviewService.create(
        { rating: 5, comment: 'P First', dateOfVisit: new Date() },
        restaurant._id,
        member._id,
      );
      await this.reviewService.create(
        { rating: 3, comment: 'P Second', dateOfVisit: new Date() },
        restaurant._id,
        member._id,
      );
      await this.reviewService.create(
        { rating: 4, comment: 'P Third', dateOfVisit: new Date() },
        restaurant._id,
        member._id,
      );
      await this.restaurantService.updateRating(restaurant._id, (5 + 3 + 4) / 3);
    });
    await this.restaurantService.create({ name: 'Döner' }).then(async restaurant => {
      await this.reviewService.create(
        { rating: 1, comment: 'D First', dateOfVisit: new Date() },
        restaurant._id,
        member._id,
      );
      await this.reviewService.create(
        { rating: 5, comment: 'D Second', dateOfVisit: new Date() },
        restaurant._id,
        member._id,
      );
      await this.reviewService.create(
        { rating: 2, comment: 'D Third', dateOfVisit: new Date() },
        restaurant._id,
        member._id,
      );
      await this.restaurantService.updateRating(restaurant._id, (1 + 5 + 2) / 3);
    });
    await this.restaurantService.create({ name: 'Asia' }).then(async restaurant => {
      await this.reviewService.create(
        { rating: 4, comment: 'A First', dateOfVisit: new Date() },
        restaurant._id,
        member._id,
      );
      await this.reviewService.create(
        { rating: 1, comment: 'A Second', dateOfVisit: new Date() },
        restaurant._id,
        member._id,
      );
      await this.reviewService.create(
        { rating: 2, comment: 'A Third', dateOfVisit: new Date() },
        restaurant._id,
        member._id,
      );
      await this.restaurantService.updateRating(restaurant._id, (4 + 1 + 2) / 3);
    });
    await this.restaurantService.create({ name: 'Burger' }).then(async restaurant => {
      await this.reviewService.create(
        { rating: 4, comment: 'B First', dateOfVisit: new Date() },
        restaurant._id,
        member._id,
      );
      await this.reviewService.create(
        { rating: 3, comment: 'B Second', dateOfVisit: new Date() },
        restaurant._id,
        member._id,
      );
      await this.reviewService.create(
        { rating: 2, comment: 'B Third', dateOfVisit: new Date() },
        restaurant._id,
        member._id,
      );
      await this.restaurantService.updateRating(restaurant._id, (4 + 3 + 2) / 3);
    });
    await this.userService.create({
      username: 'cypress-admin',
      password: await this.hashPassword('cypress-admin-password'),
      role:     UserRole.ADMIN,
    });
  }

  private async hashPassword(password: string): Promise<string> {
    return createHash('sha256').update(password).digest('hex');
  }

}
