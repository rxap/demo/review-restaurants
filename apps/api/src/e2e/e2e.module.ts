import { Logger, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { RestaurantService } from '../crud/restaurant.service';
import { ReviewService } from '../crud/review.service';
import { UserService } from '../crud/user.service';
import { Restaurant, RestaurantSchema } from '../schemas/restaurant.schema';
import { Review, ReviewSchema } from '../schemas/review.schema';
import { User, UserSchema } from '../schemas/user.schema';
import { E2eController } from './e2e.controller';
import { E2eGuard } from './e2e.guard';

@Module({
  controllers: [E2eController],
  imports:     [
    MongooseModule.forFeature([
      { name: Review.name, schema: ReviewSchema },
      { name: Restaurant.name, schema: RestaurantSchema },
      { name: User.name, schema: UserSchema },
    ]),
  ],
  providers:   [
    Logger,
    UserService,
    RestaurantService,
    ReviewService,
    E2eGuard,
  ],
})
export class E2eModule {
}
