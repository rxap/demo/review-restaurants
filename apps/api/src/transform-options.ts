import { ClassTransformOptions } from 'class-transformer';

export const transformOptions: ClassTransformOptions = {
  enableImplicitConversion: true,
  exposeDefaultValues:      true,
  excludeExtraneousValues:  true,
  exposeUnsetFields:        false,
};

export function TransformOptions(
  additionalOptions: ClassTransformOptions = {},
): ClassTransformOptions {
  return { ...transformOptions, ...additionalOptions };
}
