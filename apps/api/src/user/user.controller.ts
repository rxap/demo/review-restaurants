import { Body, ConflictException, Controller, Delete, Get, Param, Post, Put, UseGuards } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { Roles } from '../auth/roles.decorator';
import { RolesGuard } from '../auth/roles.guard';
import { UserService } from '../crud/user.service';
import { CreateUserDto } from '../dto/create-user.dto';
import { UpdateUserDto } from '../dto/update-user.dto';
import { UserDto, UserRole } from '../dto/user.dto';
import { transformOptions } from '../transform-options';

@UseGuards(JwtAuthGuard, RolesGuard)
@Controller('user')
export class UserController {

  constructor(
    private readonly userService: UserService,
  ) {
  }

  @Post()
  @Roles(UserRole.ADMIN)
  async create(@Body() createUser: CreateUserDto) {
    if (await this.userService.exists(createUser.username)) {
      throw new ConflictException();
    }
    await this.userService.create(createUser);
  }

  @Get()
  @Roles(UserRole.ADMIN)
  async getAll(): Promise<UserDto[]> {
    const userList = await this.userService.find();
    return plainToClass(UserDto, userList, transformOptions);
  }

  @Put(':userId')
  @Roles(UserRole.ADMIN)
  async update(
    @Body() updateUserDto: UpdateUserDto,
    @Param('userId')
      userId: string,
  ) {
    await this.userService.update(userId, updateUserDto);
  }

  @Delete(':userId')
  @Roles(UserRole.ADMIN)
  async delete(
    @Param('userId')
      userId: string,
  ) {
    await this.userService.delete(userId);
  }

  @Get(':userId')
  @Roles(UserRole.ADMIN)
  async getById(
    @Param('userId')
      userId: string,
  ) {
    const user = await this.userService.findOne(userId);
    return plainToClass(UserDto, user, transformOptions);
  }

}
