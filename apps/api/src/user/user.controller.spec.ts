import { Logger } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { TestingLogger } from '@nestjs/testing/services/testing-logger.service';
import { plainToClass } from 'class-transformer';
import { UserService } from '../crud/user.service';
import { CreateUserDto } from '../dto/create-user.dto';
import { UserRole } from '../dto/user.dto';
import { UserController } from './user.controller';
import Mock = jest.Mock;

describe('UserController', () => {
  let controller: UserController;

  let userService: Record<keyof UserService, Mock>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserController],
      providers:   [
        {
          provide:  UserService,
          useValue: { create: jest.fn(), exists: jest.fn().mockResolvedValue(false) },
        },
        {
          provide:  Logger,
          useClass: TestingLogger,
        },
      ],
    }).compile();

    controller  = module.get<UserController>(UserController);
    userService = module.get(UserService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should call user create method', async () => {

    const createUser = plainToClass(
      CreateUserDto,
      { username: 'test@example.com', password: 'test', role: UserRole.MEMBER },
    );

    await expect(await controller.create(createUser)).resolves;

    expect(userService.create).toBeCalledTimes(1);
    expect(userService.create).toBeCalledWith(createUser);

  });

});
