import { INestApplication, ValidationPipe } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { Test } from '@nestjs/testing';
import * as request from 'supertest';
import { AuthModule } from '../auth/auth.module';
import { AuthService } from '../auth/auth.service';
import { UserService } from '../crud/user.service';
import { UserRole } from '../dto/user.dto';
import { closeInMongodConnection, rootMongooseTestModule } from '../mongoose-test.module';
import { User } from '../schemas/user.schema';
import { transformOptions } from '../transform-options';
import { validatorOptions } from '../validator-options';
import { UserModule } from './user.module';

describe('UserModule', () => {

  let app: INestApplication;
  let authService: AuthService;
  let userService: UserService;
  let adminUser: User;
  let memberUser: User;

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [
        rootMongooseTestModule(),
        AuthModule,
        ConfigModule.forRoot({
          isGlobal: true,
          load:     [() => ({ jwt: { secret: 'secret' } })],
        }),
        UserModule,
      ],
    })
      .compile();

    app = moduleRef.createNestApplication();

    app.useGlobalPipes(
      new ValidationPipe({
        ...validatorOptions,
        transformOptions,
        transform: true,
      }),
    );

    await app.init();

    userService = app.get(UserService);
    authService = app.get(AuthService);

  });

  beforeEach(async () => {
    memberUser = await userService.create({ username: 'member', password: 'e2e', role: UserRole.MEMBER });
    adminUser  = await userService.create({ username: 'admin', password: 'e2e', role: UserRole.ADMIN });
  });

  afterEach(async () => {
    await userService.clear();
  });

  afterAll(async () => {
    await app.close();
    await closeInMongodConnection();
    jest.resetAllMocks();
  });

  describe('POST /user', () => {

    it('should response with 401 if the authentication bearer header is not defined', async () => {

      await request(app.getHttpServer())
        .post(`/user`)
        .expect(401);

    });

    it('should response with 403 if the user has not the admin role', async () => {

      const { accessToken } = await authService.login({
        username: 'member',
        _id:      adminUser._id,
        role:     UserRole.MEMBER,
      });

      await request(app.getHttpServer())
        .post(`/user`)
        .set('Authorization', 'Bearer ' + accessToken)
        .expect(403);

    });

    it('should response with 201', async () => {

      const { accessToken } = await authService.login({ username: 'member', _id: adminUser._id, role: UserRole.ADMIN });

      await request(app.getHttpServer())
        .post(`/user`)
        .set('Authorization', 'Bearer ' + accessToken)
        .send({
          username: 'username',
          role:     UserRole.ADMIN,
          password: 'password',
        }).expect(201);

    });

    it('should response with 409 if user with the same username already exists', async () => {

      const { accessToken } = await authService.login({ username: 'member', _id: adminUser._id, role: UserRole.ADMIN });

      await userService.create({ username: 'username', password: 'Password', role: UserRole.ADMIN });

      await request(app.getHttpServer())
        .post(`/user`)
        .set('Authorization', 'Bearer ' + accessToken)
        .send({
          username: 'username',
          role:     UserRole.ADMIN,
          password: 'password',
        }).expect(409);

    });

  });

  describe('GET /user', () => {

    it('should response with 401 if the authentication bearer header is not defined', async () => {

      await request(app.getHttpServer())
        .get(`/user`)
        .expect(401);

    });

    it('should response with 403 if the user has not the admin role', async () => {

      const { accessToken } = await authService.login({
        username: 'member',
        _id:      adminUser._id,
        role:     UserRole.MEMBER,
      });

      await request(app.getHttpServer())
        .get(`/user`)
        .set('Authorization', 'Bearer ' + accessToken)
        .expect(403);

    });

    it('should response with 403 if the user has not the admin role', async () => {

      const { accessToken } = await authService.login({ username: 'member', _id: adminUser._id, role: UserRole.ADMIN });

      await userService.create({ username: 'username1', password: 'password', role: UserRole.MEMBER });
      await userService.create({ username: 'username2', password: 'password', role: UserRole.MEMBER });
      await userService.create({ username: 'username3', password: 'password', role: UserRole.MEMBER });

      const { statusCode, body } = await request(app.getHttpServer())
        .get(`/user`)
        .set('Authorization', 'Bearer ' + accessToken);

      expect(statusCode).toEqual(200);
      expect(body).toHaveLength(5);

    });


  });

  describe('PUT /user/:userId', () => {

    let user: User;

    beforeEach(async () => {

      user = await userService.create({ username: 'username', password: 'password', role: UserRole.MEMBER });

    });

    it('should response with 401 if the authentication bearer header is not defined', async () => {

      await request(app.getHttpServer())
        .put(`/user/${user._id}`)
        .expect(401);

    });

    it('should response with 403 if the user has not the admin role', async () => {

      const { accessToken } = await authService.login({
        username: 'member',
        _id:      adminUser._id,
        role:     UserRole.MEMBER,
      });

      await request(app.getHttpServer())
        .put(`/user/${user._id}`)
        .set('Authorization', 'Bearer ' + accessToken)
        .expect(403);

    });

    it('should response with 200', async () => {

      const { accessToken } = await authService.login({ username: 'member', _id: adminUser._id, role: UserRole.ADMIN });

      await request(app.getHttpServer())
        .put(`/user/${user._id}`)
        .set('Authorization', 'Bearer ' + accessToken)
        .send({
          username: 'username-edit',
          role:     UserRole.ADMIN,
        })
        .expect(200);

      const updatedUser = await userService.findOne(user._id);

      expect(updatedUser).toHaveProperty('username', 'username-edit');
      expect(updatedUser).toHaveProperty('role', UserRole.ADMIN);

    });

  });

  describe('GET /user/:userId', () => {

    let user: User;

    beforeEach(async () => {

      user = await userService.create({ username: 'username', password: 'password', role: UserRole.MEMBER });

    });

    it('should response with 401 if the authentication bearer header is not defined', async () => {

      await request(app.getHttpServer())
        .get(`/user/${user._id}`)
        .expect(401);

    });

    it('should response with 403 if the user has not the admin role', async () => {

      const { accessToken } = await authService.login({
        username: 'member',
        _id:      adminUser._id,
        role:     UserRole.MEMBER,
      });

      await request(app.getHttpServer())
        .get(`/user/${user._id}`)
        .set('Authorization', 'Bearer ' + accessToken)
        .expect(403);

    });

    it('should response with 200', async () => {

      const { accessToken } = await authService.login({ username: 'member', _id: adminUser._id, role: UserRole.ADMIN });

      const { body, statusCode } = await request(app.getHttpServer())
        .get(`/user/${user._id}`)
        .set('Authorization', 'Bearer ' + accessToken);

      expect(statusCode).toEqual(200);
      expect(body).toEqual({
        username: 'username',
        _id:      user._id.toString(),
        role:     UserRole.MEMBER,
      });

    });

  });

  describe('DELETE /user/:userId', () => {

    let user: User;

    beforeEach(async () => {

      user = await userService.create({ username: 'username', password: 'password', role: UserRole.MEMBER });

    });

    it('should response with 401 if the authentication bearer header is not defined', async () => {

      await request(app.getHttpServer())
        .delete(`/user/${user._id}`)
        .expect(401);

    });

    it('should response with 403 if the user has not the admin role', async () => {

      const { accessToken } = await authService.login({
        username: 'member',
        _id:      adminUser._id,
        role:     UserRole.MEMBER,
      });

      await request(app.getHttpServer())
        .delete(`/user/${user._id}`)
        .set('Authorization', 'Bearer ' + accessToken)
        .expect(403);

    });

    it('should response with 200', async () => {

      const { accessToken } = await authService.login({ username: 'member', _id: adminUser._id, role: UserRole.ADMIN });

      await request(app.getHttpServer())
        .delete(`/user/${user._id}`)
        .set('Authorization', 'Bearer ' + accessToken)
        .expect(200);

      await expect(userService.findOne(user._id)).resolves.toBe(null);

    });

  });

});
