import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { ConfigService } from '@rxap/config';
import { OpenApiConfigService } from '@rxap/open-api';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}
Promise
  .all([
    ConfigService.Load(),
    OpenApiConfigService.Load(),
  ])
  .catch(error => {
    console.error(`application setup failed: ${error?.message ? error.message : ''}`);
    throw error;
  }).then(() => platformBrowserDynamic()
  .bootstrapModule(AppModule)
  .catch(error => {
    console.error(`application bootstrap failed: ${error?.message ? error.message : ''}`);
    throw error;
  }));
