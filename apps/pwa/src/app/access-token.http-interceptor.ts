import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { RxapAuthenticationService } from '@rxap/authentication';
import { OpenApiConfigService } from '@rxap/open-api';
import { catchError, Observable } from 'rxjs';

@Injectable()
export class AccessTokenHttpInterceptor implements HttpInterceptor {

  constructor(
    private readonly openApiConfig: OpenApiConfigService,
    private readonly authService: RxapAuthenticationService,
    private readonly router: Router,
  ) {
  }

  public intercept(req: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    if (req.url.includes(this.openApiConfig.getBaseUrl())) {

      const accessToken = localStorage.getItem('accessToken');

      if (accessToken) {
        return next.handle(req.clone({
          setHeaders: {
            Authorization: `Bearer ${accessToken}`,
          },
        })).pipe(
          catchError(error => {
            if (error instanceof HttpErrorResponse) {
              if (error.status === 401) {
                if (!req.url.includes('/auth')) {
                  this.authService.signOut();
                  this.router.navigate(['/', 'authentication', 'login']);
                }
              }
            }
            throw error;
          }),
        );
      }

    }

    return next.handle(req);
  }


}
