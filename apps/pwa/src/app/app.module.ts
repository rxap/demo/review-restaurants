import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';
import { RxapAuthenticationService } from '@rxap/authentication';
import { ServiceWorkerPromptUpdateModule } from '@rxap/service-worker';
import { environment } from '../environments/environment';
import { AccessTokenHttpInterceptor } from './access-token.http-interceptor';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { AuthenticationService } from './authentication.service';

@NgModule({
  declarations: [AppComponent],
  imports:   [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the app is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000',
    }),
    ServiceWorkerPromptUpdateModule,
  ],
  providers: [
    {
      provide:  RxapAuthenticationService,
      useClass: AuthenticationService,
    },
    {
      provide:  HTTP_INTERCEPTORS,
      useClass: AccessTokenHttpInterceptor,
      multi:    true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
