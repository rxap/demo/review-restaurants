import { Component } from '@angular/core';

@Component({
  selector:    'review-restaurants-root',
  templateUrl: './app.component.html',
  styleUrls:   ['./app.component.scss'],
})
export class AppComponent {

}
