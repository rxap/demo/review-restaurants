import { Injectable } from '@angular/core';
import {
  AuthControllerCheckRemoteMethod,
} from '@review-restaurants/open-api/remote-methods/auth-controller-check.remote-method';
import {
  AuthControllerLoginRemoteMethod,
} from '@review-restaurants/open-api/remote-methods/auth-controller-login.remote-method';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class AuthenticationService {
  public isAuthenticated$ = new BehaviorSubject<boolean | null>(null);

  constructor(
    private readonly authControllerCheck: AuthControllerCheckRemoteMethod,
    private readonly authControllerLogin: AuthControllerLoginRemoteMethod,
  ) {
  }

  public async isAuthenticated(): Promise<boolean> {

    const accessToken = localStorage.getItem('accessToken');

    if (accessToken) {

      try {
        await this.authControllerCheck.call();
        return true;
      } catch (e) {
        console.error(e.message);
        return false;
      }

    }

    return false;

  }

  public async signInWithEmailAndPassword(username: string, password: string): Promise<boolean> {

    try {

      const accessTokenDto = await this.authControllerLogin.call({
        requestBody: {
          username,
          password,
        },
      });

      localStorage.setItem('accessToken', accessTokenDto.accessToken);
    } catch (e) {
      throw new Error(e.error?.message ?? e.message);
    }

    return true;

  }

  public async signOut(): Promise<void> {
    localStorage.removeItem('accessToken');
  }

}
