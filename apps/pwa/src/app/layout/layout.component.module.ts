import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';


@NgModule({
  declarations: [
    LayoutComponent,
  ],
  imports:      [
    MatToolbarModule,
    MatButtonModule,
    FlexLayoutModule,
    MatIconModule,
    RouterModule,
    CommonModule,
  ],
  exports:      [
    LayoutComponent,
  ],
})
export class LayoutComponentModule {
}
