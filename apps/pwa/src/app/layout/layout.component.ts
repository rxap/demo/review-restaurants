import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { RxapAuthenticationService } from '@rxap/authentication';
import { log } from '@rxap/utilities/rxjs';
import jwt_decode from 'jwt-decode';
import { filter, map, Observable, startWith } from 'rxjs';

@Component({
  selector:        'review-restaurants-layout',
  templateUrl:     './layout.component.html',
  styleUrls:       ['./layout.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  host:            { class: 'review-restaurants-layout' },
})
export class LayoutComponent implements OnInit {

  public isAdmin = false;

  public isAdminView: Observable<boolean>;

  constructor(
    private readonly authService: RxapAuthenticationService,
    private readonly router: Router,
  ) {
    this.isAdminView = this.router.events.pipe(
      filter(event => event instanceof NavigationEnd),
      map(() => this.router.url),
      startWith(this.router.url),
      log(),
      map(url => url.includes('admin')),
    );
  }

  public async logout() {
    await this.authService.signOut();
    await this.router.navigate(['/', 'authentication', 'login']);
  }

  public ngOnInit() {
    // Hack for loading the JWT and extraction the role should not be used in production
    const accessToken = localStorage.getItem('accessToken');
    if (accessToken) {
      const decoded: { role: string } = jwt_decode(accessToken);
      this.isAdmin = decoded.role === 'admin';
    }
  }

}
