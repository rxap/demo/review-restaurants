import { NgModule } from '@angular/core';
import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { RxapAuthenticationGuard } from '@rxap/authentication';
import { LayoutModule, NavigationWithInserts } from '@rxap/layout';
import { LayoutComponent } from './layout/layout.component';
import { LayoutComponentModule } from './layout/layout.component.module';

export const APP_ROUTES: Routes = [
  {
    path:         'authentication',
    loadChildren: () => import('@review-restaurants/feature/authentication').then(m => m.FeatureAuthenticationModule),
  },
  {
    path:        '',
    canActivate: [RxapAuthenticationGuard],
    component:   LayoutComponent,
    children:    [
      {
        path:         'restaurant',
        loadChildren: () => import('@review-restaurants/feature/restaurant').then(m => m.FeatureRestaurantModule),
      },
      {
        path:         'admin',
        loadChildren: () => import('@review-restaurants/feature/admin').then(m => m.FeatureAdminModule),
      },
      {
        path:       '**',
        redirectTo: 'restaurant',
      },
    ],
  },
  {
    path:       '**',
    redirectTo: '',
  },
];

export const APP_ROUTER_CONFIG: ExtraOptions = {
  enableTracing: false,
};

const navigation: () => NavigationWithInserts = () => [
  {
    label:      'Restaurant',
    routerLink: ['/', 'admin', 'restaurant'],
    icon:       {
      icon: 'restaurant',
    },
  },
  {
    label:      'Review',
    routerLink: ['/', 'admin', 'review'],
    icon:       {
      icon: 'reviews',
    },
  },
  {
    label:      'User',
    routerLink: ['/', 'admin', 'user'],
    icon:       {
      icon: 'person',
    },
  },
];

@NgModule({
  exports: [RouterModule],
  imports: [
    RouterModule.forRoot(APP_ROUTES, APP_ROUTER_CONFIG),
    LayoutComponentModule,
    LayoutModule.withNavigation(navigation),
  ],
})
export class AppRoutingModule {
}
