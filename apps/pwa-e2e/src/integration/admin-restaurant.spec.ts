describe('AdminRestaurant', () => {

  before(() => {
    cy.request({
      method:  'POST',
      url:     `http://${Cypress.env('API_HOST') ?? 'localhost'}:3000/e2e/seed`,
      headers: {
        e2e: 'true',
      },
    });

  });

  beforeEach(() => {
    cy.login(true);
    cy.intercept('GET', '/restaurant').as('getAllRestaurants');
    cy.visit('/admin/restaurant');
    cy.wait('@getAllRestaurants');
  });

  it('should show the cypress test restaurants', () => {

    cy.get('table').within(() => {
      cy.get('td').contains('Asia');
      cy.get('td').contains('Döner');
      cy.get('td').contains('Burger');
      cy.get('td').contains('Pizza');
    });

  });

  it('should create a new restaurant', () => {

    cy.get('button').contains('add').click();

    cy.getInputByLabel('Name').type('cypress-test');

    cy.intercept('POST', '/restaurant').as('createRestaurant');

    cy.get('button').contains('Submit').click();

    cy.wait('@createRestaurant');
    cy.wait('@getAllRestaurants');

    cy.get('table').within(() => {
      cy.get('td').contains('Asia');
      cy.get('td').contains('Döner');
      cy.get('td').contains('Burger');
      cy.get('td').contains('Pizza');
      cy.get('td').contains('cypress-test');
    });

  });

  it('should edit the new restaurant', () => {

    cy.get('table').within(() => {
      cy.get('td').contains('cypress-test').parents('tr').within(() => {
        cy.get('button').contains('edit').click();
      });
    });

    cy.getInputByLabel('Name').type('-edit');

    cy.intercept('PUT', '/restaurant/**').as('updateRestaurant');

    cy.get('button').contains('Submit').click();

    cy.wait('@updateRestaurant');
    cy.wait('@getAllRestaurants');

    cy.get('table').within(() => {
      cy.get('td').contains('Asia');
      cy.get('td').contains('Döner');
      cy.get('td').contains('Burger');
      cy.get('td').contains('Pizza');
      cy.get('td').contains('cypress-test-edit');
    });

  });

  it('should delete the new restaurant', () => {

    cy.intercept('DELETE', '/restaurant/**').as('deleteRestaurant');

    cy.get('table').within(() => {
      cy.get('td').contains('cypress-test').parents('tr').within(() => {
        cy.get('button').contains('delete').click();
      });
    });

    cy.get('button').contains('done').click();

    cy.wait('@deleteRestaurant');
    cy.wait('@getAllRestaurants');

    cy.get('table').within(() => {
      cy.get('td').contains('Asia');
      cy.get('td').contains('Döner');
      cy.get('td').contains('Burger');
      cy.get('td').contains('Pizza');
      cy.get('td').contains('cypress-test-edit').should('not.exist');
    });

  });

});
