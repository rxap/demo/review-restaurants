describe('AdminReview', () => {

  before(() => {
    cy.request({
      method:  'POST',
      url:     `http://${Cypress.env('API_HOST') ?? 'localhost'}:3000/e2e/seed`,
      headers: {
        e2e: 'true',
      },
    });

  });

  beforeEach(() => {
    cy.login(true);
    cy.intercept('GET', '/review').as('getAllReviews');
    cy.visit('/admin/review');
    cy.wait('@getAllReviews');
  });

  it('should show the cypress test reviews', () => {

    cy.get('table').within(() => {
      const restaurantList = ['Pizza', 'Döner', 'Asia', 'Burger'];
      const date           = [
        new Date().getDate().toFixed(0).padStart(2, '0'),
        (new Date().getMonth() + 1).toFixed(0).padStart(2, '0'),
        new Date().getFullYear(),
      ].join('.');
      for (let i = 0; i < restaurantList.length && i * 3 <= 9; i++) {
        const restaurant = restaurantList[i];
        cy.get('td').contains(restaurant[0] + ' First').parents('tr').within(() => {
          cy.get('td').contains('cypress-member');
          cy.get('td').contains(date);
          cy.get('td').contains(restaurant);
          cy.get('rr-rating');
        });
        if (i * 3 < 9) {
          cy.get('td').contains(restaurant[0] + ' Second').parents('tr').within(() => {
            cy.get('td').contains('cypress-member');
            cy.get('td').contains(restaurant);
            cy.get('td').contains(date);
            cy.get('rr-rating');
          });
        }
        if (i * 3 < 9) {
          cy.get('td').contains(restaurant[0] + ' Third').parents('tr').within(() => {
            cy.get('td').contains('cypress-member');
            cy.get('td').contains(restaurant);
            cy.get('td').contains(date);
            cy.get('rr-rating');
          });
        }
      }
    });

  });

  it('should have two table pages', () => {

    cy.get('button.mat-paginator-navigation-next').click();

    cy.get('table').within(() => {

      cy.get('td').contains('B Second').parents('tr').within(() => {
        cy.get('td').contains('cypress-member');
        cy.get('td').contains('Burger');
        cy.get('rr-rating');
      });

      cy.get('td').contains('B Third').parents('tr').within(() => {
        cy.get('td').contains('cypress-member');
        cy.get('td').contains('Burger');
        cy.get('rr-rating');
      });

    });

  });

  it('should edit a review', () => {

    cy.get('table').within(() => {
      cy.get('td').contains('A First').parents('tr').within(() => {
        cy.get('button').contains('edit').click();
      });
    });

    cy.get('button:visible:contains("star_border")').last().click();
    cy.getInputByLabel('Comment').type(' edit');

    cy.intercept('PUT', '/review/**').as('updateReview');

    cy.get('button').contains('Submit').click();

    cy.wait('@updateReview');
    cy.wait('@getAllReviews');

    cy.get('table').within(() => {
      cy.get('td').contains('A First edit').parents('tr').within(element => {
        cy.wrap(element)
          .find('mat-icon:visible:contains("star")')
          .should('have.length', 5);
      });
    });

  });

  it('should delete a review', () => {

    cy.intercept('DELETE', '/review/**').as('deleteReview');

    cy.get('table').within(() => {
      cy.get('td').contains('A First').parents('tr').within(() => {
        cy.get('button').contains('delete').click();
      });
    });

    cy.get('button').contains('done').click();

    cy.wait('@deleteReview');
    cy.wait('@getAllReviews');

    cy.get('table').within(() => {
      cy.get('td').contains('A First edit').should('not.exist');
    });

  });

});
