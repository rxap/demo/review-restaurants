describe('Auth', () => {

  before(() => {
    cy.request({
      method:  'DELETE',
      url:     `http://${Cypress.env('API_HOST') ?? 'localhost'}:3000/e2e/user`,
      headers: {
        e2e: 'true',
      },
    });
  });

  it('should display login screen', () => {
    cy.visit('/');
    cy.url().should('include', '/authentication/login');
  });

  it('should create a new member user', () => {

    cy.get('button').contains('Register').click();
    cy.url().should('include', '/authentication/register');
    cy.getInputByLabel('Username').clear().type('cypress-member');
    cy.getInputByLabel('Password').clear().type('cypress-member-password');
    cy.getInputByLabel('Role').click().get('mat-option').contains('Member').click();
    cy.get('button').contains('Register').click();
    cy.url().should('include', '/authentication/login');

  });

  it('should login with new member user', () => {

    cy.url().should('include', '/authentication/login');
    cy.getInputByLabel('Username').clear().type('cypress-member');
    cy.getInputByLabel('Password').clear().type('cypress-member-password');
    cy.get('button').contains('Login').click();
    cy.url().should('include', '/restaurant');

  });

  it('should logout from new member user', () => {

    cy.url().should('include', '/restaurant');
    cy.get('button').contains('Logout').click();
    cy.url().should('include', '/authentication/login');

  });

  it('should fail to register the same user twice', () => {

    cy.get('button').contains('Register').click();
    cy.url().should('include', '/authentication/register');
    cy.getInputByLabel('Username').clear().type('cypress-member');
    cy.getInputByLabel('Password').clear().type('cypress-member-password');
    cy.getInputByLabel('Role').click().get('mat-option').contains('Member').click();
    cy.get('button').contains('Register').click();
    cy.url().should('include', '/authentication/register');
    cy.get('mat-error').contains('Use a unique username');

  });

  it('should create a new admin user', () => {

    cy.get('button').contains('Register').click();
    cy.url().should('include', '/authentication/register');
    cy.getInputByLabel('Username').clear().type('cypress-admin');
    cy.getInputByLabel('Password').clear().type('cypress-admin-password');
    cy.getInputByLabel('Role').click().get('mat-option').contains('Admin').click();
    cy.get('button').contains('Register').click();
    cy.url().should('include', '/authentication/login');

  });

  it('should login with new admin user', () => {

    cy.url().should('include', '/authentication/login');
    cy.getInputByLabel('Username').clear().type('cypress-admin');
    cy.getInputByLabel('Password').clear().type('cypress-admin-password');
    cy.get('button').contains('Login').click();
    cy.url().should('include', '/restaurant');

  });

  it('should logout from new admin user', () => {

    cy.url().should('include', '/restaurant');
    cy.get('button').contains('Logout').click();
    cy.url().should('include', '/authentication/login');

  });

});
