describe('Review', () => {

  before(() => {
    cy.request({
      method:  'POST',
      url:     `http://${Cypress.env('API_HOST') ?? 'localhost'}:3000/e2e/seed`,
      headers: {
        e2e: 'true',
      },
    });

  });

  beforeEach(() => {
    cy.login(false);
    cy.intercept('GET', '/restaurant').as('getAllRestaurants');
    cy.visit('/');
    cy.wait('@getAllRestaurants');
  });

  it('should show restaurant list', () => {

    cy.get('mat-list-item').contains('Pizza').within((element) => {
      cy.wrap(element)
        .parents('mat-list-item')
        .find('mat-icon:visible:contains("star_border")')
        .should('have.length', 1);
    });
    cy.get('mat-list-item').contains('Döner').within((element) => {
      cy.wrap(element)
        .parents('mat-list-item')
        .find('mat-icon:visible:contains("star_border")')
        .should('have.length', 2);
    });
    cy.get('mat-list-item').contains('Asia').within((element) => {
      cy.wrap(element)
        .parents('mat-list-item')
        .find('mat-icon:visible:contains("star_border")')
        .should('have.length', 3);
    });

  });

  it('should open restaurant details', () => {

    cy.get('mat-list-item').contains('Asia').within((element) => {
      cy.wrap(element)
        .parents('mat-list-item').within(() => {
        cy.get('button')
          .contains('Details')
          .click();
      });
    });

    cy.get('.mat-dialog-title').contains('Restaurant Asia').within(element => {
      cy.wrap(element)
        .parents('rr-details').within(() => {

        cy.get('th').contains('Average').within(average => {
          cy.wrap(average).parents('tr').within(() => {
            cy.get('td')
              .find('mat-icon:visible:contains("star_border")')
              .should('have.length', 3);
          });
        });

        cy.get('th').contains('Best').within(best => {
          cy.wrap(best).parents('tr').within(() => {
            cy.get('td')
              .find('mat-icon:visible:contains("star_border")')
              .should('have.length', 1);
          });
        });

        cy.get('th').contains('Worst').within(worst => {
          cy.wrap(worst).parents('tr').within(() => {
            cy.get('td')
              .find('mat-icon:visible:contains("star_border")')
              .should('have.length', 4);
          });
        });

        cy.get('textarea').should('have.value', 'A Third');

        cy.get('span').contains('Latest Review').within(comment => {
          cy.wrap(comment).parent('div').within(() => {
            cy.get('rr-rating')
              .find('mat-icon:visible:contains("star_border")')
              .should('have.length', 3);
          });
        });

        cy.get('button').contains('Close').click();

      });
    });

    cy.get('mat-list-item').contains('Asia');

  });

  it('should add new review to restaurant Asia', () => {

    cy.get('mat-list-item').contains('Asia').within((element) => {
      cy.wrap(element)
        .parents('mat-list-item').within(() => {
        cy.get('button')
          .contains('Review')
          .click();
      });
    });

    cy.intercept('GET', '/restaurant').as('getAllRestaurants');

    cy.get('.mat-dialog-title').contains('Review restaurant Asia').within(element => {
      cy.wrap(element)
        .parents('rr-review').within(() => {

        cy.get('button:visible:contains("star_border")').last().click();
        cy.get('textarea').type('cypress comment');
        cy.get('button').contains('Post').click();

      });
    });

    cy.wait('@getAllRestaurants');

    cy.get('mat-list-item').contains('Asia').within((element) => {
      cy.wrap(element)
        .parents('mat-list-item')
        .find('mat-icon:visible:contains("star_border")')
        .should('have.length', 2);
    });

  });

  it('should open restaurant details with changed values', () => {

    cy.get('mat-list-item').contains('Asia').within((element) => {
      cy.wrap(element)
        .parents('mat-list-item').within(() => {
        cy.get('button')
          .contains('Details')
          .click();
      });
    });

    cy.get('.mat-dialog-title').contains('Restaurant Asia').within(element => {
      cy.wrap(element)
        .parents('rr-details').within(() => {

        cy.get('th').contains('Average').within(average => {
          cy.wrap(average).parents('tr').within(() => {
            cy.get('td')
              .find('mat-icon:visible:contains("star_border")')
              .should('have.length', 2);
          });
        });

        cy.get('th').contains('Best').within(best => {
          cy.wrap(best).parents('tr').within(() => {
            cy.get('td')
              .find('mat-icon:visible:contains("star_border")')
              .should('have.length', 0);
          });
        });

        cy.get('th').contains('Worst').within(worst => {
          cy.wrap(worst).parents('tr').within(() => {
            cy.get('td')
              .find('mat-icon:visible:contains("star_border")')
              .should('have.length', 4);
          });
        });

        cy.get('textarea').should('have.value', 'cypress comment');

        cy.get('span').contains('Latest Review').within(comment => {
          cy.wrap(comment).parent('div').within(() => {
            cy.get('rr-rating')
              .find('mat-icon:visible:contains("star_border")')
              .should('have.length', 0);
          });
        });

        cy.get('button').contains('Close').click();

      });
    });

    cy.get('mat-list-item').contains('Asia');

  });

});
