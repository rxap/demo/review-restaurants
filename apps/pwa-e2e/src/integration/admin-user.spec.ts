describe('AdminUser', () => {

  before(() => {
    cy.request({
      method:  'POST',
      url:     `http://${Cypress.env('API_HOST') ?? 'localhost'}:3000/e2e/seed`,
      headers: {
        e2e: 'true',
      },
    });

  });

  beforeEach(() => {
    cy.login(true);
    cy.intercept('GET', '/user').as('getAllUsers');
    cy.visit('/admin/user');
    cy.wait('@getAllUsers');
  });

  it('should show the cypress test users', () => {

    cy.get('table').within(() => {
      cy.get('td').contains('cypress-member');
      cy.get('td').contains('cypress-admin');
    });

  });

  it('should create a new user', () => {

    cy.get('button').contains('add').click();

    cy.getInputByLabel('Username').type('cypress-test');
    cy.getInputByLabel('Password').type('cypress-test-password');

    cy.intercept('POST', '/user').as('createUser');

    cy.get('button').contains('Submit').click();

    cy.wait('@createUser');
    cy.wait('@getAllUsers');

    cy.get('table').within(() => {
      cy.get('td').contains('cypress-member');
      cy.get('td').contains('cypress-admin');
      cy.get('td').contains('cypress-test').parents('tr').within(() => {
        cy.get('td').contains('member');
      });
    });

  });

  it('should edit the new user', () => {

    cy.get('table').within(() => {
      cy.get('td').contains('cypress-test').parents('tr').within(() => {
        cy.get('button').contains('edit').click();
      });
    });

    cy.getInputByLabel('Username').type('-edit');
    cy.getInputByLabel('Role').click().get('mat-option').contains('admin').click();

    cy.intercept('PUT', '/user/**').as('updateUser');

    cy.get('button').contains('Submit').click();

    cy.wait('@updateUser');
    cy.wait('@getAllUsers');

    cy.get('table').within(() => {
      cy.get('td').contains('cypress-member');
      cy.get('td').contains('cypress-admin');
      cy.get('td').contains('cypress-test-edit').parents('tr').within(() => {
        cy.get('td').contains('admin');
      });
    });

  });

  it('should delete the new user', () => {

    cy.intercept('DELETE', '/user/**').as('deleteUser');

    cy.get('table').within(() => {
      cy.get('td').contains('cypress-test').parents('tr').within(() => {
        cy.get('button').contains('delete').click();
      });
    });

    cy.get('button').contains('done').click();

    cy.wait('@deleteUser');
    cy.wait('@getAllUsers');

    cy.get('table').within(() => {
      cy.get('td').contains('cypress-member');
      cy.get('td').contains('cypress-admin');
      cy.get('td').contains('cypress-test-edit').should('not.exist');
    });

  });

});
