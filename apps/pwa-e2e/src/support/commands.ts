// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************

// eslint-disable-next-line @typescript-eslint/no-namespace
declare namespace Cypress {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  interface Chainable<Subject> {
    getInputByLabel(label: string): Chainable<JQuery>;

    saveLocalStorage(): void;

    restoreLocalStorage(): void;

    login(admin: boolean): void;
  }
}

Cypress.Commands.add('login', (admin: boolean) => {

  const adminPassword  = '4b5d8fb7226a1564f16070877751c196fd0c44bd8052636d173c3214922d874b';
  const adminUsername  = 'cypress-admin';
  const memberPassword = '50af6fc96f0495051cafc4db027600c85137b5d3b08261dca5bf3ea57c65a68f';
  const memberUsername = 'cypress-member';

  if (admin) {
    cy.request({
      method: 'POST',
      url:    `http://${Cypress.env('API_HOST') ?? 'localhost'}:3000/auth/login`,
      body:   {
        username: adminUsername,
        password: adminPassword,
      },
    }).then(response => {

      expect(response.status).to.eq(200);
      const { accessToken } = response.body;
      expect(accessToken).to.be.not.null;
      localStorage.setItem('accessToken', accessToken);

    });
  } else {
    cy.request({
      method: 'POST',
      url:    `http://${Cypress.env('API_HOST') ?? 'localhost'}:3000/auth/login`,
      body:   {
        username: memberUsername,
        password: memberPassword,
      },
    }).then(response => {

      expect(response.status).to.eq(200);
      const { accessToken } = response.body;
      expect(accessToken).to.be.not.null;
      localStorage.setItem('accessToken', accessToken);

    });
  }

});

Cypress.Commands.add('getInputByLabel', (label: string) => {
  return cy
    .contains('label', label)
    .invoke('attr', 'for')
    .then((id) => cy.get('#' + id));
});

const LOCAL_STORAGE_MEMORY: Record<string, string> = {};

Cypress.Commands.add('saveLocalStorage', () => {
  Object.keys(localStorage).forEach(key => {
    LOCAL_STORAGE_MEMORY[key] = localStorage[key];
  });
});

Cypress.Commands.add('restoreLocalStorage', () => {
  Object.keys(LOCAL_STORAGE_MEMORY).forEach(key => {
    localStorage.setItem(key, LOCAL_STORAGE_MEMORY[key]);
  });
});
